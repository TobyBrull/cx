#include "misc/time.hpp"

#include <catch2/catch.hpp>

#include <chrono>
#include <iostream>

using namespace cx;
using namespace cx::literals;

TEST_CASE("time and duration", "[time][duration]")
{
  static_assert(!cx::time{});
  static_assert((1_s).nanos() == 1'000'000'000);
  static_assert(time::epoch() + 1_s > time::min());
  static_assert(1_s == 1'000'000'000 * 1_ns);
  static_assert(1_s == 1'000'000 * 1_us);
  static_assert(1_s == 1'000 * 1_ms);

  CHECK(fmt::format("{}", cx::time{}) == "INVALID");
  CHECK(fmt::format("{}", time::min()) == "MIN_TIME");
  CHECK(fmt::format("{}", time::max()) == "MAX_TIME");
  CHECK(fmt::format("{}", time::epoch()) == "1970-01-01T00:00:00.000000000Z");

  CHECK(
      fmt::format("{}", time::epoch() + 1_day) ==
      "1970-01-02T00:00:00.000000000Z");
}

TEST_CASE("default clock properties", "[clock]") {}

namespace {
  using reference_clock = std::chrono::high_resolution_clock;
  using nanoseconds     = std::chrono::duration<double, std::nano>;

  template<class Clock>
  struct test_result {
    uint64_t dummy          = {};
    nanoseconds ns_per_call = {};
    string_view name        = {};

    void print() const
    {
      using period = typename Clock::duration::period;
      const cx::time now(Clock::now().time_since_epoch().count());
      fmt::print(
          "format={}, name={}, duration={}ns, precision={}/{}, ignore={}\n",
          fmt::format("{}", now),
          name,
          ns_per_call.count(),
          period::num,
          period::den,
          dummy % 10);
    }
  };

  template<class Clock>
  [[gnu::noinline]] test_result<Clock> test_clock_speed()
  {
    constexpr uint64_t N = 1'000'000;

    uint64_t dummy = 0;

    const auto t0 = reference_clock::now();
    for (uint64_t j = 0; j < N; ++j) {
      dummy += Clock::now().time_since_epoch().count();
    }
    const auto t1 = reference_clock::now();

    const auto ns_per_call = nanoseconds(t1 - t0) / N;
    return {
        .dummy       = dummy,
        .ns_per_call = ns_per_call,
        .name        = typeid(Clock).name(),
    };
  }
}

TEST_CASE("clock benchmark", "[clock][benchmark][.]")
{
  test_clock_speed<cx::clock_rdtsc>().print();
  test_clock_speed<cx::clock_gettime_realtime>().print();
  test_clock_speed<cx::clock_gettime_monotonic>().print();
  test_clock_speed<std::chrono::high_resolution_clock>().print();
  test_clock_speed<std::chrono::system_clock>().print();
  // test_clock_speed<std::chrono::utc_clock>().print();
}
