#include "misc/error.hpp"

#include "test_common.hpp"

#include <catch2/catch.hpp>

CX_DEFINE_ERROR_CATEGORY(one, 60'001);
CX_DEFINE_ERROR_CATEGORY(two, 60'002);

namespace cxxx {
  CX_DEFINE_ERROR_CATEGORY(three, 60'003);
}

using namespace cx;

TEST_CASE("error_category", "[error_category]")
{
  using err_cat_pc = detail__error_category_private_access_;

  static_assert(std::popcount(err_cat_pc::id_mask + 1) == 1);
  static_assert(err_cat_pc::id_bits < sizeof(error_id_t) * 8 - 4);
  static_assert(err_cat_pc::id_bits >= 8);

  static_assert(sizeof(error_category) == sizeof(error_id_t));
  static_assert(sizeof(error) == sizeof(error_id_t));

  static_assert(errcat_one.id() == 60'001);
  static_assert(errcat_two.id() == 60'002);
  static_assert(cxxx::errcat_three.id() == 60'003);

  CHECK(errcat_one.name() == "one");
  CHECK(errcat_two.name() == "two");
  CHECK(cxxx::errcat_three.name() == "three");

  static_assert(errcat_one == errcat_one);
  static_assert(errcat_one != errcat_two);
  static_assert(errcat_one != cxxx::errcat_three);
  static_assert(errcat_one < errcat_two);
  static_assert(errcat_two < cxxx::errcat_three);
}

TEST_CASE("error", "[error]")
{
  constexpr error e1(errcat_one);
  constexpr error e2(errcat_two);

  static_assert(e1 == e1);
  static_assert(e1 != e2);
  static_assert(e1 < e2);

  CHECK(e1.message() == "one");
  CHECK(e2.message() == "two");

  const auto em_size = [] {
    return detail::error_messages.with([](auto& vv) { return vv.size(); });
  };

  CHECK(em_size() == 1);
  {
    const error em1(errcat_one, fmt::format("answer not {}", 42));
    CHECK(em_size() == 2);

    CHECK(em1.message() == "answer not 42");
  }
  CHECK(em_size() == 1);
  {
    error em1(errcat_one, fmt::format("answer not {}", 42));
    error em2 = em1;
    error em3(errcat_one, "dunno lol");
    em2 = em1;
    em1.clear();
    CHECK(!em1);
    CHECK(em_size() == 5);
  }
  CHECK(em_size() == 1);
}
