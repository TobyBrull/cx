#include "misc/startup.hpp"

#include <catch2/catch.hpp>

using namespace cx;

namespace {
  int counter = 0;

  void incrementer(int x)
  {
    counter += x;
  }
}

CX_STARTUP(incrementer(3));

// clang-format off
CX_STARTUP_TAGGED(tag1, incrementer(500)); CX_STARTUP_TAGGED(tag2, incrementer(500));
// clang-format on

namespace {
  CX_STARTUP(incrementer(20));
}

namespace some_namespace {
  CX_STARTUP(incrementer(100));
}

TEST_CASE("at startup", "[at_startup]")
{
  CHECK(counter == 1123);
}
