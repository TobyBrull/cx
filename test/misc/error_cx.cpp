#include "misc/error_cx.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("cx errors", "[error_cx]")
{
  CHECK(errcat_empty.name() == "empty");
  static_assert(!errcat_empty);
}
