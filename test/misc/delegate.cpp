#include "misc/delegate.hpp"
#include "misc/fixed_address.hpp"

#include <catch2/catch.hpp>

using namespace cx;

namespace test {

  constexpr int free_function_1(const double x)
  {
    return (int)(x * 10.0);
  }

  constexpr int free_function_2(const int i, const double x)
  {
    return (int)(x * 10.0) + i;
  }

  struct Foo : public fixed_address {
    int x = 1000;

    constexpr int member(double d)
    {
      return x + d;
    }

    constexpr int member_const(double d) const
    {
      return x + d;
    }

    constexpr static int static_member_1(const double x)
    {
      return (int)(x * 10.0);
    }

    constexpr static int static_member_2(const int i, const double x)
    {
      return (int)(x * 10.0) + i;
    }
  };
}

TEST_CASE("delegate", "[delegate]")
{
  delegate<int(double)> dg;
  CHECK(!dg);

  dg = make_delegate<&test::free_function_1>();
  CHECK(dg);
  CHECK(dg(3.14) == 31);
  static_assert(make_delegate<&test::free_function_1>()(7.1) == 71);

  {
    const auto copy = dg;
    CHECK(copy(7.1) == 71);
  }

  dg = make_delegate<&test::free_function_2>(100);
  CHECK(dg(3.14) == 31 + 100);
  static_assert(make_delegate<&test::free_function_2>(100)(7.1) == 71 + 100);

  {
    const auto copy = dg;
    CHECK(copy(7.1) == 71 + 100);
  }

  dg = make_delegate<&test::Foo::static_member_1>();
  CHECK(dg(3.14) == 31);
  static_assert(make_delegate<&test::Foo::static_member_1>()(7.1) == 71);

  dg = make_delegate<&test::Foo::static_member_2>(50);
  CHECK(dg(3.14) == 31 + 50);
  static_assert(make_delegate<&test::Foo::static_member_2>(50)(7.1) == 71 + 50);

  test::Foo f;

  dg = make_delegate<&test::Foo::member>(&f);
  CHECK(dg(3.1) == 1003);
  CHECK(make_delegate<&test::Foo::member>(&f)(7.1) == 1007);

  constexpr test::Foo cf;

  dg = make_delegate<&test::Foo::member_const>(&cf);
  CHECK(dg(3.1) == 1003);
  static_assert(make_delegate<&test::Foo::member_const>(&cf)(7.1) == 1007);

  CHECK(dg);
  dg.clear();
  CHECK(!dg);
}

TEST_CASE("delegate size", "[delegate]")
{
  static_assert(sizeof(delegate<int()>) == sizeof(void*) * 2);
  static_assert(sizeof(delegate<int(double)>) == sizeof(void*) * 2);
}

namespace {
  int bar(delegate<int(double)> d)
  {
    return d(123.4);
  }
}

TEST_CASE("make_delegate", "[delegate][make_delegate]")
{
  test::Foo f;

  const auto d = make_delegate<&test::Foo::member>(&f);

  CHECK(bar(d) == 1123);
}
