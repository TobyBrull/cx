#include "misc/tuple.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("tuple-for-each", "[tuple]")
{
  const std::tuple tt = {1, 2.5};
  double result       = 0.0;
  tuple_for_each(tt, [&result](const auto x) { result += x; });
  CHECK(result == 3.5);
}

TEST_CASE("tuple-transform", "[tuple]")
{
  const tuple<optional<int>, optional<string>> t = {2, "yoyo"};
  const auto result = tuple_transform(t, [](auto x) { return x.value(); });
  static_assert(std::same_as<decltype(result), const std::tuple<int, string>>);
  CHECK(result == std::tuple<int, string>(2, "yoyo"));
}
