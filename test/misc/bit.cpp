#include "misc/bit.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("bit manipulation", "[bit]")
{
  CHECK(ceil_single_bit(0) == 1);
  CHECK(ceil_single_bit(1) == 1);
  CHECK(ceil_single_bit(2) == 2);
  CHECK(ceil_single_bit(3) == 4);
  CHECK(ceil_single_bit(4) == 4);
  CHECK(ceil_single_bit(5) == 8);
  CHECK(ceil_single_bit(6) == 8);
  CHECK(ceil_single_bit(7) == 8);

  for (size_t i = 0; i <= 300; ++i) {
    const size_t c = ceil_single_bit(i);
    CHECK(i <= c);
    CHECK(std::has_single_bit(c));
  }
}
