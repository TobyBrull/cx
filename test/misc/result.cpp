#include "misc/result.hpp"
#include "misc/error_cx.hpp"

#include "test_common.hpp"

#include <catch2/catch.hpp>

using namespace cx;
using namespace cx::detail;

using namespace std::literals;

namespace {
  struct Foo {
    vector<int> v;

    Foo(vector<int> vv) : v(vv) {}
  };
}

TEST_CASE("base result", "[result]")
{
  {
    result<int> ri(42);
    result<long> rl(std::move(ri));
    CHECK(std::move(rl).as_value() == 42);
  }
  {
    result<vector<int>> rv(vector<int>{{1, 3, 2}});
    result<Foo> rf(std::move(rv));
    CHECK(std::move(rf).as_value().v == vector<int>{{1, 3, 2}});
    CHECK(std::move(rv).as_value().empty());
  }
  {
    result<vector<int>> rv(vector<int>{{1, 3, 2}});
    result<Foo> rf(rv);
    CHECK(std::move(rv).as_value() == vector<int>{{1, 3, 2}});
    CHECK(std::move(rf).as_value().v == vector<int>{{1, 3, 2}});
  }
  static_assert(std::convertible_to<vector<int>, Foo>);
  static_assert(std::convertible_to<result<vector<int>>, result<Foo>>);
}

#if CX_WITH_EXCEPTIONS()
TEST_CASE("result", "[result]")
{
  failure_handler_guard const _{&test::throw_failure_exception};

  {
    result<int> r = error(errcat_not_enough_memory);
    CHECK(r.is_error());
    CHECK(std::move(r).as_error().category() == errcat_not_enough_memory);
    CHECK_THROWS_AS(std::move(r).as_value(), test::failure_exception);
  }
  {
    result<int> r(42);
    result<int> r2{error(errcat_address_in_use)};
    CHECK(r != r2);
    CHECK(r < r2);
    CHECK(!r.is_error());
    CHECK(std::move(r).as_value() == 42);
    CHECK_THROWS_AS(std::move(r).as_error(), test::failure_exception);
  }
  {
    result<int> res_1 = errcat_io_error;
    result<int> res_2{error{errcat_io_error, "Message 1"}};
    result<int> res_3{error{errcat_io_error, "Message 2"}};
    CHECK(res_1 == res_2);
    CHECK(res_1 == res_3);
    CHECK(res_2 == res_3);
    CHECK(fmt::to_string(std::move(res_1).as_error()) == "io_error");
    CHECK(fmt::to_string(std::move(res_2).as_error()) == "Message 1");
    CHECK(fmt::to_string(std::move(res_3).as_error()) == "Message 2");
  }
  {
    result<int> re = errcat_io_error;
    result<int> rem(error(errcat_io_error, "Message 1"));
    CHECK(std::move(re).as_error() == std::move(rem).as_error());
  }
  {
    result<void> r = errcat_not_enough_memory;
    CHECK(r.is_error());
    CHECK(std::move(r).as_error() == errcat_not_enough_memory);
  }
  {
    result<void> r{};
    result<void> rhs{errcat_io_error};
    CHECK(!r.is_error());
    CHECK(r != rhs);
    CHECK(r < rhs);
    CHECK_THROWS_AS(std::move(r).as_error(), test::failure_exception);
  }

  auto res = []() -> result<std::string> {
    auto const f = []() -> result<int> {
      return errcat_not_enough_memory;
    };

    int const i = CX_TRY(f());
    return fmt::to_string(i);
  }();
  REQUIRE(res.is_error());
  CHECK(std::move(res).as_error() == errcat_not_enough_memory);
}
#endif

namespace {
  constexpr result<float> square_pos(const float x)
  {
    if (x < 0.0) {
      return errcat_argument_out_of_domain;
    }
    return x * x;
  }

  constexpr result<int> my_function(const float x)
  {
    float const y = CX_TRY(square_pos(x));
    return static_cast<int>(y);
  }
}

TEST_CASE("result doc", "[result]")
{
  CHECK(CX_TRY_REQUIRE(my_function(2.2)) == 4);
  CHECK(CX_TRY_REQUIRE(my_function(0)) == 0);
  my_function(10);
  my_function(-10);
  static_assert(my_function(2.2).as_value_or(-1000) == 4);
  static_assert(my_function(0).as_value_or(-1000) == 0);

  // TODO: static_assert when clang bug
  // (https://github.com/llvm/llvm-project/issues/57524) fixed
  CHECK(my_function(-10).as_value_or(-1000) == -1000);

  {
    auto res = my_function(-10);
    CHECK(res.is_error());
  }

  // Check that result works with move-only types.
  {
    result<std::unique_ptr<int>> res = std::make_unique<int>(42);
    std::unique_ptr<int> ptr         = std::move(res).as_value();
    REQUIRE(ptr);
    CHECK(*ptr == 42);
  }
}
