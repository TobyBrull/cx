#include "misc/generator.hpp"

#include <catch2/catch.hpp>

namespace cx::test {
  std::generator<int> iota(const int n)
  {
    for (int i = 0; i < n; ++i) {
      co_yield i;
    }
  }
}

TEST_CASE("generator", "[generator]")
{
  int sum = 0;
  for (const int i: cx::test::iota(10)) {
    sum += i;
  }
  CHECK(sum == 45);
}
