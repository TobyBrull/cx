#include "misc/ring_buffer.hpp"

#include <catch2/catch.hpp>

using namespace cx;
using namespace cx::literals;

TEST_CASE("ring_buffer", "[ring_buffer]")
{
  ring_buffer<byte> br;
  br.set_capacity(10);
  CHECK(br.capacity() == 16);
}
