#include "misc/failure.hpp"

#include "test_common.hpp"

#include <catch2/catch.hpp>

#include <csignal>

using namespace cx;

#if CX_WITH_EXCEPTIONS()
TEST_CASE("failure-handler-guard RAII", "[failure]")
{
  static int count = 0;

  auto const test_failure_handle_count = [](string) {
    count += 1;
    throw test::failure_exception();
  };

  {
    failure_handler_guard const _{test_failure_handle_count};
    CHECK_THROWS_AS(run_failure_handler("test"), test::failure_exception);
    CHECK(count == 1);
    {
      failure_handler_guard const _{&test::throw_failure_exception};
      CHECK_THROWS_AS(run_failure_handler("test"), test::failure_exception);
      CHECK_THROWS_AS(run_failure_handler("test"), test::failure_exception);
      CHECK_THROWS_AS(run_failure_handler("test"), test::failure_exception);
    }
    CHECK(count == 1);
    CHECK_THROWS_AS(run_failure_handler("test"), test::failure_exception);
    CHECK(count == 2);
  }
}

TEST_CASE("failure macros", "[macros][failure]")
{
  failure_handler_guard const _{&test::throw_failure_exception};
  CHECK_THROWS_AS(run_failure_handler("test"), test::failure_exception);

  // Unfortunately, the Catch2 CHECK_THROW... macros cannot be used to check
  // that a macro threw.
  {
    bool should_throw = false;
    bool caught       = false;
    try {
      SECTION("CX_REQUIRE_FMT(false, fmt)")
      {
        should_throw = CX_WITH_ASSERTS();
        CX_REQUIRE(false, "message!");
      }
      SECTION("CX_REQUIRE_FMT(false, fmt, ...)")
      {
        should_throw = CX_WITH_ASSERTS();
        CX_REQUIRE(false, "value = {}!", 42);
      }
      SECTION("CX_REQUIRE(true, fmt)")
      {
        should_throw = false;
        CX_REQUIRE(true, "message!");
      }
      SECTION("CX_REQUIRE_FMT(true, fmt, ...)")
      {
        should_throw = false;
        CX_REQUIRE(true, "value = {}!", 42);
      }
      SECTION("CX_ASSERT_FMT(false)")
      {
        should_throw = CX_WITH_ASSERTS();
        CX_ASSERT(false, "value = {}", 42);
      }
    }
    catch (test::failure_exception const&) {
      caught = true;
    }
    CHECK(should_throw == caught);
  }
}
#endif
