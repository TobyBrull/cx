#include "misc/buffer.hpp"

#include <catch2/catch.hpp>

using namespace cx;
using namespace cx::literals;

TEST_CASE("buffer", "[buffer]")
{
  const buffer_config cfg{
      .capacity = 10,
  };
  buffer<byte> buf(cfg);

  CHECK(buf.capacity() == 10);
  CHECK(buf.read_span().size() == 0);
  CHECK(buf.write_span().size() == 10);

  buf.write(vector<byte>{{50_bb, 53_bb, 56_bb}});

  CHECK(buf.capacity() == 10);
  CHECK(make_vector(buf.read_span()) == vector<byte>{{50_bb, 53_bb, 56_bb}});
  CHECK(buf.write_span().size() == 7);

  buf.write(vector<byte>{100_bb, 102_bb, 104_bb, 106_bb});

  CHECK(
      make_vector(buf.read_span()) ==
      vector<byte>{{50_bb, 53_bb, 56_bb, 100_bb, 102_bb, 104_bb, 106_bb}});
  CHECK(buf.write_span().size() == 3);

  buf.mark_read(2);

  CHECK(
      make_vector(buf.read_span()) ==
      vector<byte>{{56_bb, 100_bb, 102_bb, 104_bb, 106_bb}});
  CHECK(buf.write_span().size() == 3);

  buf.mark_read(3);

  CHECK(make_vector(buf.read_span()) == vector<byte>{{104_bb, 106_bb}});
  CHECK(buf.write_span().size() == 3);

  buf.write(vector<byte>{200_bb, 210_bb});

  CHECK(
      make_vector(buf.read_span()) ==
      vector<byte>{{104_bb, 106_bb, 200_bb, 210_bb}});
  CHECK(buf.write_span().size() == 1);

  SECTION("inplace")
  {
    buf.relocate_to_front();

    CHECK(
        make_vector(buf.read_span()) ==
        vector<byte>{{104_bb, 106_bb, 200_bb, 210_bb}});
    CHECK(buf.write_span().size() == 6);
  }
  SECTION("new vector")
  {
    const buffer_config new_cfg{
        .capacity = 12,
    };
    buf.set_config(new_cfg);

    CHECK(
        make_vector(buf.read_span()) ==
        vector<byte>{{104_bb, 106_bb, 200_bb, 210_bb}});
    CHECK(buf.write_span().size() == 8);
  }
}
