#include "misc/ct_map.hpp"

#include <catch2/catch.hpp>

class testor_entry : public cx::ct_map<"testor", int, cx::string>::entry {
  using entry::entry;
};

CX_CT_MAP_ENTRY(testor_entry, entry_one, 42, "answer");
CX_CT_MAP_ENTRY(testor_entry, entry_two, 123, "yoyo");
namespace cxxx {
  CX_CT_MAP_ENTRY(testor_entry, entry_tree, 69, "he");
}

using namespace cx;

TEST_CASE("compile-time map", "[ct_map]")
{
  static_assert(entry_one.key() == 42);
  static_assert(entry_two.key() == 123);
  static_assert(cxxx::entry_tree.key() == 69);

  CHECK(ct_map<"testor", int, string>::size() == 3);

  CHECK(entry_one.value() == "answer");
  CHECK(entry_two.value() == "yoyo");
  CHECK(cxxx::entry_tree.value() == "he");

  static_assert(entry_one == entry_one);
  static_assert(entry_one != entry_two);
  static_assert(entry_one < entry_two);
  static_assert(cxxx::entry_tree < entry_two);
  static_assert(entry_one < cxxx::entry_tree);
}
