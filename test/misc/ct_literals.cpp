#include "misc/ct_literals.hpp"

#include <catch2/catch.hpp>

using namespace cx;

namespace cx::test {
  template<ct_string name>
  string foo()
  {
    return name;
  }
}

TEST_CASE("string literals", "[literals][ct_string]")
{
  CHECK(test::foo<"bar">() == "bar");
  CHECK(string{"baz"_ct_s} == "baz");
}
