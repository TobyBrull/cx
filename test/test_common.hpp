#pragma once

#include "misc/failure.hpp"

namespace cx::test {
#if CX_WITH_EXCEPTIONS()
  // Exception class as thrown by `cx::test::throw_failure_exception`.
  class failure_exception : public std::exception {
   public:
    virtual char const* what() const noexcept override final
    {
      return "failure_exception";
    }
  };

  // Function to be used as a failure handler that simply throws a
  // :cpp:class:`cx::test::failure_exception`.
  //
  // auto const _ = FailureHandlerGuard::create(&test::throw_failure_exception);
  //
  [[noreturn]] void throw_failure_exception(string);
#endif

#define CX_TRY_REQUIRE(expression)                                         \
  ({                                                                       \
    auto result = (expression);                                            \
    if (result.is_error()) {                                               \
      INFO(fmt::format(                                                    \
          "CX_TRY_REQUIRE failed with {}", std::move(result).as_error())); \
      REQUIRE(false);                                                      \
    }                                                                      \
    std::move(result).as_value();                                          \
  })
}
