#include "test_common.hpp"

#include <catch2/catch.hpp>

using namespace cx;
using namespace cx::test;

#if CX_WITH_EXCEPTIONS()
void
cx::test::throw_failure_exception(string)
{
  throw cx::test::failure_exception();
}
#endif
