#include "async/endpoint.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("address-ipv4", "[address_ipv4]")
{
  static_assert(address_ipv4(0x01020304).as_uint32() == 0x01020304);
  static_assert(
      address_ipv4(0x01020304).data() == std::array<uint8_t, 4>{1, 2, 3, 4});
}

TEST_CASE("endpoint-ipv4", "[endpoint_ipv4]")
{
  constexpr endpoint_ipv4 ep{{{127, 0, 0, 1}}, 80};
  static_assert(ep.addr == address_ipv4::loopback());
  static_assert(ep.port == 80);
}
