#include "async/chan_ptr.hpp"

#include "misc/result.hpp"

#include <catch2/catch.hpp>

using namespace cx;

namespace {
  struct Base;

  struct BaseJournal : public chan_journal {
    using chan_journal::chan_journal;
  };

  struct Base : public chan_ptr_target {
    int i = 42;

    Base(BaseJournal* journal) : chan_ptr_target(journal) {}
  };

  struct Derived final : public Base {
    double d = 3.14;

    Derived(BaseJournal* journal) : Base(journal) {}
  };
}

TEST_CASE("chan_ptr", "[chan_ptr]")
{
  static_assert(std::convertible_to<Derived*, Base*>);
  static_assert(std::convertible_to<result<Derived*>, result<Base*>>);
  static_assert(std::convertible_to<chan_ptr<Derived>, chan_ptr<Base>>);
  static_assert(
      std::convertible_to<result<chan_ptr<Derived>>, result<chan_ptr<Base>>>);

  {
    BaseJournal journal{nullptr};

    auto [obj, cp_derived] = chan_ptr_target::make<Derived>(&journal);
    CHECK(obj->is_referenced());
    CHECK(journal.reference_gone.empty());
    CHECK(cp_derived->i == 42);
    CHECK(cp_derived->d == 3.14);

    cp_derived->i = 12;

    chan_ptr<Base> cp_base = std::move(cp_derived);
    CHECK(obj->is_referenced());
    CHECK(journal.reference_gone.empty());
    CHECK(!cp_derived);
    CHECK(cp_base);
    CHECK(obj->is_referenced());
    CHECK(cp_base->i == 12);

    Base* b = cp_base.ptr();
    cp_base.clear();
    CHECK(!obj->is_referenced());
    REQUIRE(journal.reference_gone.size() == 1);
    CHECK(*journal.reference_gone.begin() == b);
  }
}
