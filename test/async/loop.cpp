#include "async/loop.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_epoll.hpp"
#include "async/poller_timer_simple.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("loop::poller_ptr", "[loop][poller_ptr]")
{
  loop_impl<loop_config_default, poller_timer_simple> loop;

  CHECK(loop.poller_ptr<poller_epoll>() == errcat_no_such_poller);
  CHECK(
      loop.poller_ptr<poller_timer_simple>() ==
      &std::get<poller_timer_simple>(loop.pollers()));
}
