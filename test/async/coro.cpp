#include "async/coro.hpp"

#include <catch2/catch.hpp>

using namespace cx;

namespace {
  coro<int> my_coro_trivial()
  {
    co_return 42;
  }

  coro<int> my_coro_simple()
  {
    fmt::print("into my_coro_simple\n");
    co_await std::suspend_always{};
    fmt::print("outof my_coro_simple\n");
    co_return 42;
  }

  coro<int> my_coro_inner(const int i)
  {
    int retval = 0;
    for (int j = 0; j < i; ++j) {
      co_await std::suspend_always{};
      retval += j;
    }
    co_return retval;
  }

  coro<int> my_coro_outer(const int n)
  {
    int retval = 0;
    for (int i = 0; i < n; ++i) {
      retval += co_await my_coro_inner(i);
    }
    co_return retval;
  }
}

TEST_CASE("coro", "[coro]")
{
  coro<int> c;
  CHECK(c.is_valid() == false);

  c = my_coro_trivial();
  CHECK(c.is_valid());
  CHECK(c.is_done() == false);
  CHECK(c.is_in_initial_suspend());

  my_coro_simple();
  my_coro_outer(4);
}
