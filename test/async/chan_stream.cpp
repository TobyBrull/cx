#include "async/chan_stream.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("chan_stream_state", "[chan_stream_state]")
{
  constexpr chan_stream_state css(chan_stream_state::closed);
  static_assert(is_finished(css));
  static_assert(css == chan_stream_state::closed);
  static_assert(css != chan_stream_state::established);
}
