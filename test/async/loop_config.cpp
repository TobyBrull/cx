#include "async/loop_config.hpp"

#include <catch2/catch.hpp>

using namespace cx;

TEST_CASE("loop_config", "[loop_config]")
{
  static_assert(loop_config<loop_config_default>);
}
