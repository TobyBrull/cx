#pragma once

#include "async/loop.hpp"

namespace cx {
  template<
      template<typename...>
      class Connection,
      typename T,
      auto... MemFnPtrs>
  struct conn_wrapper
    : public Connection<decltype(cx::make_delegate<MemFnPtrs>(
          std::declval<T*>()))...> {
    using base = Connection<decltype(cx::make_delegate<MemFnPtrs>(
        std::declval<T*>()))...>;

    template<typename... Args>
    conn_wrapper(T* obj_ptr, Args&&... args)
      : base(
            std::forward<Args>(args)...,
            cx::make_delegate<MemFnPtrs>(obj_ptr)...)
    {
    }
  };
}
