#pragma once

#include "async/fwd.hpp"
#include "misc/failure.hpp"
#include "misc/fixed_address.hpp"

#include <concepts>
#include <memory>

namespace cx {
  class chan_ptr_target : public fixed_address {
    template<typename T>
      requires std::derived_from<T, chan_ptr_target>
    friend class chan_ptr;

   protected:
    chan_journal* journal_ = nullptr;

    chan_ptr_target(chan_journal*);

    // Note that the destructor is *not* virtual here even though this class has
    // a virtual function. Objects that derive from this type are always meant
    // to be managed by a poller, which should use the most derived type (which
    // must be marked "final" so that the "make" function below works). For an
    // example see `cx::chan_timer` and
    // `cx::poller_timer_priority_queue::chan_timer_impl`.

    /* not virtual */ ~chan_ptr_target();

   public:
    template<typename T, typename... Args>
    static std::tuple<std::unique_ptr<T>, chan_ptr<T>> make(Args&&...)
      requires(std::derived_from<T, chan_ptr_target> && std::is_final_v<T>);

    loop& owner_loop() const;

    bool is_referenced() const;
  };

  struct chan_journal : public fixed_address {
    chan_journal(loop*);

    loop* owner = nullptr;
    set<chan_ptr_target*> reference_gone;
  };

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  class chan_ptr {
    T* tgt_ = nullptr;

    friend class chan_ptr_target;

    template<typename U>
      requires std::derived_from<U, chan_ptr_target>
    friend class chan_ptr;

    chan_ptr(T* tgt) : tgt_(tgt) {}

   public:
    static_assert(std::derived_from<T, chan_ptr_target>);

    chan_ptr() = default;
    ~chan_ptr();

    chan_ptr(chan_ptr&&);
    chan_ptr& operator=(chan_ptr&&);

    template<typename U>
      requires(!std::same_as<T, U> && std::derived_from<T, U>)
    operator chan_ptr<U>() &&;

    operator bool() const;

    void clear();

    T* ptr();
    const T* ptr() const;

    T* operator->();
    const T* operator->() const;
  };
}

// #include "chan_ptr.ipp"

namespace cx {

  // chan_ptr_target

  inline chan_ptr_target::chan_ptr_target(chan_journal* journal)
    : journal_(journal)
  {
  }

  inline chan_ptr_target::~chan_ptr_target()
  {
    CX_ASSERT(
        !is_referenced(),
        "Destructing chan_ptr_target that is still referenced");
  }

  inline loop& chan_ptr_target::owner_loop() const
  {
    return *(journal_->owner);
  }

  inline bool chan_ptr_target::is_referenced() const
  {
    return (journal_ != nullptr);
  }

  template<typename T, typename... Args>
  std::tuple<std::unique_ptr<T>, chan_ptr<T>>
  chan_ptr_target::make(Args&&... args)
    requires(std::derived_from<T, chan_ptr_target> && std::is_final_v<T>)
  {
    std::unique_ptr<T> obj(new T(std::forward<Args>(args)...));
    chan_ptr<T> cp(obj.get());
    return {std::move(obj), std::move(cp)};
  }

  // chan_journal

  inline chan_journal::chan_journal(loop* l) : owner(l) {}

  // chan_ptr

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  chan_ptr<T>::~chan_ptr()
  {
    clear();
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  chan_ptr<T>::chan_ptr(chan_ptr&& other)
    : tgt_(std::exchange(other.tgt_, nullptr))
  {
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  chan_ptr<T>& chan_ptr<T>::operator=(chan_ptr&& other)
  {
    if (this != &other) {
      clear();
      tgt_ = std::exchange(other.tgt_, nullptr);
    }
    return (*this);
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  template<typename U>
    requires(!std::same_as<T, U> && std::derived_from<T, U>)
  chan_ptr<T>::operator chan_ptr<U>() &&
  {
    return chan_ptr<U>(std::exchange(tgt_, nullptr));
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  chan_ptr<T>::operator bool() const
  {
    return (tgt_ != nullptr);
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  void chan_ptr<T>::clear()
  {
    if (tgt_ != nullptr) {
      tgt_->journal_->reference_gone.insert(tgt_);
      tgt_->journal_ = nullptr;
      tgt_           = nullptr;
    }
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  T* chan_ptr<T>::ptr()
  {
    return tgt_;
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  const T* chan_ptr<T>::ptr() const
  {
    return tgt_;
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  T* chan_ptr<T>::operator->()
  {
    CX_ASSERT(tgt_ != nullptr, "Trying to derefernce nullptr");
    return tgt_;
  }

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  const T* chan_ptr<T>::operator->() const
  {
    CX_ASSERT(tgt_ != nullptr, "Trying to derefernce nullptr");
    return tgt_;
  }
}
