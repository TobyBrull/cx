#include "conn.hpp"

#include "async/agent.hpp"

namespace cx {
  loop& conn::owner_loop() const
  {
    return journal_->owner->owner_loop();
  }
}
