#pragma once

#include "async/chan_timer.hpp"
#include "async/coro.hpp"
#include "async/loop.hpp"

namespace cx {
  class coro_timer : public fixed_address {
    chan_timer_ptr timer_;
    std::coroutine_handle<> to_resume_ = nullptr;

    time on_timer(time);

   public:
    coro_timer() = default;
    coro_timer(chan_timer_ptr);

    chan_timer_ptr release() &&;

    struct awaiter;
    awaiter sleep_till(time);
  };

  coro<> coro_sleep_till(loop&, time);
  coro<> coro_sleep_for(loop&, duration);
}

// #include "coro_timer.ipp"

namespace cx {

  inline coro_timer::coro_timer(chan_timer_ptr timer) : timer_(std::move(timer))
  {
    CX_ASSERT(timer_, "coro_timer needs valid chan_timer_ptr");
    timer_->set_callback(make_delegate<&coro_timer::on_timer>(this));
  }

  inline chan_timer_ptr coro_timer::release() &&
  {
    timer_->set_callback({});
    return std::move(timer_);
  }

  struct coro_timer::awaiter : public std::suspend_always {
    coro_timer* this_ = nullptr;
    awaiter(coro_timer* this__) : this_(this__) {}

    void await_suspend(std::coroutine_handle<> h) const noexcept
    {
      this_->to_resume_ = h;
    }

    void await_resume() const noexcept
    {
      this_->to_resume_ = nullptr;
    }
  };

  inline coro_timer::awaiter coro_timer::sleep_till(const time new_expiry)
  {
    timer_->set_expiry(new_expiry);
    return awaiter(this);
  }
}
