#pragma once

#include "async/conn.hpp"
#include "async/fwd.hpp"
#include "async/poller.hpp"

namespace cx {
  class conn_manager : public fixed_address {
    conn_journal journal_;
    map<conn*, std::unique_ptr<conn>> conns_;

   public:
    conn_manager(agent*);

    size_t size() const;

    size_t is_empty() const;

    result<poller_result> poll_and_dispatch();

    poller_state poll_state() const;

    template<typename Conn, typename... Args>
    Conn* make_conn(Args&&... args);
  };
}

// #include "conn_manager.ipp"

namespace cx {
  inline conn_manager::conn_manager(agent* owner) : journal_(owner) {}

  inline size_t conn_manager::size() const
  {
    return conns_.size();
  }

  inline size_t conn_manager::is_empty() const
  {
    return conns_.empty();
  }

  inline result<poller_result> conn_manager::poll_and_dispatch()
  {
    poller_result retval;
    for (conn* c: journal_.removed) {
      conns_.erase(c);
      retval.num_processed_events += 1;
    }
    journal_.removed.clear();
    return retval;
  }

  inline poller_state conn_manager::poll_state() const
  {
    if (size() > 0) {
      return poller_state::busy();
    }

    return poller_state{};
  }

  template<typename Conn, typename... Args>
  Conn* conn_manager::make_conn(Args&&... args)
  {
    auto ptr = std::make_unique<Conn>(&journal_, std::forward<Args>(args)...);
    auto* retval   = ptr.get();
    conns_[retval] = std::move(ptr);
    return retval;
  }
}
