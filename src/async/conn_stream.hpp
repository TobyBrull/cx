#pragma once

#include "async/chan_stream.hpp"
#include "async/conn.hpp"
#include "misc/result.hpp"

namespace cx {
  // TODO: Get rid of "Derived" template paramter once "deducing this" is
  // available and rethink access control (public/protected/private).

  template<typename Derived>
  class conn_stream : public conn {
    void on_stream(chan_stream_event, chan_stream*);

   protected:
    chan_stream_ptr ptr_;

    // Convenience functions forwarding to ptr_ (more useful once "deducing
    // this" comes around).
    chan_stream_state state() const;
    void mark_written();
    void close();
    void fail();

    bool is_recv_buffer_full() const;

   public:
    conn_stream(conn_journal*, chan_stream_ptr);
  };
}

// #include "conn_stream.ipp"

namespace cx {

  // conn_stream

  template<typename Derived>
  void conn_stream<Derived>::on_stream(
      const chan_stream_event ev, [[maybe_unused]] chan_stream* const s)
  {
    CX_ASSERT(s == ptr_.ptr());
    static_cast<Derived*>(this)->on_conn_stream(ev);
  }

  template<typename Derived>
  chan_stream_state conn_stream<Derived>::state() const
  {
    return ptr_->state();
  }
  template<typename Derived>
  void conn_stream<Derived>::mark_written()
  {
    return ptr_->mark_written();
  }
  template<typename Derived>
  void conn_stream<Derived>::close()
  {
    return ptr_->close();
  }
  template<typename Derived>
  void conn_stream<Derived>::fail()
  {
    return ptr_->fail();
  }

  template<typename Derived>
  bool conn_stream<Derived>::is_recv_buffer_full() const
  {
    return ptr_->recv_buffer().is_full();
  }

  template<typename Derived>
  conn_stream<Derived>::conn_stream(conn_journal* journal, chan_stream_ptr ptr)
    : conn(journal), ptr_(std::move(ptr))
  {
    ptr_->set_callback(make_delegate<&conn_stream<Derived>::on_stream>(this));
  }
}
