#pragma once

#include "async/fwd.hpp"
#include "misc/fixed_address.hpp"

namespace cx {
  class conn : public fixed_address {
   protected:
    conn_journal* journal_ = nullptr;

    conn(conn_journal*);

    void remove_this_conn();

   public:
    agent* owner_agent() const;

    /// Convenience function for conn.owner_agent()->owner_loop()
    loop& owner_loop() const;

    virtual ~conn() = default;
  };

  struct conn_journal : public fixed_address {
    conn_journal(agent*);

    agent* owner = nullptr;
    set<conn*> removed;
  };
}

// #include "conn.ipp"

namespace cx {

  // conn

  inline conn::conn(conn_journal* journal) : journal_(journal) {}

  inline void conn::remove_this_conn()
  {
    journal_->removed.insert(this);
  }

  inline agent* conn::owner_agent() const
  {
    return journal_->owner;
  }

  // conn_journal

  inline conn_journal::conn_journal(agent* a) : owner(a) {}
}
