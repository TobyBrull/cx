#pragma once

#include "async/coro.hpp"

namespace cx {
  class coro_type_erased {
    std::coroutine_handle<> handle_ = nullptr;

    template<typename T>
    friend class coro;

    friend class coro_poller;

    coro_type_erased(std::coroutine_handle<>);

   public:
    template<typename T>
    coro_type_erased(coro<T>);

    coro_type_erased();

    coro_type_erased(coro_type_erased&&);
    coro_type_erased& operator=(coro_type_erased&&);

    ~coro_type_erased();

    void clear();

    std::coroutine_handle<> handle() const;

    bool is_valid() const;
    bool is_done() const;
  };
}

// #include "coro_type_erased.ipp"

namespace cx {
  template<typename T>
  coro_type_erased::coro_type_erased(coro<T> c)
  {
    handle_   = c.handle_;
    c.handle_ = nullptr;
  }

  inline coro_type_erased::coro_type_erased(std::coroutine_handle<> h)
    : handle_(h)
  {
  }

  inline coro_type_erased::coro_type_erased(coro_type_erased&& other)
    : handle_(std::exchange(other.handle_, nullptr))
  {
  }
  inline coro_type_erased& coro_type_erased::operator=(coro_type_erased&& other)
  {
    if (this != &other) {
      clear();
      handle_ = std::exchange(other.handle_, nullptr);
    }
    return *this;
  }
  inline coro_type_erased::~coro_type_erased()
  {
    clear();
  }
  inline void coro_type_erased::clear()
  {
    if (handle_ != nullptr) {
      handle_.destroy();
      handle_ = nullptr;
    }
  }

  inline std::coroutine_handle<> coro_type_erased::handle() const
  {
    return handle_;
  }

  inline bool coro_type_erased::is_valid() const
  {
    return static_cast<bool>(handle_);
  }
  inline bool coro_type_erased::is_done() const
  {
    return handle_ && handle_.done();
  }
}
