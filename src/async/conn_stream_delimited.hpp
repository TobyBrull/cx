#pragma once

#include "async/conn_stream.hpp"
#include "misc/error_cx.hpp"

namespace cx {

  CX_DEFINE_ERROR_CATEGORY(line_too_long, 1015);

  template<typename Derived, byte Delimiter = static_cast<byte>('\n')>
  class conn_stream_delimited
    : public conn_stream<conn_stream_delimited<Derived, Delimiter>> {
    vector<size_t> line_queue_;
    size_t parse_pos_begin_ = 0;
    size_t parse_pos_end_   = 0;

    size_t offset_of_line(size_t idx) const;

    using conn_base = conn_stream<conn_stream_delimited<Derived, Delimiter>>;
    friend conn_base;
    void on_conn_stream(chan_stream_event);

   protected:
    size_t num_lines() const;
    span<const byte> get_line(size_t idx) const;
    void mark_read_lines(size_t);

    result<> write_line(span<const byte>);

   public:
    conn_stream_delimited(conn_journal*, chan_stream_ptr);
  };
}

// #include "conn_stream_delimited.ipp"

namespace cx {

  // conn_newline

  template<typename Derived, byte Delimiter>
  conn_stream_delimited<Derived, Delimiter>::conn_stream_delimited(
      conn_journal* journal, chan_stream_ptr ptr)
    : conn_base(journal, std::move(ptr))
  {
  }

  template<typename Derived, byte Delimiter>
  size_t conn_stream_delimited<Derived, Delimiter>::offset_of_line(
      const size_t idx) const
  {
    size_t retval = 0;
    for (size_t i = 0; i < idx; ++i) {
      retval += line_queue_[i] + 1;
    }
    return retval;
  }

  template<typename Derived, byte Delimiter>
  size_t conn_stream_delimited<Derived, Delimiter>::num_lines() const
  {
    return line_queue_.size();
  }

  template<typename Derived, byte Delimiter>
  span<const byte>
  conn_stream_delimited<Derived, Delimiter>::get_line(const size_t idx) const
  {
    CX_REQUIRE(
        idx < num_lines(),
        "idx={} out-of-bounds with num-lines={}",
        idx,
        line_queue_.size());

    const auto rs = conn_base::ptr_->recv_buffer().read_span();
    return {rs.data() + offset_of_line(idx), line_queue_[idx]};
  }

  template<typename Derived, byte Delimiter>
  void conn_stream_delimited<Derived, Delimiter>::mark_read_lines(
      const size_t num_read)
  {
    const size_t pos = offset_of_line(num_read);
    line_queue_.erase(line_queue_.begin(), line_queue_.begin() + num_read);
    conn_base::ptr_->recv_buffer().mark_read(pos);
    parse_pos_begin_ -= pos;
    parse_pos_end_ -= pos;
  }

  template<typename Derived, byte Delimiter>
  void conn_stream_delimited<Derived, Delimiter>::on_conn_stream(
      const chan_stream_event ev)
  {
    auto& rbuf = conn_base::ptr_->recv_buffer();

    bool found_new_line = false;

    const auto rs = rbuf.read_span().subspan(parse_pos_end_);
    if (rs.empty() == false) {
      for (size_t i = 0; i < rs.size(); ++i) {
        if (rs[parse_pos_end_] == static_cast<byte>(Delimiter)) {
          line_queue_.push_back(parse_pos_end_ - parse_pos_begin_);
          found_new_line = true;

          parse_pos_begin_ = parse_pos_end_ + 1;
        }
        parse_pos_end_ += 1;
      }
    }

    if (found_new_line || is_finished(conn_base::ptr_->state())) {
      static_cast<Derived*>(this)->on_conn_stream_delim(ev);
    }

    // If one received line is longer than the complete receive buffer.
    if (conn_base::is_recv_buffer_full() && line_queue_.empty()) {
      conn_base::fail();
      return;
    }
  }

  template<typename Derived, byte Delimiter>
  result<> conn_stream_delimited<Derived, Delimiter>::write_line(
      const span<const byte> sb)
  {
    auto& sbuf = conn_base::ptr_->send_buffer();
    CX_TRY(sbuf.write(sb));
    const byte delimiter = Delimiter;
    CX_TRY(sbuf.write(span<const byte>(&delimiter, 1)));
    return {};
  }
}
