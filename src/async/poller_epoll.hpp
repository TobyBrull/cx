#pragma once

#include "async/chan_accepter.hpp"
#include "async/chan_stream.hpp"
#include "async/chan_timer.hpp"
#include "async/loop.hpp"
#include "async/poller.hpp"
#include "misc/ptr_containers.hpp"

#include <sys/epoll.h>

namespace cx {
  class poller_epoll : public poller_base {
   public:
    class chan_stream_accepter_impl final : public chan_stream_accepter {
      template<typename T>
        requires std::derived_from<T, chan_ptr_target>
      friend class chan_ptr;
      friend class chan_ptr_target;
      friend class poller_epoll;

      poller_epoll* owner_ = nullptr;
      int accepter_fd_     = -1;

      result<> epoll_register();
      result<> trigger();

      chan_stream_accepter_impl(
          poller_epoll*,
          int accepter_fd,
          const chan_stream_config& accepted_stream_cfg,
          delegate<void(chan_stream_ptr)>);

     public:
      ~chan_stream_accepter_impl();
    };

    class chan_stream_impl final : public chan_stream {
      template<typename T>
        requires std::derived_from<T, chan_ptr_target>
      friend class chan_ptr;
      friend class chan_ptr_target;
      friend class poller_epoll;

      poller_epoll* owner_ = nullptr;
      int fd_              = -1;

      result<> epoll_register();
      result<> trigger();
      void terminate_impl(chan_stream_state);

      chan_stream_impl(poller_epoll*, int fd, const chan_stream_config&);

     public:
      ~chan_stream_impl();
    };

    class chan_message_accepter_impl final : public chan_message_accepter {
      template<typename T>
        requires std::derived_from<T, chan_ptr_target>
      friend class chan_ptr;
      friend class chan_ptr_target;
      friend class poller_epoll;

      poller_epoll* owner_ = nullptr;

     public:
    };

    class chan_message_impl final : public chan_message {
      template<typename T>
        requires std::derived_from<T, chan_ptr_target>
      friend class chan_ptr;
      friend class chan_ptr_target;
      friend class poller_epoll;

      poller_epoll* owner_ = nullptr;

     public:
    };

   private:
    loop& loop_;
    int epoll_fd_ = -1;

    vector<struct epoll_event> temp_events_;

    ptr_set<chan_stream_accepter_impl> stream_accepters_;
    chan_stream_accepter_journal stream_accepters_journal_;

    ptr_set<chan_stream_impl> streams_;
    chan_stream_journal streams_journal_;

    ptr_set<chan_message_accepter_impl> message_accepters_;

    ptr_set<chan_message_impl> messages_;

   public:
    poller_epoll(loop& loop) noexcept;
    result<> init();
    ~poller_epoll();

    string_view name() const;

    void format_json_stats(string&) const;

    result<poller_result> poll_and_dispatch(const time may_block_until);

    poller_state poll_state() const;

    size_t num_epoll_fds() const;

    result<chan_ptr<poller_epoll::chan_stream_impl>>
    create_chan_stream(const endpoint_stream&, const chan_stream_config&);

    result<chan_ptr<poller_epoll::chan_stream_accepter_impl>>
    create_chan_stream_accepter(
        const endpoint_stream_accepter&, const chan_stream_accepter_config&);
  };
}
