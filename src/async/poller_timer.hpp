#pragma once

#include "async/poller_timer_simple.hpp"

#include <optional>
#include <queue>
#include <variant>

namespace cx {
  using poller_timer_default = poller_timer_simple;
}
