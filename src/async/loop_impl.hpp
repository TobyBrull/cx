#pragma once

#include "async/chan_timer.hpp"
#include "async/endpoint.hpp"
#include "async/loop.hpp"
#include "async/loop_config.hpp"
#include "misc/error_cx.hpp"

namespace cx {

  template<loop_config LoopCfg, poller... Ps>
  class loop_impl final : public loop {
    LoopCfg loop_cfg_;
    std::tuple<Ps...> pollers_;

    virtual void* poller_ptr_impl(const std::type_info&) override final;

    virtual void pollers_format_json_stats_impl(string&) const override final;
    virtual result<> init_impl() override final;
    virtual result<> run_impl() override final;

    enum class exclude_last {
      YES = 0,
      NO  = 1,
    };

    template<exclude_last>
    poller_state poll_state() const;

   public:
    loop_impl(LoopCfg = {});
    virtual ~loop_impl() = default;

    std::tuple<Ps...>& pollers();

    virtual result<chan_timer_ptr>
        create_chan_timer(time, delegate<time(time)>) final override;

    virtual result<chan_stream_ptr> create_chan_stream(
        const endpoint_stream&, const chan_stream_config&) final override;
    virtual result<chan_stream_accepter_ptr> create_chan_stream_accepter(
        const endpoint_stream_accepter&,
        const chan_stream_accepter_config&) final override;

    virtual result<chan_message_ptr>
        create_chan_message(string_view) final override;
    virtual result<chan_message_accepter_ptr>
        create_chan_message_accepter(string_view) final override;
  };
}

// #include "loop_impl.ipp"

namespace cx {
  template<loop_config LoopCfg, poller... Ps>
  loop_impl<LoopCfg, Ps...>::loop_impl(LoopCfg loop_cfg)
    : loop_cfg_(std::move(loop_cfg)), pollers_{(ct_type<Ps>{}, *this)...}
  {
  }

  template<loop_config LoopCfg, poller... Ps>
  void* loop_impl<LoopCfg, Ps...>::poller_ptr_impl(const std::type_info& ti)
  {
    void* retval = nullptr;
    tuple_for_each(pollers_, [&retval, &ti]<poller P>(P& p) {
      if (typeid(P) == ti) {
        retval = &p;
      }
    });
    return retval;
  }

  template<loop_config LoopCfg, poller... Ps>
  std::tuple<Ps...>& loop_impl<LoopCfg, Ps...>::pollers()
  {
    return pollers_;
  }

  namespace detail {
    template<typename ChannelPtr, poller... Ps, typename F>
    result<ChannelPtr> loop_impl_create(std::tuple<Ps...>& pollers, F f)
    {
      optional<result<ChannelPtr>> retval = nullopt;

      tuple_for_each(pollers, [&retval, f]<poller P>(P& p) {
        if (retval.has_value()) {
          return;
        }
        auto cp = f(p);
        if (cp.is_error() &&
            cp.as_error() == errcat_poller_abstained_from_create) {
          return;
        }
        retval = std::move(cp);
      });

      if (retval.has_value()) {
        return std::move(retval).value();
      }
      else {
        return errcat_all_pollers_abstained_from_create;
      }
    }
  }

  template<loop_config LoopCfg, poller... Ps>
  result<chan_timer_ptr> loop_impl<LoopCfg, Ps...>::create_chan_timer(
      const time expiry, const delegate<time(time)> callback)
  {
    return detail::loop_impl_create<chan_timer_ptr>(
        pollers_, [expiry, callback]<poller P>(P& p) {
          return p.create_chan_timer(expiry, callback);
        });
  }

  template<loop_config LoopCfg, poller... Ps>
  result<chan_stream_ptr> loop_impl<LoopCfg, Ps...>::create_chan_stream(
      const endpoint_stream& ep, const chan_stream_config& cfg)
  {
    return detail::loop_impl_create<chan_stream_ptr>(
        pollers_,
        [&ep, &cfg]<poller P>(P& p) { return p.create_chan_stream(ep, cfg); });
  }

  template<loop_config LoopCfg, poller... Ps>
  result<chan_stream_accepter_ptr>
  loop_impl<LoopCfg, Ps...>::create_chan_stream_accepter(
      const endpoint_stream_accepter& ep,
      const chan_stream_accepter_config& cfg)
  {
    return detail::loop_impl_create<chan_stream_accepter_ptr>(
        pollers_, [ep, cfg]<poller P>(P& p) {
          return p.create_chan_stream_accepter(ep, cfg);
        });
  }

  template<loop_config LoopCfg, poller... Ps>
  result<chan_message_ptr>
  loop_impl<LoopCfg, Ps...>::create_chan_message(const string_view arg)
  {
    return detail::loop_impl_create<chan_message_ptr>(
        pollers_, [arg]<poller P>(P& p) { return p.create_chan_message(arg); });
  }

  template<loop_config LoopCfg, poller... Ps>
  result<chan_message_accepter_ptr>
  loop_impl<LoopCfg, Ps...>::create_chan_message_accepter(const string_view arg)
  {
    return detail::loop_impl_create<chan_message_accepter_ptr>(
        pollers_,
        [arg]<poller P>(P& p) { return p.create_chan_message_accepter(arg); });
  }

  template<loop_config LoopCfg, poller... Ps>
  void
  loop_impl<LoopCfg, Ps...>::pollers_format_json_stats_impl(string& out) const
  {
    tuple_for_each_indexed(
        pollers_, [&]<size_t I, poller P>(ct_value<I>, const P& p) {
          p.format_json_stats(out);
          if (I + 1 < sizeof...(Ps)) {
            out.append(",\n  ");
          }
        });
  }

  template<loop_config LoopCfg, poller... Ps>
  result<> loop_impl<LoopCfg, Ps...>::init_impl()
  {
    result<> retval = {};
    tuple_for_each_indexed(
        pollers_, [&]<size_t I, poller P>(ct_value<I>, P& p) {
          if (retval.is_error()) {
            return;
          }
          retval = p.init();
        });
    return retval;
  }

  template<loop_config LoopCfg, poller... Ps>
  template<typename loop_impl<LoopCfg, Ps...>::exclude_last exclude_last_v>
  poller_state loop_impl<LoopCfg, Ps...>::poll_state() const
  {
    poller_state retval = {};
    retval.accumulate(root_agent_poll_state());
    tuple_for_each_indexed(
        pollers_, [&]<size_t I, poller P>(ct_value<I>, const P& p) {
          constexpr bool is_last = (I + 1 == sizeof...(Ps));
          if ((exclude_last_v == exclude_last::YES) && is_last) {
            // ignore this poller
          }
          else {
            retval.accumulate(p.poll_state());
          }
        });
    return retval;
  }

  namespace detail {
    template<typename PollAndDispatch>
    void run_impl(string_view name, PollAndDispatch poll_and_dispatch)
    {
      auto res = poll_and_dispatch();
      if (res.is_error()) {
        fmt::print(
            "Error running poll_and_dispatch for {}: {}\n",
            name,
            std::move(res).as_error());
      }
    }
  }

  template<loop_config LoopCfg, poller... Ps>
  result<> loop_impl<LoopCfg, Ps...>::run_impl()
  {
    const auto tick = [&](const bool try_block) {
      update_tick_time();

      detail::run_impl(
          "agents", [&] { return root_agent_poll_and_dispatch(); });

      tuple_for_each_indexed(
          pollers_, [&]<size_t I, poller P>(ct_value<I>, P& p) {
            constexpr bool is_last = (I + 1 == sizeof...(Ps));

            if (is_last && try_block) {
              const poller_state ps = poll_state<exclude_last::YES>();
              detail::run_impl(p.name(), [&] {
                return p.poll_and_dispatch(ps.soonest_next_event);
              });
            }
            else {
              detail::run_impl(
                  p.name(), [&] { return p.poll_and_dispatch({}); });
            }
          });
    };

    while (true) {
      tick(loop_cfg_.try_block_in_last_poller);

      const poller_state ps = poll_state<exclude_last::NO>();
      if (ps.is_busy == false) {
        tick(false);
        if (root_agent_.children().empty()) {
          return {};
        }
        else {
          root_agent_.remove_this_agent();
          tick(false);
          return errcat_loop_still_had_agents;
        }
      }
    }
  }
}
