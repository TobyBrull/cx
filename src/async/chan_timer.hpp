#pragma once

#include "async/chan_ptr.hpp"
#include "async/fwd.hpp"
#include "misc/containers.hpp"
#include "misc/delegate.hpp"
#include "misc/time.hpp"

namespace cx {
  class chan_timer : public chan_ptr_target {
   protected:
    time expiry_;
    time updated_expiry_;
    delegate<time(time)> callback_;

    template<typename T>
      requires std::derived_from<T, chan_ptr_target>
    friend class chan_ptr;

    chan_timer(chan_timer_journal*, time, delegate<time(time)>);

   public:
    /* not virtual */ ~chan_timer() = default;

    void set_expiry(time);
    time expiry() const;

    void set_callback(delegate<time(time)>);
  };

  using chan_timer_ptr = chan_ptr<chan_timer>;

  struct chan_timer_journal : public chan_journal {
    using chan_journal::chan_journal;

    set<chan_timer*> updated_expiry;
  };
}

// #include "chan_timer.ipp"

namespace cx {
  inline chan_timer::chan_timer(
      chan_timer_journal* journal,
      const time expiry,
      const delegate<time(time)> callback)
    : chan_ptr_target(journal), expiry_(expiry), callback_(callback)
  {
    CX_ASSERT(journal_ != nullptr, "chan_timer::journal must never be nullptr");
  }

  inline void chan_timer::set_expiry(const time new_expiry)
  {
    if (new_expiry != expiry_) {
      static_cast<chan_timer_journal*>(journal_)->updated_expiry.insert(this);
      updated_expiry_ = new_expiry;
    }
  }

  inline time chan_timer::expiry() const
  {
    return expiry_;
  }

  inline void chan_timer::set_callback(delegate<time(time)> callback)
  {
    callback_ = callback;
  }
}
