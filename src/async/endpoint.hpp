#pragma once

#include "misc/containers.hpp"

namespace cx {
  class address_ipv4 {
    std::array<uint8_t, 4> data_ = {0};

   public:
    constexpr address_ipv4() = default;
    constexpr address_ipv4(std::array<uint8_t, 4>) noexcept;

    constexpr explicit address_ipv4(uint32_t) noexcept;
    constexpr explicit address_ipv4(string_view);

    constexpr static address_ipv4 loopback();
    constexpr static address_ipv4 any();

    constexpr uint32_t as_uint32() const;
    constexpr const std::array<uint8_t, 4>& data() const;

    constexpr auto operator<=>(address_ipv4 const&) const noexcept = default;
  };

  struct endpoint_ipv4 {
    address_ipv4 addr = {};
    uint16_t port     = 0;
  };

  class address_ipv6 {
    std::array<uint8_t, 16> data_ = {0};

   public:
    constexpr address_ipv6() = default;
    constexpr address_ipv6(std::array<uint8_t, 16>) noexcept;

    constexpr explicit address_ipv6(string_view);

    constexpr const std::array<uint8_t, 16>& data() const;

    constexpr auto operator<=>(address_ipv6 const&) const noexcept = default;
  };

  struct endpoint_ipv6 {
    address_ipv6 addr = {};
    uint16_t port     = 0;
  };

  struct endpoint_uds : public path {};

  struct endpoint_shm_queue {};

  struct endpoint_fd {
    int recv_fd = -1;
    int send_fd = -1;
  };

  struct endpoint_console {};

  using endpoint_stream = variant<
      endpoint_ipv4,
      endpoint_ipv6,
      endpoint_uds,
      endpoint_shm_queue,
      endpoint_fd,
      endpoint_console>;

  using endpoint_stream_accepter = variant<
      endpoint_ipv4,
      endpoint_ipv6,
      endpoint_uds,
      endpoint_shm_queue,
      endpoint_fd>;
}

// #include "endpoint.ipp"

namespace cx {
  constexpr address_ipv4::address_ipv4(
      const std::array<uint8_t, 4> data) noexcept
    : data_(data)
  {
  }

  constexpr address_ipv4::address_ipv4(const uint32_t x) noexcept
    // TODO: does this make sense?
    : address_ipv4({
          static_cast<uint8_t>((x >> 24) & 0xFF),
          static_cast<uint8_t>((x >> 16) & 0xFF),
          static_cast<uint8_t>((x >> 8) & 0xFF),
          static_cast<uint8_t>((x >> 0) & 0xFF),
      })
  {
  }

  constexpr address_ipv4 address_ipv4::loopback()
  {
    return address_ipv4{{127, 0, 0, 1}};
  }

  constexpr address_ipv4 address_ipv4::any()
  {
    return address_ipv4{{0, 0, 0, 0}};
  }

  constexpr uint32_t address_ipv4::as_uint32() const
  {
    // TODO: does this make sense?
    return (static_cast<uint32_t>(data_[0]) << 24) |
        (static_cast<uint32_t>(data_[1]) << 16) |
        (static_cast<uint32_t>(data_[2]) << 8) |
        (static_cast<uint32_t>(data_[3]) << 0);
  }

  constexpr const std::array<uint8_t, 4>& address_ipv4::data() const
  {
    return data_;
  }

  constexpr const std::array<uint8_t, 16>& address_ipv6::data() const
  {
    return data_;
  }

}
