#pragma once

#include <coroutine>
#include <utility>

namespace cx {
  template<typename Result, typename Derived>
  struct coro_promise_accesser {
    Result value = Result{};

    constexpr bool await_ready() noexcept
    {
      return false;
    }

    template<typename Promise>
    constexpr bool await_suspend(std::coroutine_handle<Promise> h) noexcept
    {
      value = static_cast<Derived*>(this)->on_access(h.promise());
      return false;
    };

    constexpr Result await_resume() noexcept
    {
      return std::move(value);
    }
  };
}
