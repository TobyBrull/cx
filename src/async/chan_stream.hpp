#pragma once

#include "async/chan_ptr.hpp"
#include "misc/buffer.hpp"
#include "misc/delegate.hpp"
#include "misc/fixed_address.hpp"

namespace cx {
  enum class chan_stream_state {
    opening     = 1,
    established = 2,
    closed      = 3,
    failed      = 4,
  };
  constexpr bool is_finished(chan_stream_state);

  enum class chan_stream_event : uint8_t {
    state       = 0b001,
    send_buffer = 0b010,
    recv_buffer = 0b100,
  };

  struct chan_stream_config {
    buffer_config send_buffer_config = {};
    buffer_config recv_buffer_config = {};
    delegate<void(chan_stream_event, chan_stream*)> callback;
  };

  class chan_stream : public chan_ptr_target {
   protected:
    buffer<byte> send_buffer_;
    buffer<byte> recv_buffer_;
    chan_stream_state state_ = chan_stream_state::opening;
    delegate<void(chan_stream_event, chan_stream*)> callback_;

    void set_state(chan_stream_state new_state);

    template<typename T>
      requires std::derived_from<T, chan_ptr_target>
    friend class chan_ptr;

    chan_stream(chan_stream_journal*, const chan_stream_config&);

   public:
    ~chan_stream() = default;

    auto& send_buffer();
    auto& recv_buffer();

    const auto& send_buffer() const;
    const auto& recv_buffer() const;

    // Convenience wrapper for send_buffer().write()
    result<> write(span<const byte>);

    chan_stream_state state() const;
    void mark_written();
    void close();
    void fail();

    void set_callback(delegate<void(chan_stream_event, chan_stream*)>);
  };

  using chan_stream_ptr = chan_ptr<chan_stream>;

  struct chan_stream_journal : public chan_journal {
    using chan_journal::chan_journal;

    set<chan_stream*> written;
    set<chan_stream*> closed;
    set<chan_stream*> failed;
  };
}

// #include "chan_stream.ipp"

// chan_stream_state

template<>
struct fmt::formatter<cx::chan_stream_state>
  : public fmt::formatter<cx::string> {
  template<typename FormatContext>
  auto format(const cx::chan_stream_state& x, FormatContext& ctx)
  {
    switch (x) {
      case cx::chan_stream_state::opening:
        return fmt::format_to(ctx.out(), "opening");
      case cx::chan_stream_state::established:
        return fmt::format_to(ctx.out(), "established");
      case cx::chan_stream_state::closed:
        return fmt::format_to(ctx.out(), "closed");
      case cx::chan_stream_state::failed:
        return fmt::format_to(ctx.out(), "failed");
    }
    cx::unreachable();
  }
};

namespace cx {
  // chan_stream_state

  constexpr bool is_finished(const chan_stream_state s)
  {
    switch (s) {
      case chan_stream_state::opening:
      case chan_stream_state::established:
        return false;
      case chan_stream_state::closed:
      case chan_stream_state::failed:
        return true;
    }
    cx::unreachable();
  }

  // chan_stream

  inline chan_stream::chan_stream(
      chan_stream_journal* const journal, const chan_stream_config& cfg)
    : chan_ptr_target(journal)
    , send_buffer_(cfg.send_buffer_config)
    , recv_buffer_(cfg.recv_buffer_config)
    , callback_(cfg.callback)
  {
    CX_ASSERT(
        journal_ != nullptr, "chan_stream::journal must never be nullptr");
  }

  inline void chan_stream::set_state(const chan_stream_state new_state)
  {
    CX_ASSERT(new_state != chan_stream_state::opening);

    if (is_finished(state_)) {
      CX_ASSERT(new_state != chan_stream_state::established);
      return;
    }

    if (state_ != new_state) {
      state_ = new_state;
      if (is_referenced()) {
        callback_(chan_stream_event::state, this);
      }
    }
  }

  inline auto& chan_stream::send_buffer()
  {
    return send_buffer_;
  }
  inline auto& chan_stream::recv_buffer()
  {
    return recv_buffer_;
  }

  inline const auto& chan_stream::send_buffer() const
  {
    return send_buffer_;
  }
  inline const auto& chan_stream::recv_buffer() const
  {
    return recv_buffer_;
  }

  inline result<> chan_stream::write(const span<const byte> ws)
  {
    CX_TRY(send_buffer().write(ws));
    return {};
  }

  inline void chan_stream::mark_written()
  {
    static_cast<chan_stream_journal*>(journal_)->written.insert(this);
  }
  inline void chan_stream::close()
  {
    static_cast<chan_stream_journal*>(journal_)->closed.insert(this);
  }
  inline void chan_stream::fail()
  {
    static_cast<chan_stream_journal*>(journal_)->failed.insert(this);
  }
  inline chan_stream_state chan_stream::state() const
  {
    return state_;
  }

  inline void chan_stream::set_callback(
      const delegate<void(chan_stream_event, chan_stream*)> callback)
  {
    callback_ = callback;
  }
}
