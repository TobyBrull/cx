#pragma once

#include "async/coro.hpp"

namespace cx {
  template<typename... Ts>
  coro<tuple<Ts...>> coro_in_parallel(coro<Ts>...);
}

// #include "coro_in_parallel.ipp"

namespace cx {
  namespace detail {
    template<typename... Ts>
    struct coro_in_parallel_state : public std::suspend_always {
      tuple<optional<Ts>...> result  = {};
      int remaining                  = sizeof...(Ts);
      std::coroutine_handle<> handle = nullptr;

      void await_suspend(std::coroutine_handle<> h)
      {
        handle = h;
      }

      void resume()
      {
        handle.resume();
      }
    };

    template<size_t I, typename T, typename... Ts>
    coro<>
    coro_in_parallel_wrapper(coro_in_parallel_state<Ts...>* state, coro<T> c)
    {
      std::get<I>(state->result) = co_await c;
      state->remaining -= 1;
      if (state->remaining == 0) {
        state->resume();
      }
    }

    template<typename... Ts, size_t... Is>
    auto wrapped_coros_tuple(
        std::index_sequence<Is...>,
        coro_in_parallel_state<Ts...>* state,
        coro<Ts>... coros)
    {
      return std::tuple{
          coro_in_parallel_wrapper<Is, Ts, Ts...>(state, std::move(coros))...};
    }
  }

  template<typename... Ts>
  coro<tuple<Ts...>> coro_in_parallel(coro<Ts>... coros)
  {
    CX_ASSERT(
        (coros.is_in_initial_suspend() && ...),
        "all coros must be in initial-suspend");

    coro_journal* journal = co_await detail::coro_get_journal{};

    detail::coro_in_parallel_state<Ts...> state{};

    auto coro_store = detail::wrapped_coros_tuple(
        std::make_index_sequence<sizeof...(Ts)>(), &state, std::move(coros)...);

    tuple_for_each(coro_store, [journal](coro<>& wrapped_coro) {
      wrapped_coro.handle().promise().journal_ = journal;
      wrapped_coro.handle().resume();
    });

    co_await state;

    co_return tuple_transform(
        std::move(state.result), [](auto x) { return x.value(); });
  }
}
