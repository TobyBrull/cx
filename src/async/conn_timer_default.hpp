#pragma once

#include "async/conn_timer.hpp"

namespace cx {
  enum class conn_timer_repeat_style {
    // Chooses the next expiry as `cx::loop::tick_time() +
    // cx::conn_timer_repeat::interval`.
    FROM_LOOP_TICK_TIME,

    // Chooses the next expiry as `cx::time::now() +
    // cx::conn_timer_repeat::interval`.
    FROM_TIME_NOW,

    // Increments the previous expiry by `cx::conn_timer_repeat::interval`
    // until
    // the new expiry is greater than the current `cx::loop::tick_time`.
    FROM_PREVIOUS_EXPIRY,
  };

  struct conn_timer_repeat {
    duration interval;
    uint32_t count = 0; // 0 means infinitely
    conn_timer_repeat_style repeat_style =
        conn_timer_repeat_style::FROM_LOOP_TICK_TIME;
  };

  struct conn_timer_default_config {
    variant<duration, time> expiry     = duration{};
    optional<conn_timer_repeat> repeat = nullopt;
  };

  class conn_timer_default final : public conn_timer<conn_timer_default> {
    using conn_base = conn_timer<conn_timer_default>;

    loop& loop_;
    conn_timer_default_config cfg_;
    delegate<void()> f_;

    enum class new_status {
      REPEATS,
      COMPLETED,
    };
    new_status update();

    friend conn_base;
    time on_conn_timer(time);

   public:
    conn_timer_default(
        conn_journal*,
        chan_timer_ptr,
        const conn_timer_default_config&,
        delegate<void()>);
  };
}

// #include "conn_timer.ipp"

namespace cx {
}
