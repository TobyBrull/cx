#include "coro_stream.hpp"

#include "misc/delegate.hpp"

namespace cx {

  void coro_stream::on_stream(chan_stream_event ev, chan_stream* s)
  {
    if (ev == chan_stream_event::state) {
      if (s->state() == chan_stream_state::established) {
        return;
      }
    }

    if (to_resume_) {
      to_resume_.resume();
    }

    return;
  }
}
