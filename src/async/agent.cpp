#include "agent.hpp"

#include "async/loop.hpp"

namespace cx {

  // Invariant of agent tree:
  //
  //  - If an agent node in a tree is ON_START_PENDING, all children are also
  //  ON_START_PENDING.
  //
  //  - If an agent node in a tree is ON_START_DONE, all parents are also
  //  ON_START_DONE.
  //
  //  - If an agent node in a tree is ON_STOP_PENDING, all children are also
  //  ON_STOP_PENDING.

  result<poller_result> agent::poll_and_dispatch()
  {
    poller_result retval;

    if (state_ == agent_state::ON_START_PENDING) {
      auto res = on_start();
      if (res.is_error()) {
        fmt::print(
            "Error starting agent {}: {}\n", name_, std::move(res).as_error());
      }
      state_ = agent_state::ON_START_DONE;
      retval.num_processed_events += 1;
    }

    for (auto it = children_.begin(); it != children_.end();) {
      retval.accumulate(CX_TRY(it->second->poll_and_dispatch()));
      if (it->second->state_ == agent_state::ON_STOP_DONE) {
        it = children_.erase(it);
      }
      else {
        ++it;
      }
    }

    retval.accumulate(CX_TRY(manager_.poll_and_dispatch()));

    if (state_ == agent_state::ON_STOP_PENDING) {
      on_stop();
      state_ = agent_state::ON_STOP_DONE;
      retval.num_processed_events += 1;
    }

    return retval;
  }

  poller_state agent::poll_state() const
  {
    for (auto it = children_.begin(); it != children_.end(); ++it) {
      const poller_state child_state = it->second->poll_state();
      if (child_state.is_busy) {
        return poller_state::busy();
      }
    }

    return manager_.poll_state();
  }

  result<> agent::add_child_agent_impl(string name, std::unique_ptr<agent> ptr)
  {
    if (state_ == agent_state::ON_STOP_PENDING) {
      return errcat_tryed_to_add_agent_to_removed_agent;
    }

    ptr->name_ = name;

    children_.emplace(std::move(name), std::move(ptr));

    return {};
  }

  void agent::remove_this_agent_impl()
  {
    switch (state_) {
      case agent_state::ON_START_PENDING:
        for (const auto& [name, child]: children_) {
          child->remove_this_agent_impl();
        }
        children_.clear();
        break;
      case agent_state::ON_START_DONE:
        for (const auto& [name, child]: children_) {
          child->remove_this_agent_impl();
        }
        state_ = agent_state::ON_STOP_PENDING;
        break;
      case agent_state::ON_STOP_PENDING:
      case agent_state::ON_STOP_DONE:
        break;
    }
  }

  void agent::remove_this_agent()
  {
    remove_this_agent_impl();
  }

  void agent::format_json_stats(string& out, const int indent) const
  {
    out.insert(out.end(), indent, ' ');
    fmt::format_to(
        std::back_inserter(out),
        "{{ \"name\": \"{}\", \"num-conns\": {}, \"num-coros\": {}, "
        "\"children\": [",
        name_,
        manager_.conns.size(),
        manager_.coros.num_coros());

    bool first = true;
    for (const auto& [name, child]: children_) {
      if (first) {
        out.append("\n");
        first = false;
      }
      else {
        out.append(",\n");
      }
      child->format_json_stats(out, indent + 2);
    }

    if (children_.empty()) {
      out.append("] }");
    }
    else {
      out.append("\n");
      out.insert(out.end(), indent, ' ');
      out.append("] }");
    }
  }
}
