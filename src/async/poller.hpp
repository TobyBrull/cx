#pragma once

#include "async/chan_accepter.hpp"
#include "async/chan_message.hpp"
#include "async/chan_ptr.hpp"
#include "async/chan_stream.hpp"
#include "async/chan_timer.hpp"
#include "async/endpoint.hpp"
#include "misc/result.hpp"
#include "misc/time.hpp"

#include <concepts>

namespace cx {
  struct poller_state {
    bool is_busy = false;

    time soonest_next_event = time::max();

    constexpr static poller_state busy();

    void accumulate(const poller_state&);
  };

  struct poller_result {
    size_t num_processed_events = 0;

    void accumulate(const poller_result&);
  };

  // Will usually contain a member of type "loop&" and are therefore in general
  // assumed to be non-copyable and non-movable.
  template<typename Poller>
  concept poller =
      requires(Poller poller, const Poller const_poller, time may_block_till) {
        {
          Poller{std::declval<loop&>()}
        }
        // The "noexcept" is not strictly necessary here; it is only here to
        // emphasize that pollers should not perform operations that can fail in
        // the constructor (e.g., epoll_create should happen in
        // "poller_epoll::init()", not in the constructor).
        noexcept;

        {
          poller.init()
          } -> std::same_as<result<>>;
        {
          const_poller.name()
          } -> std::same_as<string_view>;
        {
          poller.poll_and_dispatch(may_block_till)
          } -> std::same_as<result<poller_result>>;
        {
          const_poller.poll_state()
          } -> std::same_as<poller_state>;
        {
          const_poller.format_json_stats(std::declval<string&>())
        };
        {
          poller.create_chan_timer(time{}, delegate<time(time)>{})
          } -> std::convertible_to<result<chan_timer_ptr>>;
        {
          poller.create_chan_stream(
              std::declval<endpoint_stream>(), chan_stream_config{})
          } -> std::convertible_to<result<chan_stream_ptr>>;
        {
          poller.create_chan_stream_accepter(
              std::declval<endpoint_stream_accepter>(),
              chan_stream_accepter_config{})
          } -> std::convertible_to<result<chan_stream_accepter_ptr>>;
        {
          poller.create_chan_message("")
          } -> std::convertible_to<result<chan_message_ptr>>;
        {
          poller.create_chan_message_accepter("")
          } -> std::convertible_to<result<chan_message_accepter_ptr>>;
      };

  CX_DEFINE_ERROR_CATEGORY(all_pollers_abstained_from_create, 1010);
  CX_DEFINE_ERROR_CATEGORY(poller_abstained_from_create, 1011);

  struct poller_base {
    result<chan_timer_ptr> create_chan_timer(time, delegate<time(time)>)
    {
      return errcat_poller_abstained_from_create;
    }

    result<chan_stream_ptr>
    create_chan_stream(const endpoint_stream&, const chan_stream_config&)
    {
      return errcat_poller_abstained_from_create;
    }

    result<chan_stream_accepter_ptr> create_chan_stream_accepter(
        const endpoint_stream_accepter&, const chan_stream_accepter_config&)
    {
      return errcat_poller_abstained_from_create;
    }

    result<chan_message_ptr> create_chan_message(string_view)
    {
      return errcat_poller_abstained_from_create;
    }

    result<chan_message_accepter_ptr> create_chan_message_accepter(string_view)
    {
      return errcat_poller_abstained_from_create;
    }
  };
}

// #include "poller.ipp"

namespace cx {

  constexpr poller_state poller_state::busy()
  {
    return poller_state{
        .is_busy            = true,
        .soonest_next_event = time::min(),
    };
  }

  inline void poller_state::accumulate(const poller_state& other)
  {
    if (other.is_busy) {
      is_busy = other.is_busy;
    }

    soonest_next_event = std::min(soonest_next_event, other.soonest_next_event);
  }

  inline void poller_result::accumulate(const poller_result& other)
  {
    num_processed_events += other.num_processed_events;
  }
}
