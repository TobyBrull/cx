#pragma once

#include "async/chan_stream.hpp"
#include "async/coro.hpp"
#include "async/loop.hpp"

namespace cx {

  class coro_stream : public fixed_address {
    chan_stream_ptr stream_;
    std::coroutine_handle<> to_resume_ = nullptr;

    void on_stream(chan_stream_event, chan_stream*);

   public:
    coro_stream() = default;
    coro_stream(chan_stream_ptr);

    chan_stream_ptr release() &&;

    struct read_some_awaiter;
    struct read_n_awaiter;

    // co_await coro_stream.read_...() -> span<const byte>
    // The returned span is valid to the next suspend point.
    read_some_awaiter read_some();
    read_n_awaiter read_n(size_t n);

    struct write_awaiter;
    write_awaiter write(span<const byte>);
  };
}

// #include "coro_stream.ipp"

namespace cx {
  inline coro_stream::coro_stream(chan_stream_ptr stream)
    : stream_(std::move(stream))
  {
    CX_ASSERT(stream_, "coro_stream needs valid chan_stream_ptr");
    stream_->set_callback(make_delegate<&coro_stream::on_stream>(this));
  }

  inline chan_stream_ptr coro_stream::release() &&
  {
    stream_->set_callback({});
    return std::move(stream_);
  }

  // read_some_awaiter

  struct coro_stream::read_some_awaiter {
    coro_stream* stream_ = nullptr;
    read_some_awaiter(coro_stream* stream) : stream_(stream) {}

    bool await_ready()
    {
      auto& rbuf = stream_->stream_->recv_buffer();
      return !rbuf.is_empty();
    }

    void await_suspend(std::coroutine_handle<> h)
    {
      stream_->to_resume_ = h;
    }

    span<const byte> await_resume()
    {
      stream_->to_resume_ = nullptr;

      auto& rbuf  = stream_->stream_->recv_buffer();
      auto retval = rbuf.read_span();
      rbuf.mark_read(retval.size());
      return retval;
    }
  };

  inline coro_stream::read_some_awaiter coro_stream::read_some()
  {
    return read_some_awaiter(this);
  }

  // write_awaiter

  struct coro_stream::write_awaiter {
    coro_stream* stream_   = nullptr;
    span<const byte> data_ = {};
    write_awaiter(coro_stream* stream, span<const byte> data)
      : stream_(stream), data_(data)
    {
    }

    bool await_ready()
    {
      auto& sbuf = stream_->stream_->send_buffer();
      return sbuf.write_span().size() >= data_.size();
    }

    void await_suspend(std::coroutine_handle<> h)
    {
      stream_->to_resume_ = h;
    }

    void await_resume()
    {
      stream_->to_resume_ = nullptr;

      auto& sbuf = stream_->stream_->send_buffer();
      sbuf.write(data_);
      stream_->stream_->mark_written();
    };
  };

  inline coro_stream::write_awaiter
  coro_stream::write(const span<const byte> data)
  {
    return write_awaiter(this, data);
  }
}
