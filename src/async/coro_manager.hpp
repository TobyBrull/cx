#pragma once

#include "async/coro_type_erased.hpp"
#include "async/poller.hpp"

namespace cx {
  class coro_manager {
    map<void*, coro_type_erased> coro_set_;

    friend struct coro_yield_t;

    coro_journal journal_;

   public:
    coro_manager(agent*);

    size_t num_coros() const;
    size_t num_yielding() const;

    template<typename T>
    void add_coro(coro<T>);

    result<poller_result> poll_and_dispatch();

    poller_state poll_state() const;
  };

  struct coro_yield;
}

// #include "coro_manager.ipp"

namespace cx {
  inline coro_manager::coro_manager(agent* owner) : journal_(owner) {}

  inline size_t coro_manager::num_coros() const
  {
    return coro_set_.size();
  }
  inline size_t coro_manager::num_yielding() const
  {
    return journal_.yielding.size();
  }

  template<typename T>
  void coro_manager::add_coro(coro<T> t)
  {
    CX_REQUIRE(t.is_valid() && t.is_in_initial_suspend());

    const auto handle = t.handle();

    [[maybe_unused]] auto [it, inserted] =
        coro_set_.emplace(handle.address(), std::move(t));
    CX_ASSERT(inserted);

    handle.promise().journal_    = &journal_;
    handle.promise().is_managed_ = true;

    // Run to first "real" suspend point (i.e., not initial suspend).
    handle.resume();
  }

  inline poller_state coro_manager::poll_state() const
  {
    if (num_coros() > 0) {
      return poller_state::busy();
    }

    return poller_state{};
  }

  inline result<poller_result> coro_manager::poll_and_dispatch()
  {
    poller_result retval;

    {
      auto copy = std::move(journal_.finished);
      retval.num_processed_events += copy.size();
      journal_.finished.clear();
      for (const auto h: copy) {
        coro_set_.erase(h.address());
      }
    }

    {
      auto copy = std::move(journal_.yielding);
      retval.num_processed_events += copy.size();
      journal_.yielding.clear();
      for (const auto h: copy) {
        h.resume();
      }
    }

    return retval;
  }

  // coro_yield

  struct coro_yield : public std::suspend_always {
    template<typename Promise>
    void await_suspend(std::coroutine_handle<Promise> h) const noexcept
    {
      h.promise().journal_->yielding.push_back(h);
    }
  };
}
