#pragma once

#include "misc/containers.hpp"

#include <concepts>
#include <memory>

namespace cx {
  class loop;

  class agent;

  // chan

  class chan_ptr_target;

  template<typename T>
    requires std::derived_from<T, chan_ptr_target>
  class chan_ptr;
  struct chan_journal;

  class chan_timer;
  struct chan_timer_journal;

  class chan_stream;
  struct chan_stream_journal;

  class chan_stream_accepter;
  struct chan_stream_accepter_journal;

  class chan_message;
  struct chan_message_journal;

  class chan_message_accepter;
  struct chan_message_accepter_journal;

  // coro

  namespace detail {
    template<typename T>
    struct coro_promise;
  }

  template<typename T = void>
  class coro;

  struct coro_journal;

  class coro_type_erased;

  class coro_manager;

  // conn

  class conn;

  struct conn_journal;

  class conn_manager;
}
