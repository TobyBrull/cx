#include "poller_timer_simple.hpp"

#include "async/chan_ptr.hpp"

namespace cx {
  poller_timer_simple::chan_timer_impl::chan_timer_impl(
      poller_timer_simple* const owner,
      const time expiry,
      const delegate<time(time)> callback)
    : chan_timer(&(owner->journal_), expiry, callback), owner_(owner)
  {
  }

  bool poller_timer_simple::deref_expiry::operator()(
      const chan_timer_impl* const lhs, const chan_timer_impl* const rhs) const
  {
    return lhs->expiry() < rhs->expiry();
  }

  void poller_timer_simple::insert_impl(std::unique_ptr<chan_timer_impl> ptr)
  {
    auto* p = ptr.get();
    if (ptr->expiry_) {
      timers_live_sorted_.emplace(p, std::move(ptr));
    }
    else {
      timers_dead_.emplace(p, std::move(ptr));
    }
  }

  std::unique_ptr<poller_timer_simple::chan_timer_impl>
  poller_timer_simple::remove_refed_impl(
      poller_timer_simple::chan_timer_impl* p)
  {
    if (p->expiry()) {
      const auto [beg_it, end_it] = timers_live_sorted_.equal_range(p);
      auto it                     = beg_it;
      for (; it != end_it; ++it) {
        if (it->first == p) {
          break;
        }
      }
      CX_ASSERT(it != end_it);
      auto retval = std::move(it->second);
      timers_live_sorted_.erase(it);
      return retval;
    }
    else {
      const auto it = timers_dead_.find(p);
      CX_ASSERT(it != timers_dead_.end());
      auto retval = std::move(it->second);
      timers_dead_.erase(it);
      return retval;
    }
  }

  result<> poller_timer_simple::init()
  {
    return {};
  }

  result<poller_result> poller_timer_simple::poll_and_dispatch(time)
  {
    const auto handle_journal = [&] {
      for (chan_ptr_target* pp: journal_.reference_gone) {
        auto* p = static_cast<chan_timer_impl*>(pp);
        journal_.updated_expiry.erase(p);
        auto ptr = remove_refed_impl(p);
        ptr.reset();
      }
      journal_.reference_gone.clear();

      for (chan_timer* pp: journal_.updated_expiry) {
        auto* p      = static_cast<chan_timer_impl*>(pp);
        auto ptr     = remove_refed_impl(p);
        ptr->expiry_ = ptr->updated_expiry_;
        insert_impl(std::move(ptr));
      }
      journal_.updated_expiry.clear();
    };
    handle_journal();

    poller_result retval;
    while (!timers_live_sorted_.empty() &&
           timers_live_sorted_.begin()->second->expiry() <= loop_.tick_time()) {
      auto cur = std::move(timers_live_sorted_.begin()->second);
      timers_live_sorted_.erase(timers_live_sorted_.begin());

      const time new_expiry = cur->callback_(cur->expiry_);

      retval.num_processed_events += 1;

      cur->expiry_ = new_expiry;
      insert_impl(std::move(cur));
    }

    handle_journal();

    return retval;
  }

  poller_state poller_timer_simple::poll_state() const
  {
    poller_state retval;
    if (!timers_live_sorted_.empty()) {
      retval.is_busy            = true;
      retval.soonest_next_event = timers_live_sorted_.begin()->second->expiry();
    }
    return retval;
  }

  result<chan_ptr<poller_timer_simple::chan_timer_impl>>
  poller_timer_simple::create_chan_timer(
      const time expiry, const delegate<time(time)> callback)
  {
    auto [tgt, cp] =
        chan_ptr_target::make<chan_timer_impl>(this, expiry, callback);
    insert_impl(std::move(tgt));
    return {std::move(cp)};
  }

  string_view poller_timer_simple::name() const
  {
    return "timer_simple";
  }

  void poller_timer_simple::format_json_stats(string& out) const
  {
    fmt::format_to(
        std::back_inserter(out),
        "{{\"name\": \"poller_timer_simple\", \"live-refed\": [\n");

    for (const auto& [k, v]: timers_live_sorted_) {
      fmt::format_to(std::back_inserter(out), "    {}\n", k->expiry());
    }

    fmt::format_to(
        std::back_inserter(out),
        "  ], \"dead-refed\": {} }}",
        timers_dead_.size());
  }

  poller_timer_simple::poller_timer_simple(loop& loop) noexcept
    : loop_(loop), journal_(&loop_)
  {
  }
}
