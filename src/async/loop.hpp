#pragma once

#include "async/agent.hpp"
#include "async/endpoint.hpp"
#include "async/fwd.hpp"
#include "async/poller.hpp"
#include "misc/containers.hpp"
#include "misc/ct_type.hpp"
#include "misc/delegate.hpp"
#include "misc/fixed_address.hpp"
#include "misc/result.hpp"
#include "misc/time.hpp"
#include "misc/tuple.hpp"

#include <memory>

namespace cx {

  CX_DEFINE_ERROR_CATEGORY(loop_still_had_agents, 1000);
  CX_DEFINE_ERROR_CATEGORY(no_such_poller, 1001);

  class loop : public fixed_address {
    time tick_time_ = time::min();

    bool is_inited_  = false;
    bool is_running_ = false;

    virtual void* poller_ptr_impl(const std::type_info&) = 0;

    virtual void pollers_format_json_stats_impl(string&) const = 0;
    virtual result<> init_impl()                               = 0;
    virtual result<> run_impl()                                = 0;

   protected:
    agent root_agent_;
    result<poller_result> root_agent_poll_and_dispatch();
    poller_state root_agent_poll_state() const;

    void update_tick_time();

   public:
    loop();
    virtual ~loop();

    bool is_running() const;

    time tick_time() const;

    agent& root_agent();

    // Polymorphic channel creation

    virtual result<chan_timer_ptr>
    create_chan_timer(time expiry = {}, delegate<time(time)> callback = {}) = 0;

    virtual result<chan_stream_ptr>
    create_chan_stream(const endpoint_stream&, const chan_stream_config&) = 0;

    virtual result<chan_stream_accepter_ptr> create_chan_stream_accepter(
        const endpoint_stream_accepter&,
        const chan_stream_accepter_config&) = 0;

    virtual result<chan_message_ptr> create_chan_message(string_view) = 0;

    virtual result<chan_message_accepter_ptr>
        create_chan_message_accepter(string_view) = 0;

    // Event-loop state

    string format_json_stats() const;

    result<> init();

    result<> run();

    /// Scheduled
    void stop(duration grace_period, bool with_error = false);

    /// Scheduled
    void pause();

    // Poller

    template<poller P>
    result<P*> poller_ptr();
  };
}

// #include "loop.ipp"

namespace cx {
  inline bool loop::is_running() const
  {
    return is_running_;
  }

  inline result<poller_result> loop::root_agent_poll_and_dispatch()
  {
    return root_agent_.poll_and_dispatch();
  }
  inline poller_state loop::root_agent_poll_state() const
  {
    return root_agent_.poll_state();
  }

  inline void loop::update_tick_time()
  {
    const time exact_new_tick_time = time::now();
    if (tick_time_ < exact_new_tick_time) {
      tick_time_ = exact_new_tick_time;
    }
    else {
      tick_time_ = tick_time_ + duration::nanoseconds(1);
    }
  }

  inline time loop::tick_time() const
  {
    return tick_time_;
  }

  inline agent& loop::root_agent()
  {
    return root_agent_;
  }

  template<poller P>
  result<P*> loop::poller_ptr()
  {
    void* const ptr = poller_ptr_impl(typeid(P));
    if (ptr != nullptr) {
      return static_cast<P*>(ptr);
    }
    else {
      return errcat_no_such_poller;
    }
  }
}
