#pragma once

#include "async/chan_timer.hpp"
#include "async/conn.hpp"

namespace cx {
  template<typename Derived>
  class conn_timer : public conn {
    time on_timer(time);

   protected:
    chan_timer_ptr ptr_;

   public:
    conn_timer(conn_journal*, chan_timer_ptr);
  };
}

// #include "conn_timer.ipp"

namespace cx {

  // conn_timer

  template<typename Derived>
  time conn_timer<Derived>::on_timer(const time expiry)
  {
    return static_cast<Derived*>(this)->on_conn_timer(expiry);
  }

  template<typename Derived>
  conn_timer<Derived>::conn_timer(conn_journal* journal, chan_timer_ptr ptr)
    : conn(journal), ptr_(std::move(ptr))
  {
    ptr_->set_callback(make_delegate<&conn_timer<Derived>::on_timer>(this));
  }
}
