#pragma once

#include "misc/ct_literals.hpp"

#include <concepts>
#include <type_traits>

namespace cx {

  template<typename LoopConfig>
  concept loop_config = requires(LoopConfig loop_cfg) {
                          {
                            loop_cfg.try_block_in_last_poller
                            } -> std::convertible_to<bool>;
                        };

  struct loop_config_default {
    ct_value<true> try_block_in_last_poller;
  };

  struct loop_config_fast {
    ct_value<false> try_block_in_last_poller;
  };
}
