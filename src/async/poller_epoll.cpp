#include "poller_epoll.hpp"

#include "async/chan_accepter.hpp"
#include "misc/buffer.hpp"
#include "misc/error_posix.hpp"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>

#include <variant>

namespace cx {

  // chan_stream_accepter_impl

  poller_epoll::chan_stream_accepter_impl::chan_stream_accepter_impl(
      poller_epoll* const owner,
      const int accepter_fd,
      const chan_stream_config& accepted_cfg,
      const delegate<void(chan_stream_ptr)> callback)
    : chan_stream_accepter(
          &(owner->stream_accepters_journal_), accepted_cfg, callback)
    , owner_(owner)
    , accepter_fd_(accepter_fd)
  {
  }

  result<> poller_epoll::chan_stream_accepter_impl::epoll_register()
  {
    struct ::epoll_event ev;
    ev.events   = EPOLLIN | EPOLLOUT | EPOLLET;
    ev.data.ptr = this;

    [[maybe_unused]] const int res = CX_TRY_POSIX(
        ::epoll_ctl(owner_->epoll_fd_, EPOLL_CTL_ADD, accepter_fd_, &ev));
    return {};
  }

  result<> poller_epoll::chan_stream_accepter_impl::trigger()
  {
    while (true) {
      struct ::sockaddr_in cli_addr;
      unsigned int socklen = sizeof(cli_addr);
      const int res        = ::accept4(
          accepter_fd_,
          (struct ::sockaddr*)&cli_addr,
          &socklen,
          SOCK_NONBLOCK | SOCK_CLOEXEC);
      if (res == -1) {
        if (errno == EWOULDBLOCK) {
          break;
        }
        else {
          return errcat_from_posix_errno(errno);
        }
      }
      else {
        const int accepted_fd = res;
        auto [tgt, cp] = chan_ptr_target::make<poller_epoll::chan_stream_impl>(
            owner_, accepted_fd, accepted_stream_cfg_);
        CX_TRY(tgt->epoll_register());
        owner_->streams_.insert(std::move(tgt));
        callback_(std::move(cp));
      }
    }
    return {};
  }

  poller_epoll::chan_stream_accepter_impl::~chan_stream_accepter_impl()
  {
    if (accepter_fd_ != -1) {
      ::epoll_ctl(owner_->epoll_fd_, EPOLL_CTL_DEL, accepter_fd_, nullptr);
      ::close(accepter_fd_);
      accepter_fd_ = -1;
    }
  }

  // chan_stream_impl

  poller_epoll::chan_stream_impl::chan_stream_impl(
      poller_epoll* const owner, const int fd, const chan_stream_config& cfg)
    : chan_stream(&(owner->streams_journal_), cfg), owner_(owner), fd_(fd)
  {
  }

  result<> poller_epoll::chan_stream_impl::epoll_register()
  {
    struct ::epoll_event ev;
    ev.events   = EPOLLIN | EPOLLOUT | EPOLLET;
    ev.data.ptr = this;

    [[maybe_unused]] const int res =
        CX_TRY_POSIX(::epoll_ctl(owner_->epoll_fd_, EPOLL_CTL_ADD, fd_, &ev));
    return {};
  }

  result<> poller_epoll::chan_stream_impl::trigger()
  {
    if (is_finished(state_)) {
      return {};
    }
    while (true) {
      auto ws = recv_buffer_.prepare_for_write();
      if (ws.empty()) {
        break;
      }
      const int res = ::recv(fd_, ws.data(), ws.size(), 0);
      if (res == -1) {
        if (errno == EWOULDBLOCK) {
          break;
        }
        else if (errno == ECONNREFUSED) {
          set_state(chan_stream_state::failed);
          return {};
        }
        else {
          return errcat_from_posix_errno(errno);
        }
      }
      else {
        if (res == 0) {
          set_state(chan_stream_state::closed);
          return {};
        }
        else {
          set_state(chan_stream_state::established);
          recv_buffer_.mark_written(res);
        }
      }
    }

    if (!recv_buffer_.is_empty()) {
      callback_(chan_stream_event::recv_buffer, this);
    }

    const auto rs = send_buffer_.read_span();
    if (rs.empty() == false) {
      const int res = ::send(fd_, rs.data(), rs.size(), 0);
      if (res == -1) {
        if (errno == EWOULDBLOCK) {
          ;
        }
        else {
          return errcat_from_posix_errno(errno);
        }
      }
      send_buffer_.mark_read(res);
    }

    return {};
  }

  void
  poller_epoll::chan_stream_impl::terminate_impl(const chan_stream_state st)
  {
    if (fd_ != -1) {
      ::epoll_ctl(owner_->epoll_fd_, EPOLL_CTL_DEL, fd_, nullptr);
      ::close(fd_);
      set_state(st);
      fd_ = -1;
    }
  }

  poller_epoll::chan_stream_impl::~chan_stream_impl()
  {
    terminate_impl(chan_stream_state::closed);
  }

  // chan_message_accepter_impl

  // chan_message_impl

  // poller_epoll

  result<> poller_epoll::init()
  {
    epoll_fd_ = CX_TRY_POSIX(::epoll_create1(EPOLL_CLOEXEC));
    temp_events_.resize(128);
    return {};
  }

  poller_epoll::~poller_epoll()
  {
    if (epoll_fd_ != -1) {
      ::close(epoll_fd_);
      epoll_fd_ = -1;
    }
  }

  result<poller_result>
  poller_epoll::poll_and_dispatch(const time may_block_till)
  {
    CX_ASSERT(epoll_fd_ != -1, "epoll file-descriptor not created");

    // handle journals
    {
      for (chan_ptr_target* pp: streams_journal_.reference_gone) {
        auto* p = static_cast<chan_stream_impl*>(pp);
        streams_journal_.written.erase(p);
        streams_journal_.closed.erase(p);
        streams_journal_.failed.erase(p);
        auto res = streams_.erase(p);
        CX_ASSERT(res.is_value());
        std::move(res).as_value().reset();
      }
      streams_journal_.reference_gone.clear();

      for (chan_stream* pp: streams_journal_.closed) {
        auto* p = static_cast<chan_stream_impl*>(pp);
        p->terminate_impl(chan_stream_state::closed);
      }
      streams_journal_.closed.clear();

      for (chan_stream* pp: streams_journal_.failed) {
        auto* p = static_cast<chan_stream_impl*>(pp);
        p->terminate_impl(chan_stream_state::failed);
      }
      streams_journal_.failed.clear();

      for (chan_stream* pp: streams_journal_.written) {
        auto* p = static_cast<chan_stream_impl*>(pp);
        p->trigger();
      }
      streams_journal_.written.clear();

      for (chan_ptr_target* pp: stream_accepters_journal_.reference_gone) {
        auto* p  = static_cast<chan_stream_accepter_impl*>(pp);
        auto res = stream_accepters_.erase(p);
        CX_ASSERT(res.is_value());
        std::move(res).as_value().reset();
      }
      stream_accepters_journal_.reference_gone.clear();
    }

    if (num_epoll_fds() == 0) {
      return poller_result{};
    }

    const int epoll_wait_timeout = [&] {
      if (!may_block_till || may_block_till == time::min()) {
        return 0;
      }
      const duration may_block_for = (may_block_till - time::now());
      if (may_block_for <= duration{}) {
        return 0;
      }
      else {
        return std::min(
            static_cast<int>(may_block_for.nanos() / 1'000'000), 100);
      }
    }();
    const int num_events = CX_TRY_POSIX(::epoll_wait(
        epoll_fd_,
        temp_events_.data(),
        temp_events_.size(),
        epoll_wait_timeout));

    for (int i = 0; i < num_events; ++i) {
      const struct epoll_event& ev = temp_events_[i];

      if (stream_accepters_.contains(ev.data.ptr)) {
        auto* ptr = static_cast<chan_stream_accepter_impl*>(ev.data.ptr);
        CX_TRY(ptr->trigger());
      }
      else if (streams_.contains(ev.data.ptr)) {
        auto* ptr = static_cast<chan_stream_impl*>(ev.data.ptr);
        CX_TRY(ptr->trigger());
      }
      else {
        cx::unreachable();
      }
    }

    return poller_result{
        .num_processed_events = static_cast<size_t>(num_events),
    };
  }

  poller_state poller_epoll::poll_state() const
  {
    const size_t n = num_epoll_fds();
    return poller_state{
        .is_busy            = (n > 0),
        .soonest_next_event = (n > 0) ? time::min() : time::max(),
    };
  }

  size_t poller_epoll::num_epoll_fds() const
  {
    return stream_accepters_.size() + streams_.size() +
        message_accepters_.size() + messages_.size();
  }

  result<chan_ptr<poller_epoll::chan_stream_impl>>
  poller_epoll::create_chan_stream(
      const endpoint_stream& ep, const chan_stream_config& cfg)
  {
    CX_ASSERT(epoll_fd_ != -1, "epoll file-descriptor not created");

    if (std::holds_alternative<endpoint_ipv4>(ep)) {
      const endpoint_ipv4& e = std::get<endpoint_ipv4>(ep);

      const int fd = CX_TRY_POSIX(
          ::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0));

      struct ::sockaddr_in srv_addr;
      memset(&srv_addr, 0, sizeof(srv_addr));
      srv_addr.sin_family      = AF_INET;
      srv_addr.sin_addr.s_addr = ::htonl(e.addr.as_uint32());
      srv_addr.sin_port        = ::htons(e.port);
      const int res =
          ::connect(fd, (struct ::sockaddr*)&srv_addr, sizeof(srv_addr));
      if (res == -1 && errno != EINPROGRESS) {
        return errcat_from_posix_errno(errno);
      }

      CX_ASSERT(fd >= 0, "Invalid fd (={})", fd);

      auto [tgt, cp] =
          chan_ptr_target::make<poller_epoll::chan_stream_impl>(this, fd, cfg);
      CX_TRY(tgt->epoll_register());
      streams_.insert(std::move(tgt));
      return {std::move(cp)};
    }
    else if (
        std::holds_alternative<endpoint_fd>(ep) ||
        std::holds_alternative<endpoint_console>(ep)) {
      return errcat_poller_abstained_from_create;
    }
    else {
      return errcat_poller_abstained_from_create;
    }
  }

  result<chan_ptr<poller_epoll::chan_stream_accepter_impl>>
  poller_epoll::create_chan_stream_accepter(
      const endpoint_stream_accepter& ep,
      const chan_stream_accepter_config& cfg)
  {
    CX_ASSERT(epoll_fd_ != -1, "epoll file-descriptor not created");

    if (std::holds_alternative<endpoint_ipv4>(ep)) {
      const endpoint_ipv4& e = std::get<endpoint_ipv4>(ep);

      const int accepter_fd = CX_TRY_POSIX(
          ::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0));

      if (cfg.reuse_address) {
        const int enable = 1;

        [[maybe_unused]] const int res = CX_TRY_POSIX_CLEANUP(
            ::setsockopt(
                accepter_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)),
            ::close(accepter_fd));
      }

      struct ::sockaddr_in srv_addr;
      memset(&srv_addr, 0, sizeof(srv_addr));
      srv_addr.sin_family      = AF_INET;
      srv_addr.sin_addr.s_addr = ::htonl(e.addr.as_uint32());
      srv_addr.sin_port        = ::htons(e.port);

      [[maybe_unused]] const int res1 = CX_TRY_POSIX_CLEANUP(
          ::bind(accepter_fd, (struct ::sockaddr*)&srv_addr, sizeof(srv_addr)),
          ::close(accepter_fd));

      [[maybe_unused]] const int res2 = CX_TRY_POSIX_CLEANUP(
          ::listen(accepter_fd, cfg.backlog), ::close(accepter_fd));

      CX_ASSERT(accepter_fd >= 0, "Invalid accepter-fd (={})", accepter_fd);

      auto [tgt, cp] = chan_ptr_target::make<chan_stream_accepter_impl>(
          this, accepter_fd, cfg.accepted_stream_cfg, cfg.callback);
      CX_TRY(tgt->epoll_register());
      stream_accepters_.insert(std::move(tgt));
      return {std::move(cp)};
    }
    else {
      return errcat_poller_abstained_from_create;
    }
  }

  string_view poller_epoll::name() const
  {
    return "epoll";
  }

  void poller_epoll::format_json_stats(string& out) const
  {
    fmt::format_to(
        std::back_inserter(out),
        "{{ \"name\" : \"poller_epoll\", \"stream-accepters\" : {}, "
        "\"streams\" : {} }}",
        stream_accepters_.size(),
        streams_.size());
  }

  poller_epoll::poller_epoll(loop& loop) noexcept
    : loop_(loop), stream_accepters_journal_(&loop), streams_journal_(&loop)
  {
  }
}
