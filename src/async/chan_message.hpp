#pragma once

#include "async/chan_ptr.hpp"

namespace cx {
  class chan_message : public chan_ptr_target {
    template<typename T>
      requires std::derived_from<T, chan_ptr_target>
    friend class chan_ptr;

   public:
  };

  struct chan_message_journal : public chan_journal {
    using chan_journal::chan_journal;
  };

  using chan_message_ptr = chan_ptr<chan_message>;
}

// #include "chan_message.ipp"

namespace cx {
}
