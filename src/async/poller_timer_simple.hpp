#pragma once

#include "async/chan_timer.hpp"
#include "async/loop.hpp"
#include "async/poller.hpp"

#include <list>
#include <map>

namespace cx {
  class poller_timer_simple : public poller_base {
   public:
    class chan_timer_impl final : public chan_timer {
      template<typename T>
        requires std::derived_from<T, chan_ptr_target>
      friend class chan_ptr;
      friend class chan_ptr_target;
      friend class poller_timer_simple;

      poller_timer_simple* owner_ = nullptr;

      chan_timer_impl(poller_timer_simple*, time, delegate<time(time)>);

     public:
      ~chan_timer_impl() = default;
    };

   private:
    loop& loop_;

    chan_timer_journal journal_;

    struct deref_expiry {
      bool operator()(const chan_timer_impl*, const chan_timer_impl*) const;
    };
    std::multimap<
        chan_timer_impl*,
        std::unique_ptr<chan_timer_impl>,
        deref_expiry>
        timers_live_sorted_;
    map<chan_timer_impl*, std::unique_ptr<chan_timer_impl>> timers_dead_;

    void insert_impl(std::unique_ptr<chan_timer_impl>);
    std::unique_ptr<chan_timer_impl> remove_refed_impl(chan_timer_impl*);

   public:
    poller_timer_simple(loop& loop) noexcept;
    result<> init();

    string_view name() const;

    void format_json_stats(string&) const;

    result<poller_result> poll_and_dispatch(const time may_block_until);

    poller_state poll_state() const;

    result<chan_ptr<chan_timer_impl>>
        create_chan_timer(time, delegate<time(time)>);
  };
}
