#include "loop.hpp"

#include "misc/scope_guard.hpp"

namespace cx {
  loop::loop() : root_agent_(*this)
  {
    update_tick_time();
  }

  loop::~loop() {}

  void loop::stop(const duration grace_period, const bool with_error)
  {
    (void)grace_period;
    (void)with_error;
  }

  string loop::format_json_stats() const
  {
    string out;
    out.append("{\n  ");
    pollers_format_json_stats_impl(out);
    out.append(",\n");
    root_agent_.format_json_stats(out, 2);
    out.append("\n}");
    return out;
  }

  result<> loop::init()
  {
    if (!is_inited_) {
      CX_TRY(init_impl());
      is_inited_ = true;
    }
    return {};
  }

  result<> loop::run()
  {
    CX_TRY(init());

    is_running_ = true;
    scope_guard _([this] { is_running_ = false; });
    return run_impl();
  }
}
