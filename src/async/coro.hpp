#pragma once

#include "async/coro_promise_accesser.hpp"
#include "async/fwd.hpp"
#include "misc/containers.hpp"
#include "misc/failure.hpp"
#include "misc/fixed_address.hpp"

#include <coroutine>

namespace cx {
  template<typename T>
  class coro {
    friend class coro_type_erased;
    friend struct detail::coro_promise<T>;
    std::coroutine_handle<detail::coro_promise<T>> handle_ = nullptr;
    coro(std::coroutine_handle<detail::coro_promise<T>> handle);

   public:
    using promise_type = detail::coro_promise<T>;

    coro() = default;
    coro(coro&&) noexcept;
    coro& operator=(coro&&) noexcept;
    ~coro();
    void clear();

    bool is_valid() const;
    bool is_done() const;

    bool is_in_initial_suspend() const;

    std::coroutine_handle<detail::coro_promise<T>> handle() const;

    T result() &&;

    struct awaiter;
    awaiter operator co_await();
  };

  struct coro_journal : public fixed_address {
    coro_journal(agent*);

    agent* owner = nullptr;
    vector<std::coroutine_handle<>> finished;
    vector<std::coroutine_handle<>> yielding;
  };

  struct coro_owner_agent;
}

// #include "coroutine.ipp"

namespace cx {

  // coro_promise

  namespace detail {
    struct coro_get_journal
      : public coro_promise_accesser<coro_journal*, coro_get_journal> {
      template<typename Promise>
      auto on_access(Promise& p)
      {
        return p.journal_;
      }
    };

    template<typename T>
    struct coro_promise_result {
      optional<T> result_ = nullopt;

      void return_value(T result)
      {
        result_ = std::move(result);
      }
    };

    template<>
    struct coro_promise_result<void> {
      optional<unit> result_ = nullopt;

      void return_void()
      {
        result_ = unit{};
      }
    };

    template<typename T>
    struct coro_promise : public coro_promise_result<T> {
      coro_journal* journal_                = nullptr;
      bool in_initial_suspend_              = false;
      bool is_managed_                      = false;
      std::coroutine_handle<> continuation_ = nullptr;

      struct initial_awaiter : public std::suspend_always {
        std::coroutine_handle<coro_promise> handle_ = nullptr;
        void await_suspend(std::coroutine_handle<coro_promise> handle) noexcept
        {
          handle.promise().in_initial_suspend_ = true;
          handle_                              = handle;
        }

        void await_resume() const noexcept
        {
          handle_.promise().in_initial_suspend_ = true;
          CX_ASSERT(handle_.promise().journal_ != nullptr);
        }
      };
      initial_awaiter initial_suspend() noexcept
      {
        return {};
      }

      struct final_awaiter : public std::suspend_always {
        std::coroutine_handle<>
        await_suspend(std::coroutine_handle<coro_promise> h) noexcept
        {
          auto& p = h.promise();
          if (p.continuation_) {
            return p.continuation_;
          }
          else {
            p.mark_finished(h);
            return std::noop_coroutine();
          }
        }
      };
      void mark_finished(std::coroutine_handle<coro_promise> h)
      {
        if (is_managed_) {
          journal_->finished.push_back(h);
        }
      }
      final_awaiter final_suspend() noexcept
      {
        return {};
      }

      coro<T> get_return_object()
      {
        return {std::coroutine_handle<coro_promise<T>>::from_promise(*this)};
      }

      void unhandled_exception()
      {
#if CX_WITH_EXCEPTIONS()
        throw;
#endif
      }
    };
  }

  // coro

  template<typename T>
  coro<T>::coro(std::coroutine_handle<detail::coro_promise<T>> handle)
    : handle_(std::move(handle))
  {
  }

  template<typename T>
  coro<T>::coro(coro&& other) noexcept
    : handle_(std::exchange(other.handle_, nullptr))
  {
  }
  template<typename T>
  coro<T>& coro<T>::operator=(coro&& other) noexcept
  {
    if (this != &other) {
      clear();
      handle_ = std::exchange(other.handle_, nullptr);
    }
    return *this;
  }
  template<typename T>
  coro<T>::~coro()
  {
    clear();
  }
  template<typename T>
  void coro<T>::clear()
  {
    if (handle_ != nullptr) {
      handle_.destroy();
      handle_ = nullptr;
    }
  }

  template<typename T>
  bool coro<T>::is_valid() const
  {
    return static_cast<bool>(handle_);
  }
  template<typename T>
  bool coro<T>::is_done() const
  {
    CX_ASSERT(is_valid(), "is_done may only be called on valid coro");
    return handle_.done();
  }

  template<typename T>
  bool coro<T>::is_in_initial_suspend() const
  {
    return handle_.promise().in_initial_suspend_;
  }

  template<typename T>
  std::coroutine_handle<detail::coro_promise<T>> coro<T>::handle() const
  {
    return handle_;
  }

  template<typename T>
  T coro<T>::result() &&
  {
    CX_ASSERT(is_done(), "can only get result if 'is_done'");
    CX_ASSERT(handle_.promise().result().has_value());
    T retval = std::move(handle_.promise().result().value());
    clear();
    return retval;
  }

  template<typename T>
  struct coro<T>::awaiter : public std::suspend_always {
    std::coroutine_handle<detail::coro_promise<T>> callee_handle;

    awaiter(std::coroutine_handle<promise_type> h) : callee_handle(h) {}

    template<typename CallerPromise>
    std::coroutine_handle<>
    await_suspend(std::coroutine_handle<CallerPromise> caller_handle)
    {
      callee_handle.promise().continuation_ = caller_handle;
      callee_handle.promise().journal_      = caller_handle.promise().journal_;
      return callee_handle;
    }
    T await_resume()
    {
      auto& retval = callee_handle.promise().result_;
      CX_ASSERT(retval.has_value());
      if constexpr (std::same_as<T, void>) {
        return;
      }
      else {
        return std::move(retval.value());
      }
    }
  };

  template<typename T>
  typename coro<T>::awaiter coro<T>::operator co_await()
  {
    CX_ASSERT(is_valid(), "can't co_await invalid coro");
    return awaiter(handle_);
  }

  // coro_journal

  inline coro_journal::coro_journal(agent* a) : owner(a) {}

  // coro_owner_agent

  struct coro_owner_agent
    : public coro_promise_accesser<agent*, coro_owner_agent> {
    template<typename Promise>
    auto on_access(Promise& p)
    {
      return p.journal_->owner;
    }
  };
}
