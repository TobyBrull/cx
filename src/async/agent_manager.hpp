#pragma once

#include "conn_manager.hpp"
#include "coro_manager.hpp"

namespace cx {
  struct agent_manager : public fixed_address {
    conn_manager conns;
    coro_manager coros;

    agent_manager(agent*);

    result<poller_result> poll_and_dispatch();

    poller_state poll_state() const;
  };
}

// #include "agent_manager.ipp"

namespace cx {
  inline agent_manager::agent_manager(agent* owner) : conns(owner), coros(owner)
  {
  }

  inline result<poller_result> agent_manager::poll_and_dispatch()
  {
    poller_result retval;
    retval.accumulate(CX_TRY(conns.poll_and_dispatch()));
    retval.accumulate(CX_TRY(coros.poll_and_dispatch()));
    return retval;
  }

  inline poller_state agent_manager::poll_state() const
  {
    poller_state retval;
    retval.accumulate(conns.poll_state());
    retval.accumulate(coros.poll_state());
    return retval;
  }
}
