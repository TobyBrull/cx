#include "conn_timer_default.hpp"

#include "async/agent.hpp"
#include "async/loop.hpp"

namespace cx {
  namespace detail {
    inline time compute_expiry_time(
        const variant<duration, time>& expiry, const time tick_time)
    {
      struct visitor {
        time tick_time_;

        time operator()(const duration d) const
        {
          return tick_time_ + d;
        }
        time operator()(const time t) const
        {
          return t;
        }
      };
      return std::visit(visitor{tick_time}, expiry);
    }
  }

  conn_timer_default::conn_timer_default(
      conn_journal* journal,
      chan_timer_ptr ptr,
      const conn_timer_default_config& cfg,
      delegate<void()> f)
    : conn_timer<conn_timer_default>(journal, std::move(ptr))
    , loop_(conn_base::owner_agent()->owner_loop())
    , cfg_(cfg)
    , f_(std::move(f))
  {
    conn_base::ptr_->set_expiry(
        detail::compute_expiry_time(cfg_.expiry, loop_.tick_time()));
  }

  time conn_timer_default::on_conn_timer(const time old_expiry)
  {
    f_();

    if (cfg_.repeat.has_value()) {
      conn_timer_repeat& r = cfg_.repeat.value();

      time next_expiry;

      switch (r.repeat_style) {
        case conn_timer_repeat_style::FROM_LOOP_TICK_TIME:
          next_expiry = loop_.tick_time() + r.interval;
          break;

        case conn_timer_repeat_style::FROM_TIME_NOW:
          next_expiry = time::now() + r.interval;
          break;

        case conn_timer_repeat_style::FROM_PREVIOUS_EXPIRY:
          next_expiry = old_expiry;
          do {
            next_expiry = next_expiry + r.interval;
          } while (next_expiry <= loop_.tick_time());
          break;
      }

      if (r.count == 0) {
        // infinitely repeating
        return next_expiry;
      }
      else {
        r.count -= 1;
        return (r.count == 0) ? time{} : next_expiry;
      }
    }
    else {
      return {};
    }
  }
}
