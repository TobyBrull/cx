#pragma once

#include "async/agent_manager.hpp"
#include "async/fwd.hpp"
#include "async/poller.hpp"
#include "misc/containers.hpp"
#include "misc/result.hpp"
#include "misc/time.hpp"

#include <concepts>

namespace cx {

  CX_DEFINE_ERROR_CATEGORY(tryed_to_add_agent_to_removed_agent, 1030);

  // An agent can only linearly progress through these states, except that it
  // can also go from ON_START_PENDING directly to ON_STOP_PENDING (if the agent
  // was removed before it was started; this includes the case where "on_start"
  // returns an error). The ON_STOP_DONE state is only used internally; once an
  // agent arrives in this state, it is immediately destructed.
  enum class agent_state {
    ON_START_PENDING = 0,
    ON_START_DONE    = 1, // == RUNNING
    ON_STOP_PENDING  = 2,
    ON_STOP_DONE     = 3,
  };

  class agent : public fixed_address {
    virtual result<> on_start();
    virtual void on_stop();

    friend class loop;

    // root agent constructor
    agent(loop& loop);

    result<poller_result> poll_and_dispatch();

    poller_state poll_state() const;

    agent_state state_ = agent_state::ON_START_PENDING;

    string name_;
    agent* parent_ = nullptr;

    map_multi<string, std::unique_ptr<agent>> children_;
    result<> add_child_agent_impl(string name, std::unique_ptr<agent>);
    void remove_this_agent_impl();

    agent_manager manager_;

   protected:
    loop& loop_;

    agent(agent* parent);

   public:
    virtual ~agent() = default;

    agent_state state() const;
    const string& name() const;
    agent* parent() const;
    const map_multi<string, std::unique_ptr<agent>>& children() const;

    loop& owner_loop() const;

    const agent_manager& manager() const;

    void format_json_stats(string&, int indent) const;

    template<typename T>
    void add_coro(coro<T>);

    template<typename Conn, typename... Args>
    Conn* make_conn(Args&&...);

    template<typename Agent, typename... Args>
      requires(std::is_final_v<Agent> && std::derived_from<Agent, agent>)
    result<Agent*> add_child_agent(string name, Args&&...);

    void remove_child_agent(agent*);
    void remove_child_agents(string_view name);

    /// Schedules destruction of this agent (and all sub-agents).
    /// If called on the cx::loop::root_agent(), this is equivalent to
    /// cx::loop::stop().
    void remove_this_agent();
  };
}

// #include "agent.ipp"

namespace cx {
  inline agent::agent(loop& loop) : manager_(this), loop_(loop) {}

  inline agent::agent(agent* parent)
    : parent_(parent), manager_(this), loop_(parent->loop_)
  {
  }

  inline result<> agent::on_start()
  {
    return {};
  }

  inline void agent::on_stop() {}

  inline agent_state agent::state() const
  {
    return state_;
  }

  inline const string& agent::name() const
  {
    return name_;
  }

  inline agent* agent::parent() const
  {
    return parent_;
  }

  inline const map_multi<string, std::unique_ptr<agent>>&
  agent::children() const
  {
    return children_;
  }

  inline loop& agent::owner_loop() const
  {
    return loop_;
  }

  inline const agent_manager& agent::manager() const
  {
    return manager_;
  }

  template<typename T>
  void agent::add_coro(coro<T> coro)
  {
    manager_.coros.add_coro(std::move(coro));
  }

  template<typename Conn, typename... Args>
  Conn* agent::make_conn(Args&&... args)
  {
    return manager_.conns.make_conn<Conn>(std::forward<Args>(args)...);
  }

  template<typename Agent, typename... Args>
    requires(std::is_final_v<Agent> && std::derived_from<Agent, agent>)
  result<Agent*> agent::add_child_agent(string name, Args&&... args)
  {
    // TODO: Pass pointer to derived type here instead of "this" by using the
    // "deducing this" proposal.
    auto ptr      = std::make_unique<Agent>(this, std::forward<Args>(args)...);
    Agent* retval = ptr.get();
    CX_TRY(add_child_agent_impl(std::move(name), std::move(ptr)));
    return retval;
  }

  inline void agent::remove_child_agents(const string_view name)
  {
    const auto [beg_it, end_it] = children_.equal_range(std::string{name});
    for (auto it = beg_it; it != end_it; ++it) {
      it->second->remove_this_agent();
    }
  }
}
