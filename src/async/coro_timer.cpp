#include "coro_timer.hpp"

#include "misc/delegate.hpp"

namespace cx {
  time coro_timer::on_timer(time)
  {
    CX_ASSERT(to_resume_, "no valid handle to resume");
    to_resume_.resume();
    return {};
  }

  coro<> coro_sleep_till(loop& loop, const time t)
  {
    coro_timer ct(loop.create_chan_timer(t, {}).as_value());

    co_await ct.sleep_till(t);
  }
  coro<> coro_sleep_for(loop& loop, const duration d)
  {
    return coro_sleep_till(loop, loop.tick_time() + d);
  }
}
