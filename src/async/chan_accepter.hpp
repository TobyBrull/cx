#pragma once

#include "async/chan_ptr.hpp"
#include "async/chan_stream.hpp"

namespace cx {
  struct chan_stream_accepter_config {
    int backlog                              = 128;
    bool reuse_address                       = true;
    chan_stream_config accepted_stream_cfg   = {};
    delegate<void(chan_stream_ptr)> callback = {};
  };

  class chan_stream_accepter : public chan_ptr_target {
   protected:
    chan_stream_config accepted_stream_cfg_;
    delegate<void(chan_stream_ptr)> callback_;

    template<typename T>
      requires std::derived_from<T, chan_ptr_target>
    friend class chan_ptr;

    chan_stream_accepter(
        chan_stream_accepter_journal* journal,
        const chan_stream_config& accepted_stream_cfg,
        delegate<void(chan_stream_ptr)>);

   public:
    ~chan_stream_accepter() = default;

    void set_accepted_stream_config(const chan_stream_config&);
    const chan_stream_config& accepted_stream_config() const;

    void set_callback(delegate<void(chan_stream_ptr)>);
  };

  struct chan_stream_accepter_journal : public chan_journal {
    using chan_journal::chan_journal;
  };

  class chan_message_accepter : public chan_ptr_target {
    template<typename T>
      requires std::derived_from<T, chan_ptr_target>
    friend class chan_ptr;

   public:
  };

  struct chan_message_accepter_journal : public chan_journal {
    using chan_journal::chan_journal;
  };

  using chan_stream_accepter_ptr  = chan_ptr<chan_stream_accepter>;
  using chan_message_accepter_ptr = chan_ptr<chan_message_accepter>;
}

// #include "chan_accepter.ipp"

namespace cx {

  // chan_stream_accepter

  inline chan_stream_accepter::chan_stream_accepter(
      chan_stream_accepter_journal* const journal,
      const chan_stream_config& cfg,
      const delegate<void(chan_stream_ptr)> callback)
    : chan_ptr_target(journal), accepted_stream_cfg_(cfg), callback_(callback)
  {
  }

  inline void chan_stream_accepter::set_accepted_stream_config(
      const chan_stream_config& cfg)
  {
    accepted_stream_cfg_ = cfg;
  }

  inline const chan_stream_config&
  chan_stream_accepter::accepted_stream_config() const
  {
    return accepted_stream_cfg_;
  }

  inline void
  chan_stream_accepter::set_callback(delegate<void(chan_stream_ptr)> callback)
  {
    callback_ = callback;
  }

  // chan_message_accepter

}
