#pragma once

#include "misc/containers.hpp"

#include <bit>

namespace cx {
  size_t ceil_single_bit(size_t);
}

// #include "bit.ipp"

namespace cx {
  inline size_t ceil_single_bit(const size_t x)
  {
    if (std::has_single_bit(x)) {
      return x;
    }
    else {
      return 1 << ((sizeof(x) * 8) - std::countl_zero(x));
    }
  }
}
