#pragma once

#include "config.hpp"

#include "misc/containers.hpp"

#include <fmt/format.h>

#include <source_location>
#include <stdexcept>
#include <utility>

// TODO:
// * Thread safety
// * Use logging framework

namespace cx {
  /// Executes the currently active failure handler. The failure handler must
  /// terminate the program or throw an exception.
  ///
  /// The default failure handler throws an exception if exceptions are enabled
  /// (via the CMake flag `CX_WITH_EXCEPTIONS`) or prints the
  /// `failure_description` to `stderr` and calls `std::exit(1)` otherwise.
  [[noreturn]] void run_failure_handler(string failure_description);

  using failure_handler_t = decltype(&run_failure_handler);

  /// RAII class for controlling the installed failure handler.
  ///
  /// Usage:
  ///
  /// ```
  /// {
  ///   cx::failure_handler_guard const _{&my_failure_handler};
  ///
  ///   // ...
  /// }
  /// ```
  struct failure_handler_guard {
    failure_handler_t const old_failure_handler = nullptr;

    /// @param new_failure_handler Must be a function that never returns.
    [[nodiscard]] failure_handler_guard(failure_handler_t new_failure_handler);

    ~failure_handler_guard();

    failure_handler_guard(failure_handler_guard&&)            = delete;
    failure_handler_guard& operator=(failure_handler_guard&&) = delete;

    failure_handler_guard(failure_handler_guard const&)            = delete;
    failure_handler_guard& operator=(failure_handler_guard const&) = delete;
  };

  [[noreturn]] void unreachable();

#if CX_WITH_ASSERTS()
  /// Macro to check implementation errors (including postconditions), i.e.,
  /// errors made in the implementation of a function. Also used for documenting
  /// the implementation.

#  define CX_ASSERT(condition, ...)                                         \
    do {                                                                    \
      if (!(condition)) {                                                   \
        cx::detail::invoke_failure_handler(std::source_location::current()  \
                                               __VA_OPT__(, ) __VA_ARGS__); \
      }                                                                     \
    } while (false)

#else
#  define CX_ASSERT(...)
#endif

/// Macro to check preconditions in functions, i.e., errors made by the user of
/// the function.
#define CX_REQUIRE(...) CX_ASSERT(__VA_ARGS__)
}

// #include "failure.ipp"

namespace cx {

  [[noreturn]] inline void unreachable()
  {
#if CX_WITH_EXCEPTIONS()
    throw std::runtime_error("cx::unreachable called");
#else
    fmt::print("cx::unreachable() called\n");
    std::exit(1);
#endif
  }

  namespace detail {
    template<typename FormatStr, typename... Args>
    [[noreturn]] void invoke_failure_handler_fmt(
        string failure_desc, FormatStr fmt_str, const Args&... args)
    {
      failure_desc += fmt::format(fmt::runtime(fmt_str), args...);
      run_failure_handler(std::move(failure_desc));
    }

    template<typename... Args>
    [[noreturn]] void
    invoke_failure_handler(const std::source_location loc, Args&&... args)
    {
      string failure_desc = fmt::format(
          "cx::failure in function {} at {}:{}: ",
          loc.function_name(),
          loc.file_name(),
          loc.line());
      if constexpr (sizeof...(Args) == 0) {
        run_failure_handler(std::move(failure_desc));
      }
      else {
        invoke_failure_handler_fmt(
            std::move(failure_desc), std::forward<Args>(args)...);
      }
    }
  }
}
