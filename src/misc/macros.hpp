#pragma once

namespace cx {
#define CX_ATTRIBUTE_USED __attribute__((__used__))

#define MERGE_(a, b)                   a##b
#define LABEL_(a)                      MERGE_(unique_name_, a)
#define CX_TRANSLATION_UNIT_LOCAL_NAME LABEL_(__COUNTER__)

#define STRINGIZE(x)  STRINGIZE2(x)
#define STRINGIZE2(x) #x
}
