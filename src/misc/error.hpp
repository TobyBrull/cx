#pragma once

#include "misc/ct_map.hpp"
#include "misc/startup.hpp"
#include "misc/thread.hpp"

#include <optional>

namespace cx {
  /// Conceptually:
  ///
  /// ```
  /// struct error_category {
  ///   string name;
  /// };
  ///
  /// struct error {
  ///   error_category category;
  ///   string message;
  /// };
  /// ```
  ///
  /// The implementation is optimized for performance, though (`error_category`
  /// is just an integer and using an empty `message` is very fast).
  ///
  /// Usage:
  ///
  /// ```
  /// // Always prepends "errcat_" to the generated variable name.
  /// CX_DEFINE_ERROR_CATEGORY(file_not_found, 123);
  /// CX_DEFINE_ERROR_CATEGORY(file_permissions, 124);
  ///
  /// const error err_1(errcat_file_not_found);
  /// const error err_2(errcat_file_permission, "can't read file {}");
  ///
  /// if (err_2.category() == errcat_file_perissions) {
  ///   fmt::print("Got error: {}", err_2);
  /// }
  /// ```

  using error_id_t = unsigned int;

  class error_category : private ct_map<"errcat", error_id_t, string>::entry {
    using entry::entry;

    friend class error;
    friend struct detail__error_category_private_access_;
    static constexpr error_id_t id_bits = (sizeof(error_id_t) * 8 / 2);
    static constexpr error_id_t id_mask = (1 << id_bits) - 1;

   public:
    using map_type = entry::map_type;

    constexpr explicit operator bool() const noexcept;

    constexpr error_id_t id() const noexcept;

    const string& name() const;

    constexpr auto operator<=>(const error_category&) const noexcept = default;
  };

/// Produces the following global variable.
/// ```
/// constexpr cx::error_category errcat_<name> = cx::error_category(id);
/// ```
/// \endrst
#define CX_DEFINE_ERROR_CATEGORY(name, id)                       \
  CX_CT_MAP_ENTRY(cx::error_category, errcat_##name, id, #name); \
  CX_STARTUP_TAGGED(                                             \
      define_error_category,                                     \
      cx::detail__error_category_private_access_::register_(id));

  class error {
    using message_index_t = error_id_t;
    static constexpr message_index_t comp_message_index(error_id_t);
    static constexpr error_id_t comp_error_id(message_index_t, error_category);
    static void g_message_deallocate(message_index_t);
    static message_index_t g_message_allocate(string message);
    static string g_message_get(message_index_t);
    static message_index_t g_message_copy(message_index_t);

    error_id_t id_or_handle_ = 0;

   public:
    constexpr ~error();

    constexpr error(const error&);
    constexpr error& operator=(const error&);

    constexpr error() = default;

    /// Just copies the id from the `error_category` and doesn't require any
    /// memory allocations.
    constexpr error(error_category) noexcept;

    /// Stores the `message` in a global vector. This message is deallocated
    /// once the error is destructed.
    error(error_category, string message);

    constexpr void clear();

    constexpr explicit operator bool() const noexcept;

    constexpr error_category category() const noexcept;

    /// The `message` passed to the constructor or otherwise the name of the
    /// category
    string message() const;

    /// Only compares the `cx::error::category`.
    constexpr auto operator<=>(const error&) const noexcept;
    constexpr bool operator==(const error&) const noexcept;
  };
}

// #include "error.ipp"

namespace cx {

  // error_category

  constexpr error_category::operator bool() const noexcept
  {
    return (key() != 0);
  }

  constexpr error_id_t error_category::id() const noexcept
  {
    return key();
  }

  inline const string& error_category::name() const
  {
    return value();
  }

  // detail__error_category_private_access_

  struct detail__error_category_private_access_ {
    static constexpr error_id_t id_bits = error_category::id_bits;
    static constexpr error_id_t id_mask = error_category::id_mask;

    static void register_([[maybe_unused]] const error_id_t id)
    {
      CX_REQUIRE(
          id <= error_category::id_mask,
          "cx::error_category id={} is larger than id_mask={})",
          id,
          error_category::id_mask);
    }
  };

  // error

  namespace detail {
    extern mutexed<vector<optional<string>>> error_messages;
  }

  constexpr error::message_index_t
  error::comp_message_index(const error_id_t id_or_handle)
  {
    return (id_or_handle >> error_category::id_bits);
  }

  constexpr error_id_t error::comp_error_id(
      const message_index_t msg_idx, const error_category errcat)
  {
    return (errcat.id() | (msg_idx << error_category::id_bits));
  }

  constexpr error::~error()
  {
    clear();
  }

  constexpr error::error(const error& other)
    : id_or_handle_(other.id_or_handle_)
  {
    const error_id_t msg_idx = comp_message_index(other.id_or_handle_);
    if (msg_idx != 0) {
      id_or_handle_ = comp_error_id(g_message_copy(msg_idx), other.category());
    }
  }

  constexpr error& error::operator=(const error& other)
  {
    if (this != &other) {
      clear();
      const error_id_t msg_idx = comp_message_index(other.id_or_handle_);
      if (msg_idx != 0) {
        id_or_handle_ =
            comp_error_id(g_message_copy(msg_idx), other.category());
      }
      else {
        id_or_handle_ = other.id_or_handle_;
      }
    }
    return (*this);
  }

  constexpr error::error(const error_category errcat) noexcept
    : id_or_handle_(errcat.key())
  {
  }

  inline error::error(const error_category errcat, string message)
  {
    id_or_handle_ =
        comp_error_id(g_message_allocate(std::move(message)), errcat);
  }

  constexpr void error::clear()
  {
    const error_id_t msg_idx = comp_message_index(id_or_handle_);
    if (msg_idx != 0) {
      g_message_deallocate(msg_idx);
    }
    id_or_handle_ = 0;
  }

  constexpr error::operator bool() const noexcept
  {
    return (id_or_handle_ != 0);
  }

  constexpr error_category error::category() const noexcept
  {
    return error_category(id_or_handle_ & error_category::id_mask);
  }

  inline string error::message() const
  {
    const error_id_t msg_idx = comp_message_index(id_or_handle_);
    if (msg_idx == 0) {
      return category().name();
    }
    else {
      return g_message_get(msg_idx);
    }
  }

  constexpr auto error::operator<=>(const error& other) const noexcept
  {
    return category() <=> other.category();
  }

  constexpr bool error::operator==(const error& other) const noexcept
  {
    return category() == other.category();
  }
}

/// Supports the same format specifications as a string.
template<>
struct fmt::formatter<cx::error_category> : public fmt::formatter<cx::string> {
  template<typename FormatContext>
  auto format(const cx::error_category& errcat, FormatContext& ctx)
  {
    return this->fmt::formatter<cx::string>::format(errcat.name(), ctx);
  }
};

/// Supports the same format specifications as a string.
template<>
struct fmt::formatter<cx::error> : public fmt::formatter<cx::string> {
  template<typename FormatContext>
  auto format(const cx::error& err, FormatContext& ctx)
  {
    return this->fmt::formatter<cx::string>::format(err.message(), ctx);
  }
};
