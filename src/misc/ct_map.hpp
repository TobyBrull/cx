#pragma once

#include "misc/containers.hpp"
#include "misc/failure.hpp"
#include "misc/startup.hpp"

namespace cx {
  /// Approximates a compile-time map that can be extended from various
  /// translation units. Here, `Key` has to be a literal type. In the current
  /// implementation `T` doesn't have to be a literal type, but `ct_map` should
  /// only be used in cases where all entries are known at compile-time.
  ///
  /// Usage:
  /// ```
  /// // translation_unit_1.hpp
  ///
  /// class some_entry : public cx::ct_map<"some_entry", int, cx::string>::entry
  /// {
  ///   using entry::entry;
  /// };
  ///
  /// // translation_unit_2.hpp
  ///
  /// CX_CT_MAP_ENTRY(some_entry, first_entry, 42, "answer");
  ///
  /// // translation_unit_3.cpp
  ///
  /// CX_CT_MAP_ENTRY(some_entry, second_entry, 123, "question");
  ///
  /// // translation_unit_4.cpp
  ///
  /// void foo()
  /// {
  ///   CHECK(first_entry.key() == 42);
  ///   CHECK(first_entry.value() == "answer");
  ///   CHECK(first_entry < second_entry);
  /// }
  /// ```
  template<ct_string map_name, typename Key, typename T>
  class ct_map {
    static map<Key, T>& data();

    template<typename>
    friend struct detail__ct_map_private_access_;

   public:
    constexpr inline static ct_string name = map_name;

    using key_type   = Key;
    using value_type = T;

    static size_t size();

    class entry {
      Key key_ = {};

      template<typename>
      friend struct detail__ct_map_private_access_;

     protected:
      constexpr explicit entry(Key key) : key_(key) {}

     public:
      using map_type = ct_map;

      constexpr Key key() const;

      const T& value() const;

      constexpr auto operator<=>(const entry&) const noexcept = default;
    };
  };

#define CX_CT_MAP_ENTRY(entry_type, varname, key, value)                   \
  [[maybe_unused]] constexpr entry_type varname =                          \
      cx::detail__ct_map_private_access_<entry_type::map_type>::make<      \
          entry_type>(key);                                                \
  CX_STARTUP_TAGGED(                                                       \
      ct_map_entry,                                                        \
      cx::detail__ct_map_private_access_<entry_type::map_type>::register_( \
          key, value))
}

// #include "ct_map.ipp"

namespace cx {

  // ct_map

  template<ct_string map_name, typename Key, typename T>
  map<Key, T>& ct_map<map_name, Key, T>::data()
  {
    static map<Key, T> retval = {};
    return retval;
  }

  template<ct_string map_name, typename Key, typename T>
  size_t ct_map<map_name, Key, T>::size()
  {
    return data().size();
  }

  // detail__ct_map_private_access_

  template<typename CTMapType>
  struct detail__ct_map_private_access_ {
    constexpr inline static ct_string map_name = CTMapType::name;

    using Key = typename CTMapType::key_type;
    using T   = typename CTMapType::value_type;

    static void register_(Key key, T value)
    {
      auto& mm                       = CTMapType::data();
      [[maybe_unused]] const auto it = mm.find(key);
      CX_REQUIRE(
          it == mm.end(),
          "cx::ct_map<{}> key={} used twice (for values={} and value={})",
          string{map_name},
          key,
          value,
          it->second);

      mm.emplace(key, std::move(value));
    }
    template<typename EntryType>
    constexpr static EntryType make(Key key)
    {
      return EntryType(key);
    }
    static const T& get(Key key)
    {
      auto& mm      = CTMapType::data();
      const auto it = mm.find(key);
      CX_ASSERT(it != mm.end(), "could not find value for key={}", key);
      return it->second;
    }
  };

#define CX_CT_MAP_ENTRY_REGISTER(entry_type, key, value)          \
  static cx::detail__ct_map_private_access_<entry_type::map_type> \
      CX_ATTRIBUTE_USED CX_TRANSLATION_UNIT_LOCAL_NAME(key, value)

  // ct_map::entry

  template<ct_string map_name, typename Key, typename T>
  constexpr Key ct_map<map_name, Key, T>::entry::key() const
  {
    return key_;
  }

  template<ct_string map_name, typename Key, typename T>
  const T& ct_map<map_name, Key, T>::entry::value() const
  {
    return detail__ct_map_private_access_<ct_map<map_name, Key, T>>::get(key_);
  }
}
