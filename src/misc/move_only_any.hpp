#pragma once

#include "misc/containers.hpp"

namespace cx {
  class move_only_any {
    struct storage_base {
      virtual ~storage_base() = default;
    };

    template<typename T>
    struct storage final : public storage_base {
      T store_;

      template<typename... Args>
      storage(Args&&...);

      virtual ~storage() = default;
    };

    std::unique_ptr<storage_base> data_;

   public:
    move_only_any()  = default;
    ~move_only_any() = default;

    move_only_any(move_only_any&&)            = default;
    move_only_any& operator=(move_only_any&&) = default;

    template<typename T>
    move_only_any(T);

    template<typename T, typename... Args>
    T* emplace(Args&&...);
  };
}

// #include "move_only_any.ipp"

namespace cx {

  template<typename T>
  template<typename... Args>
  move_only_any::storage<T>::storage(Args&&... args)
    : store_(std::forward<Args>(args)...)
  {
  }

  template<typename T>
  move_only_any::move_only_any(T x)
  {
    data_ = std::make_unique<storage<T>>(std::move(x));
  }

  template<typename T, typename... Args>
  T* move_only_any::emplace(Args&&... args)
  {
    auto typed_data = std::make_unique<storage<T>>(std::forward<Args>(args)...);
    T* retval       = &(typed_data->store_);
    data_           = std::move(typed_data);
    return retval;
  }
}
