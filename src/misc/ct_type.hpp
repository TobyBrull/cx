#pragma once

namespace cx {
  template<typename T>
  struct ct_type {
    using type = T;

    constexpr ct_type() = default;

    constexpr ct_type(T){};
  };

  template<>
  struct ct_type<void> {
    using type = void;

    constexpr ct_type() = default;
  };
}
