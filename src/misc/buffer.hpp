#pragma once

#include "misc/result.hpp"

namespace cx {

  CX_DEFINE_ERROR_CATEGORY(write_buffer_full, 500);

  struct buffer_config {
    size_t capacity = (1 << 14);

    // A relocation will only happen if the read-span size is below this
    // fraction of the capacity.
    float effective_size_factor = 0.25;

    // If the read-span size is below this fraction of the capacity, a
    // relocation will be encouraged.
    float fast_relocation_factor = 0.05;
  };

  template<typename T>
  class buffer {
    vector<T> data_;
    size_t effective_size_  = 0;
    size_t fast_reloc_size_ = 0;
    size_t begin_           = 0;
    size_t end_             = 0;

   public:
    using value_type = T;

    buffer(const buffer_config&);

    void set_config(const buffer_config&);

    bool is_empty() const;
    size_t size() const;
    size_t capacity() const;

    span<T> read_span();
    span<const T> read_span() const;

    span<T> write_span();
    span<const T> write_span() const;

    void mark_read(size_t);
    void mark_written(size_t);

    bool is_full() const;

    result<> write(span<const T>);

    span<T> prepare_for_write();

    void relocate_to_front();
  };
}

// #include "buffer.ipp"

namespace cx {
  // buffer

  template<typename T>
  buffer<T>::buffer(const buffer_config& cfg)
    : data_(cfg.capacity)
    , effective_size_(cfg.capacity * cfg.effective_size_factor)
    , fast_reloc_size_(cfg.capacity * cfg.fast_relocation_factor)
  {
    CX_ASSERT(
        effective_size_ >= 1,
        "The effective-size (={}) must at least be 1",
        effective_size_);
  }

  template<typename T>
  void buffer<T>::set_config(const buffer_config& cfg)
  {
    if (cfg.capacity == data_.size()) {
      effective_size_  = (cfg.capacity * cfg.effective_size_factor);
      fast_reloc_size_ = (cfg.capacity * cfg.fast_relocation_factor);
      relocate_to_front();
    }
    else {
      const size_t copy_size = std::min(size(), cfg.capacity);
      buffer<T> new_buf(cfg);
      std::ranges::copy(
          read_span().subspan(0, copy_size), new_buf.write_span().begin());
      new_buf.mark_written(copy_size);
      std::swap(*this, new_buf);
    }
  }

  template<typename T>
  bool buffer<T>::is_empty() const
  {
    return (begin_ == end_);
  }
  template<typename T>
  size_t buffer<T>::size() const
  {
    return (end_ - begin_);
  }
  template<typename T>
  size_t buffer<T>::capacity() const
  {
    return data_.size();
  }

  template<typename T>
  void buffer<T>::mark_read(const size_t len)
  {
    CX_ASSERT(
        len <= read_span().size(),
        "Trying to mark as read (={}) more than available (={})",
        len,
        read_span().size());
    begin_ += len;
  }
  template<typename T>
  void buffer<T>::mark_written(const size_t len)
  {
    CX_ASSERT(
        len <= write_span().size(),
        "Trying to mark as written (={}) more than available (={})",
        len,
        write_span().size());
    end_ += len;
  }

  template<typename T>
  span<T> buffer<T>::read_span()
  {
    return {data_.begin() + begin_, end_ - begin_};
  }
  template<typename T>
  span<T> buffer<T>::write_span()
  {
    return {data_.begin() + end_, data_.size() - end_};
  }

  template<typename T>
  span<const T> buffer<T>::read_span() const
  {
    return {data_.begin() + begin_, end_ - begin_};
  }
  template<typename T>
  span<const T> buffer<T>::write_span() const
  {
    return {data_.begin() + end_, data_.size() - end_};
  }

  template<typename T>
  bool buffer<T>::is_full() const
  {
    return (size() == capacity());
  }

  template<typename T>
  result<> buffer<T>::write(const span<const T> ws)
  {
    if (ws.size() <= write_span().size()) {
      std::ranges::copy(ws, write_span().begin());
      mark_written(ws.size());
      return {};
    }
    else {
      return errcat_write_buffer_full;
    }
  }

  template<typename T>
  span<T> buffer<T>::prepare_for_write()
  {
    if (write_span().size() == 0) {
      if (begin_ == 0) {
        return {};
      }
      else {
        relocate_to_front();
      }
    }
    return write_span();
  }

  template<typename T>
  void buffer<T>::relocate_to_front()
  {
    const size_t s = read_span().size();
    std::copy(data_.begin() + begin_, data_.begin() + end_, data_.begin());
    begin_ = 0;
    end_   = s;
  }
}
