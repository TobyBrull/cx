#include "failure.hpp"

#include <exception>

namespace {
  [[noreturn]] void default_failure_handler(cx::string failure_desc)
  {
#if CX_WITH_EXCEPTIONS()
    throw std::runtime_error(std::move(failure_desc));
#else
    fmt::print(stderr, "{}\n", failure_desc);
    std::exit(1);
#endif
  }

  cx::failure_handler_t& current_failure_handler()
  {
    static cx::failure_handler_t retval = &default_failure_handler;
    return retval;
  }
}

namespace cx {

  [[noreturn]] void run_failure_handler(string failure_desc)
  {
    (*current_failure_handler())(std::move(failure_desc));

    // This should never be reached as "current_failure_handler()" is assumed to
    // be a [[noreturn]] function.
    std::abort();
  }

  failure_handler_guard::~failure_handler_guard()
  {
    CX_ASSERT(old_failure_handler != nullptr, "no old failure handler");
    current_failure_handler() = old_failure_handler;
  }

  failure_handler_guard::failure_handler_guard(
      failure_handler_t const new_failure_handler)
    : old_failure_handler(current_failure_handler())
  {
    CX_REQUIRE(new_failure_handler != nullptr, "no new failure handler");

    CX_ASSERT(old_failure_handler != nullptr, "no old failure handler");

    current_failure_handler() = new_failure_handler;
  }
}

#if !CX_WITH_EXCEPTIONS()
namespace cx::detail {
  struct dtor_monitor {
    using callback_type = void (*)();

    callback_type dtor_callback = nullptr;

    ~dtor_monitor()
    {
      CX_REQUIRE(dtor_callback != nullptr, "no dtor-callback");
      dtor_callback();
    }
  };
}
#endif
