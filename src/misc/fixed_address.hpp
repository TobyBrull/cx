#pragma once

#include "misc/ct_type.hpp"

namespace cx {
  // Classes can derive from this struct to indicate that their address in
  // memory will never change. This implies that pointers to objects of such
  // classes can be used in callbacks.
  struct fixed_address {
    fixed_address() = default;

    fixed_address(fixed_address&&)            = delete;
    fixed_address& operator=(fixed_address&&) = delete;

    fixed_address(const fixed_address&)            = delete;
    fixed_address& operator=(const fixed_address&) = delete;

    ~fixed_address() = default;
  };

  template<typename FixedAddress>
  consteval bool is_fixed_address(ct_type<FixedAddress> = {});
}

// #include "fixed_address.ipp"

namespace cx {

  template<typename FixedAddress>
  consteval bool is_fixed_address(ct_type<FixedAddress>)
  {
    return !std::is_move_constructible_v<FixedAddress> &&
        !std::is_copy_constructible_v<FixedAddress>;
  }
}
