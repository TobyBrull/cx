#pragma once

namespace cx {

#define CX_MAIN(cx_main_func)                                               \
  int main(const int, const char**)                                         \
  {                                                                         \
    cx::result<> res = cx_main_func();                                      \
    if (res.is_error()) {                                                   \
      fmt::print("Error on program exit: {}\n", std::move(res).as_error()); \
      return 1;                                                             \
    }                                                                       \
    else {                                                                  \
      fmt::print("Program exited successfully\n");                          \
      return 0;                                                             \
    }                                                                       \
  }

}
