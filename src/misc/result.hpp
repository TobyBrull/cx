#pragma once

#include "misc/ct_type.hpp"
#include "misc/error.hpp"
#include "misc/failure.hpp"

#include <fmt/format.h>

#include <system_error>

namespace cx {
  /// A type that either represents a successful return value of type `T` or an
  /// error.
  ///
  /// Usage:
  ///
  /// ```
  /// result<double> square_pos(const double x)
  /// {
  ///   if (x < 0.0) {
  ///     return errcat_argument_out_of_domain;
  ///   }
  ///   return x * x;
  /// }
  ///
  /// result<int> my_function(const double x)
  /// {
  ///   const double y = CX_TRY(square_pos(x));
  ///   return static_cast<int>(y);
  /// }
  /// ```
  template<typename T = void>
  struct result {
    using value_type = T;

    static_assert(!std::is_same_v<T, error>);
    static_assert(!std::is_same_v<T, void>);

    variant<T, error> payload;

    constexpr result(variant<T, error>) noexcept(
        std::is_nothrow_move_constructible_v<T>);
    constexpr result(T value) noexcept(std::is_nothrow_move_constructible_v<T>);
    constexpr result(error err) : payload(std::move(err)) {}
    constexpr result(error_category errcat) noexcept : payload(errcat) {}

    template<typename U>
      requires(!std::same_as<T, U> && std::convertible_to<T, U>)
    constexpr operator result<U>() &&;

    template<typename U>
      requires(!std::same_as<T, U> && std::convertible_to<T, U>)
    constexpr operator result<U>() const&;

    /// Whether this result is in the "value" state., i.e., whether the
    /// `cx::result::payload` contains a `T`.
    constexpr bool is_value() const noexcept;

    /// @requires `cx::result::is_value()`.
    constexpr T as_value() &&;
    constexpr T as_value_or(T&& default_value) &&;
    constexpr T as_value_or(const T& default_value) &&;

    /// Whether this result is in the "error" state, i.e., whether the
    /// `cx::result::payload` contains an `cx::error`.
    constexpr bool is_error() const noexcept;

    /// @requires `cx::result::is_error()`.
    constexpr error as_error() &&;
    constexpr error as_error() const&;

    constexpr operator bool() const noexcept;

    template<typename F>
    auto transform(F f) &&;

    constexpr auto operator<=>(const result<T>&) const noexcept = default;
  };

  template<>
  struct result<void> {
    using value_type = void;

    variant<std::monostate, error> payload;

    constexpr result() noexcept = default;
    constexpr result(error err) : payload(std::move(err)) {}
    constexpr result(error_category errcat) noexcept : payload(errcat) {}

    constexpr bool is_value() const noexcept;
    constexpr void as_value() &&;

    constexpr bool is_error() const noexcept;
    constexpr error as_error() &&;

    constexpr operator bool() const noexcept;

    constexpr auto operator<=>(const result<void>&) const noexcept = default;
  };

  template<typename Result>
  consteval bool is_result(ct_type<Result> = {});

/// Evaluates `expression` which has to return a `cx::result`. If the
/// `cx::result` contains an error, returns from currency scope. Otherwise,
/// evaluates to the value contained in the `cx::result`.
#define CX_TRY(expression)                                           \
  ({                                                                 \
    auto result = (expression);                                      \
    static_assert(cx::detail::is_result_t<decltype(result)>::value); \
    if (result.is_error()) {                                         \
      return std::move(result).as_error();                           \
    }                                                                \
    std::move(result).as_value();                                    \
  })
}

// #include "result.ipp"

namespace cx {

  namespace detail {
    template<typename Result>
    struct is_result_impl : public std::false_type {};

    template<typename T>
    struct is_result_impl<result<T>> : public std::true_type {};
  }

  template<typename Result>
  consteval bool is_result(ct_type<Result>)
  {
    return detail::is_result_impl<Result>::value;
  }

  // result<T>

  template<typename T>
  constexpr result<T>::result(variant<T, error> p) noexcept(
      std::is_nothrow_move_constructible_v<T>)
    : payload(std::move(p))
  {
  }

  template<typename T>
  constexpr result<T>::result(T value) noexcept(
      std::is_nothrow_move_constructible_v<T>)
    : payload(std::move(value))
  {
  }

  namespace detail {
    template<typename T, typename U>
    struct move_conversion_visitor {
      variant<U, error> operator()(T&& x)
      {
        return std::move(x);
      }
      variant<U, error> operator()(error&& e)
      {
        return std::move(e);
      }
    };
  }

  template<typename T>
  template<typename U>
    requires(!std::same_as<T, U> && std::convertible_to<T, U>)
  constexpr result<T>::operator result<U>() &&
  {
    return {std::visit(
        detail::move_conversion_visitor<T, U>{}, std::move(payload))};
  }

  namespace detail {
    template<typename T, typename U>
    struct copy_conversion_visitor {
      variant<U, error> operator()(const T& x)
      {
        return x;
      }
      variant<U, error> operator()(const error& e)
      {
        return e;
      }
    };
  }

  template<typename T>
  template<typename U>
    requires(!std::same_as<T, U> && std::convertible_to<T, U>)
  constexpr result<T>::operator result<U>() const&
  {
    return {std::visit(detail::copy_conversion_visitor<T, U>{}, payload)};
  }

  template<typename T>
  constexpr bool result<T>::is_value() const noexcept
  {
    return std::holds_alternative<T>(payload);
  }

  template<typename T>
  constexpr T result<T>::as_value() &&
  {
    CX_REQUIRE(is_value(), "no value in cx::result");
    return std::get<T>(std::move(payload));
  }

  template<typename T>
  constexpr T result<T>::as_value_or(T&& default_value) &&
  {
    if (is_error()) {
      return std::move(default_value);
    }
    else {
      return std::move(*this).as_value();
    }
  }

  template<typename T>
  constexpr T result<T>::as_value_or(const T& default_value) &&
  {
    if (is_error()) {
      return default_value;
    }
    else {
      return std::move(*this).as_value();
    }
  }

  template<typename T>
  constexpr bool result<T>::is_error() const noexcept
  {
    return std::holds_alternative<error>(payload);
  }

  template<typename T>
  constexpr error result<T>::as_error() &&
  {
    CX_REQUIRE(is_error(), "no error in cx::result");
    return std::get<error>(std::move(payload));
  }

  template<typename T>
  constexpr error result<T>::as_error() const&
  {
    CX_REQUIRE(is_error(), "no error in cx::result");
    return std::get<error>(payload);
  }

  template<typename T>
  constexpr result<T>::operator bool() const noexcept
  {
    return is_value();
  }

  template<typename T>
  template<typename F>
  auto result<T>::transform(F f) &&
  {
    using U = std::decay_t<decltype(f(std::declval<T>()))>;
    if (is_value()) {
      return result<U>(f(std::move(std::get<T>(payload))));
    }
    else {
      return result<U>(std::move(std::get<error>(payload)));
    }
  }

  // result<void>

  constexpr bool result<void>::is_value() const noexcept
  {
    return std::holds_alternative<std::monostate>(payload);
  }

  constexpr bool result<void>::is_error() const noexcept
  {
    return std::holds_alternative<error>(payload);
  }

  constexpr void result<void>::as_value() &&
  {
    CX_REQUIRE(is_value(), "no value in cx::result<void>");
    return;
  }

  constexpr error result<void>::as_error() &&
  {
    CX_REQUIRE(is_error(), "no value in cx::result<void>");
    return std::get<error>(std::move(payload));
  }

  constexpr result<void>::operator bool() const noexcept
  {
    return is_value();
  }
}

namespace cx::detail {
  template<typename T>
  struct is_result_t : public std::false_type {};

  template<typename T>
  struct is_result_t<result<T>> : public std::true_type {};
}
