#pragma once

#include <algorithm>
#include <cctype>
#include <cstdint>
#include <filesystem>
#include <optional>
#include <span>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <variant>
#include <vector>

#include <fmt/format.h>

namespace cx {
  using size_t = std::size_t;

  // TODO:
  // using string      = vector<char>;
  // using string_view = span<const char>;
  using string      = std::string;
  using string_view = std::string_view;

  extern const string empty_string;

  using unit = std::monostate;

  using path = std::filesystem::path;

  using byte = std::byte;

  namespace literals {
    constexpr byte operator""_bb(unsigned long long) noexcept;
  }

  template<typename T>
  using optional = std::optional<T>;

  using nullopt_t                    = std::nullopt_t;
  constexpr inline nullopt_t nullopt = std::nullopt;

  template<typename... Ts>
  using variant = std::variant<Ts...>;

  template<typename... Ts>
  using tuple = std::tuple<Ts...>;

  template<typename T>
  using vector = std::vector<T>;

  template<typename T, size_t Extent = std::dynamic_extent>
  using span = std::span<T, Extent>;

  // TODO: Replace with std::flat_set and std::flat_map
  template<typename Key, typename T>
  using map = std::unordered_map<Key, T, std::hash<Key>, std::equal_to<>>;

  template<typename Key, typename T>
  using map_multi =
      std::unordered_multimap<Key, T, std::hash<Key>, std::equal_to<>>;

  template<typename Key>
  using set = std::unordered_set<Key, std::hash<Key>, std::equal_to<>>;

  template<std::ranges::range Range>
  std::vector<std::ranges::range_value_t<Range>>
  make_vector(Range const& range);

  bool is_printable(const string_view sv);

  string_view as_string_view(span<const byte>);
  span<const byte> as_byte_span(string_view);
}

// #include "containers.ipp"

template<typename T>
  requires(sizeof(T) == 1)
struct fmt::formatter<cx::span<T>> : public fmt::formatter<cx::string> {
  template<typename FormatContext>
  auto format(const cx::span<T>& x, FormatContext& ctx)
  {
    const std::string_view sv{
        reinterpret_cast<const char*>(x.data()), x.size()};
    if (cx::is_printable(sv)) {
      return fmt::format_to(ctx.out(), "[{}]", sv);
    }
    else {
      auto it    = ctx.out();
      it         = fmt::format_to(it, "[");
      bool first = true;
      for (const cx::byte b: x) {
        if (first) {
          first = false;
        }
        else {
          it = fmt::format_to(it, " ");
        }
        it = fmt::format_to(it, "{:#04x}", static_cast<int>(b));
      }
      it = fmt::format_to(it, "]");
      return it;
    }
  }
};

namespace cx {
  namespace literals {
    constexpr byte operator""_bb(const unsigned long long x) noexcept
    {
      return byte{static_cast<byte>(x)};
    }
  }

  template<std::ranges::range Range>
  std::vector<std::ranges::range_value_t<Range>> make_vector(Range const& range)
  {
    std::vector<std::ranges::range_value_t<Range>> retval;
    for (auto const& x: range) {
      retval.push_back(x);
    }
    return retval;
  }

  inline string_view as_string_view(const span<const byte> bb)
  {
    return {reinterpret_cast<const char*>(bb.data()), bb.size()};
  }
  inline span<const byte> as_byte_span(const string_view sv)
  {
    return {reinterpret_cast<const byte*>(sv.data()), sv.size()};
  }
}
