#pragma once

#include "misc/containers.hpp"

#include <algorithm>

namespace cx {
  template<auto Value>
  struct ct_value {
    using type = decltype(Value);

    constexpr static inline type value = Value;

    constexpr operator type() const;
  };

  template<int N>
  struct ct_string {
    char data[N];

    constexpr ct_string(const char (&str)[N]);

    operator string() const;
  };

  template<ct_string str>
  constexpr auto operator""_ct_s();
}

// #include "ct_literals.ipp"

namespace cx {

  // ct_integer

  template<auto Value>
  constexpr ct_value<Value>::operator type() const
  {
    return value;
  }

  // ct_string

  template<int N>
  constexpr ct_string<N>::ct_string(const char (&str)[N])
  {
    std::copy(str, str + N, data);
  }

  template<int N>
  ct_string<N>::operator string() const
  {
    return string(data);
  };

  template<ct_string str>
  constexpr auto operator""_ct_s()
  {
    return str;
  }
}
