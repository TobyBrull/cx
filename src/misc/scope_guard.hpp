#pragma once

#include <utility>

namespace cx {
  template<typename F>
  struct scope_guard {
    F exit_function;

    [[nodiscard]] scope_guard(F ef) : exit_function(std::move(ef)) {}
    ~scope_guard()
    {
      exit_function();
    }

    scope_guard(scope_guard&&)            = delete;
    scope_guard& operator=(scope_guard&&) = delete;
  };
}
