#pragma once

#include "misc/containers.hpp"
#include "misc/error.hpp"
#include "misc/result.hpp"

namespace cx {
  CX_DEFINE_ERROR_CATEGORY(no_such_element, 900);

  template<typename T>
  class ptr_set {
    map<const void*, std::unique_ptr<T>> store_;

   public:
    size_t size() const;

    bool is_empty() const;

    void clear();

    void insert(std::unique_ptr<T>);

    result<std::unique_ptr<T>> erase(const void*);

    template<typename F>
    void erase_if(F);

    bool contains(const void*) const;

    template<typename F>
    void for_each(F);
  };

  template<typename T>
  class ptr_vector {
    vector<std::unique_ptr<T>> store_;

   public:
    using iterator       = typename vector<std::unique_ptr<T>>::iterator;
    using const_iterator = typename vector<std::unique_ptr<T>>::const_iterator;

    iterator begin();
    iterator end();

    void clear();

    void push_back(std::unique_ptr<T>);
  };
}

// #include "ptr_containers.ipp"

namespace cx {

  // ptr_set

  template<typename T>
  size_t ptr_set<T>::size() const
  {
    return store_.size();
  }

  template<typename T>
  bool ptr_set<T>::is_empty() const
  {
    return store_.empty();
  }

  template<typename T>
  void ptr_set<T>::clear()
  {
    store_.clear();
  }

  template<typename T>
  void ptr_set<T>::insert(std::unique_ptr<T> ptr)
  {
    T* const p = ptr.get();
    store_[p]  = std::move(ptr);
  }

  template<typename T>
  result<std::unique_ptr<T>> ptr_set<T>::erase(const void* p)
  {
    const auto it = store_.find(p);
    if (it == store_.end()) {
      return errcat_no_such_element;
    }
    else {
      auto retval = std::move(it->second);
      store_.erase(it);
      return retval;
    }
  }

  template<typename T>
  template<typename F>
  void ptr_set<T>::erase_if(F f)
  {
    for (auto it = store_.begin(); it != store_.end();) {
      if (f(it->second.get())) {
        it = store_.erase(it);
      }
      else {
        ++it;
      }
    }
  }

  template<typename T>
  bool ptr_set<T>::contains(const void* p) const
  {
    return (store_.find(p) != store_.end());
  }

  template<typename T>
  template<typename F>
  void ptr_set<T>::for_each(F f)
  {
    for (const auto& elem: store_) {
      f(elem.second.get());
    }
  }

  // ptr_vector

  template<typename T>
  typename ptr_vector<T>::iterator ptr_vector<T>::begin()
  {
    return store_.begin();
  }

  template<typename T>
  typename ptr_vector<T>::iterator ptr_vector<T>::end()
  {
    return store_.end();
  }

  template<typename T>
  void ptr_vector<T>::clear()
  {
    store_.clear();
  }

  template<typename T>
  void ptr_vector<T>::push_back(std::unique_ptr<T> ptr)
  {
    store_.push_back(std::move(ptr));
  }
}
