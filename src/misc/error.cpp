#include "error.hpp"

// error

namespace cx::detail {
  mutexed<vector<optional<string>>> error_messages = {{string("")}};
}

namespace cx {
  void error::g_message_deallocate(const message_index_t msg_idx)
  {
    detail::error_messages.with([msg_idx](
                                    vector<optional<string>>& error_messages) {
      CX_ASSERT(
          error_messages.at(msg_idx).has_value(),
          "trying to deallocate non-existent message with id={}",
          msg_idx);
      error_messages[msg_idx] = nullopt;
      while (error_messages.size() > 1 && !error_messages.back().has_value()) {
        error_messages.pop_back();
      }
    });
  }

  error::message_index_t error::g_message_allocate(string message)
  {
    return detail::error_messages.with(
        [message =
             std::move(message)](vector<optional<string>>& error_messages) {
          const message_index_t retval = error_messages.size();
          error_messages.push_back(std::move(message));
          return retval;
        });
  }

  string error::g_message_get(const message_index_t msg_idx)
  {
    return detail::error_messages.with(
        [msg_idx](vector<optional<string>>& error_messages) {
          CX_ASSERT(
              error_messages.at(msg_idx).has_value(),
              "trying to fetch non-existent message with id={}",
              msg_idx);
          return error_messages[msg_idx].value();
        });
  }

  error::message_index_t error::g_message_copy(const message_index_t msg_idx)
  {
    return detail::error_messages.with(
        [msg_idx](vector<optional<string>>& error_messages) {
          const message_index_t retval = error_messages.size();
          error_messages.push_back(error_messages[msg_idx]);
          return retval;
        });
  }
}
