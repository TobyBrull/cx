#pragma once

#include "misc/error.hpp"

namespace cx {
  CX_DEFINE_ERROR_CATEGORY(empty, 0);

  CX_DEFINE_ERROR_CATEGORY(unknown, 1);

  CX_DEFINE_ERROR_CATEGORY(not_implemented_yet, 2);

  // From std::errc.
  CX_DEFINE_ERROR_CATEGORY(address_family_not_supported, 100);
  CX_DEFINE_ERROR_CATEGORY(address_in_use, 101);
  CX_DEFINE_ERROR_CATEGORY(address_not_available, 102);
  CX_DEFINE_ERROR_CATEGORY(already_connected, 103);
  CX_DEFINE_ERROR_CATEGORY(argument_list_too_long, 104);
  CX_DEFINE_ERROR_CATEGORY(argument_out_of_domain, 105);
  CX_DEFINE_ERROR_CATEGORY(bad_address, 106);
  CX_DEFINE_ERROR_CATEGORY(bad_file_descriptor, 107);
  CX_DEFINE_ERROR_CATEGORY(bad_message, 108);
  CX_DEFINE_ERROR_CATEGORY(broken_pipe, 109);
  CX_DEFINE_ERROR_CATEGORY(connection_aborted, 110);
  CX_DEFINE_ERROR_CATEGORY(connection_already_in_progress, 111);
  CX_DEFINE_ERROR_CATEGORY(connection_refused, 112);
  CX_DEFINE_ERROR_CATEGORY(connection_reset, 113);
  CX_DEFINE_ERROR_CATEGORY(cross_device_link, 114);
  CX_DEFINE_ERROR_CATEGORY(destination_address_required, 115);
  CX_DEFINE_ERROR_CATEGORY(device_or_resource_busy, 116);
  CX_DEFINE_ERROR_CATEGORY(directory_not_empty, 117);
  CX_DEFINE_ERROR_CATEGORY(executable_format_error, 118);
  CX_DEFINE_ERROR_CATEGORY(file_exists, 119);
  CX_DEFINE_ERROR_CATEGORY(file_too_large, 120);
  CX_DEFINE_ERROR_CATEGORY(filename_too_long, 121);
  CX_DEFINE_ERROR_CATEGORY(function_not_supported, 122);
  CX_DEFINE_ERROR_CATEGORY(host_unreachable, 123);
  CX_DEFINE_ERROR_CATEGORY(identifier_removed, 124);
  CX_DEFINE_ERROR_CATEGORY(illegal_byte_sequence, 125);
  CX_DEFINE_ERROR_CATEGORY(inappropriate_io_control_operation, 126);
  CX_DEFINE_ERROR_CATEGORY(interrupted, 127);
  CX_DEFINE_ERROR_CATEGORY(invalid_argument, 128);
  CX_DEFINE_ERROR_CATEGORY(invalid_seek, 129);
  CX_DEFINE_ERROR_CATEGORY(io_error, 130);
  CX_DEFINE_ERROR_CATEGORY(is_a_directory, 131);
  CX_DEFINE_ERROR_CATEGORY(message_size, 132);
  CX_DEFINE_ERROR_CATEGORY(network_down, 133);
  CX_DEFINE_ERROR_CATEGORY(network_reset, 134);
  CX_DEFINE_ERROR_CATEGORY(network_unreachable, 135);
  CX_DEFINE_ERROR_CATEGORY(no_buffer_space, 136);
  CX_DEFINE_ERROR_CATEGORY(no_child_process, 137);
  CX_DEFINE_ERROR_CATEGORY(no_link, 138);
  CX_DEFINE_ERROR_CATEGORY(no_lock_available, 139);
  CX_DEFINE_ERROR_CATEGORY(no_message_available, 140);
  CX_DEFINE_ERROR_CATEGORY(no_message, 141);
  CX_DEFINE_ERROR_CATEGORY(no_protocol_option, 142);
  CX_DEFINE_ERROR_CATEGORY(no_space_on_device, 143);
  CX_DEFINE_ERROR_CATEGORY(no_stream_resources, 144);
  CX_DEFINE_ERROR_CATEGORY(no_such_device_or_address, 145);
  CX_DEFINE_ERROR_CATEGORY(no_such_device, 146);
  CX_DEFINE_ERROR_CATEGORY(no_such_file_or_directory, 147);
  CX_DEFINE_ERROR_CATEGORY(no_such_process, 148);
  CX_DEFINE_ERROR_CATEGORY(not_a_directory, 149);
  CX_DEFINE_ERROR_CATEGORY(not_a_socket, 150);
  CX_DEFINE_ERROR_CATEGORY(not_a_stream, 151);
  CX_DEFINE_ERROR_CATEGORY(not_connected, 152);
  CX_DEFINE_ERROR_CATEGORY(not_enough_memory, 153);
  CX_DEFINE_ERROR_CATEGORY(not_supported, 154);
  CX_DEFINE_ERROR_CATEGORY(operation_canceled, 155);
  CX_DEFINE_ERROR_CATEGORY(operation_in_progress, 156);
  CX_DEFINE_ERROR_CATEGORY(operation_not_permitted, 157);
  CX_DEFINE_ERROR_CATEGORY(operation_not_supported, 158);
  CX_DEFINE_ERROR_CATEGORY(operation_would_block, 159);
  CX_DEFINE_ERROR_CATEGORY(owner_dead, 160);
  CX_DEFINE_ERROR_CATEGORY(permission_denied, 161);
  CX_DEFINE_ERROR_CATEGORY(protocol_error, 162);
  CX_DEFINE_ERROR_CATEGORY(protocol_not_supported, 163);
  CX_DEFINE_ERROR_CATEGORY(read_only_file_system, 164);
  CX_DEFINE_ERROR_CATEGORY(resource_deadlock_would_occur, 165);
  CX_DEFINE_ERROR_CATEGORY(resource_unavailable_try_again, 166);
  CX_DEFINE_ERROR_CATEGORY(result_out_of_range, 167);
  CX_DEFINE_ERROR_CATEGORY(state_not_recoverable, 168);
  CX_DEFINE_ERROR_CATEGORY(stream_timeout, 169);
  CX_DEFINE_ERROR_CATEGORY(text_file_busy, 170);
  CX_DEFINE_ERROR_CATEGORY(timed_out, 171);
  CX_DEFINE_ERROR_CATEGORY(too_many_files_open_in_system, 172);
  CX_DEFINE_ERROR_CATEGORY(too_many_files_open, 173);
  CX_DEFINE_ERROR_CATEGORY(too_many_links, 174);
  CX_DEFINE_ERROR_CATEGORY(too_many_symbolic_link_levels, 175);
  CX_DEFINE_ERROR_CATEGORY(value_too_large, 176);
  CX_DEFINE_ERROR_CATEGORY(wrong_protocol_type, 177);

  CX_DEFINE_ERROR_CATEGORY(connection_finished, 400);
  CX_DEFINE_ERROR_CATEGORY(parse_error, 401);
  CX_DEFINE_ERROR_CATEGORY(received_error, 402);
}
