#pragma once

#include "misc/bit.hpp"
#include "misc/buffer.hpp"

namespace cx {
  template<typename T>
  class ring_buffer {
    vector<T> data_;
    size_t mask_  = 0;
    size_t begin_ = 0;
    size_t end_   = 0;

   public:
    using value_type = T;

    ring_buffer();

    size_t capacity() const;
    void set_capacity(size_t);

    bool is_empty() const;
    size_t size() const;

    void mark_read(size_t);
    void mark_written(size_t);

    bool is_full() const;

    result<> write(span<const T>);

    span<T> prepare_for_write();

    span<const T> prepare_for_read(size_t from_idx);

    //

    struct iterator {
      ring_buffer* b = nullptr;
      size_t pos     = 0;

      T& operator*();
      const T& operator*() const;

      iterator& operator++();
      iterator operator++(int);

      auto operator<=>(const iterator&);
      bool operator==(const iterator&);
    };
    iterator begin();
    iterator end();
  };
}

// #include "ring_buffer.ipp"

namespace cx {

  // ring_buffer::iterator

  template<typename T>
  T& ring_buffer<T>::iterator::operator*()
  {
    return b->data_[pos & b->mask_];
  }
  template<typename T>
  const T& ring_buffer<T>::iterator::operator*() const
  {
    return b->data_[pos & b->mask_];
  }
  template<typename T>
  typename ring_buffer<T>::iterator& ring_buffer<T>::iterator::operator++()
  {
    pos += 1;
    return (*this);
  }
  template<typename T>
  typename ring_buffer<T>::iterator ring_buffer<T>::iterator::operator++(int)
  {
    iterator retval(*this);
    pos += 1;
    return retval;
  }
  template<typename T>
  auto ring_buffer<T>::iterator::operator<=>(const iterator& other)
  {
    return (*this).pos <=> other.pos;
  }
  template<typename T>
  bool ring_buffer<T>::iterator::operator==(const iterator& other)
  {
    return (*this).pos == other.pos;
  }

  template<typename T>
  typename ring_buffer<T>::iterator ring_buffer<T>::begin()
  {
    return {.b = this, .pos = begin_};
  }
  template<typename T>
  typename ring_buffer<T>::iterator ring_buffer<T>::end()
  {
    return {.b = this, .pos = end_};
  }

  // ring_buffer

  template<typename T>
  ring_buffer<T>::ring_buffer()
  {
  }

  template<typename T>
  size_t ring_buffer<T>::capacity() const
  {
    CX_ASSERT(std::has_single_bit(mask_ + 1));
    CX_ASSERT(data_.size() == (mask_ + 1));
    return data_.size();
  }

  template<typename T>
  void ring_buffer<T>::set_capacity(const size_t new_capacity)
  {
    const size_t used_new_size = ceil_single_bit(new_capacity);
    if (used_new_size != data_.size()) {
      ring_buffer<T> new_this;
      new_this.data_.resize(used_new_size);
      new_this.mask_ = (used_new_size - 1);
      new_this.end_  = std::min(used_new_size, size());
      std::copy(begin(), end(), new_this.begin());
      std::swap(*this, new_this);
    }
  }

  template<typename T>
  bool ring_buffer<T>::is_empty() const
  {
    return (begin_ == end_);
  }
  template<typename T>
  size_t ring_buffer<T>::size() const
  {
    return (end_ - begin_);
  }

  template<typename T>
  void ring_buffer<T>::mark_read(const size_t num_read)
  {
    begin_ += num_read;
  }

  template<typename T>
  void ring_buffer<T>::mark_written(const size_t num_written)
  {
    end_ += num_written;
  }

  template<typename T>
  bool ring_buffer<T>::is_full() const
  {
    return begin_ + data_.size() == end_;
  }

  template<typename T>
  result<> ring_buffer<T>::write(const span<const T> src)
  {
    size_t written       = 0;
    size_t left_to_write = src.size();
    while (left_to_write > 0) {
      auto tgt = prepare_for_write();
      if (tgt.empty()) {
        break;
      }
      const size_t write_size = std::min(tgt.size(), left_to_write);
      std::copy(
          src.begin() + written,
          src.begin() + written + write_size,
          tgt.begin());
      mark_written(write_size);
      left_to_write -= write_size;
      written += write_size;
    }
    if (left_to_write > 0) {
      return errcat_write_buffer_full;
    }
    else {
      return {};
    }
  }

  template<typename T>
  span<T> ring_buffer<T>::prepare_for_write()
  {
    const size_t back_chunk_end = (end_ & (~mask_));
    if (end_ == back_chunk_end) {
      return span<T>{
          data_.data(),
          (begin_ & mask_),
      };
    }
    else {
      return span<T>{
          data_.data() + (end_ & mask_),
          data_.size() - (end_ & mask_),
      };
    }
  }

  template<typename T>
  span<const T> ring_buffer<T>::prepare_for_read(const size_t from_idx)
  {
    const size_t local_begin = begin_ + from_idx;

    const size_t back_chunk_begin = (local_begin & (~mask_));
    if (local_begin == back_chunk_begin) {
      return span<const T>{
          data_.data(),
          (end_ & mask_),
      };
    }
    else {
      return span<const T>{
          data_.data() + (local_begin & mask_),
          data_.size() - (local_begin & mask_),
      };
    }
  }
}
