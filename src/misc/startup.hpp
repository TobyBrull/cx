#pragma once

#include "misc/ct_literals.hpp"
#include "misc/macros.hpp"

namespace cx {

  /// Runs a function once at startup. The tagged macro is only needed (using
  /// different tags) if multiple commands have to be run from the same line.
  ///
  /// ```
  /// void foo(int x);
  /// CX_STARTUP(foo(123));
  /// CX_STARTUP_TAGGED(one, foo(42)); CX_STARTUP_TAGGED(two, foo(21));
  /// ```
#define CX_STARTUP_TAGGED(tag, ...)                                        \
  template<cx::ct_string name>                                             \
  struct detail__at_startup_;                                              \
  template<>                                                               \
  struct detail__at_startup_<__FILE__                                      \
                             ":" STRINGIZE(__LINE__) ":" STRINGIZE(tag)> { \
    inline static CX_ATTRIBUTE_USED bool ran = [] {                        \
      __VA_ARGS__;                                                         \
      return false;                                                        \
    }();                                                                   \
  };
#define CX_STARTUP(...) CX_STARTUP_TAGGED(default, __VA_ARGS__)
}
