#include "containers.hpp"

namespace cx {
  const string empty_string = {};

  bool is_printable(const string_view sv)
  {
    return std::all_of(
        sv.begin(), sv.end(), [](const char c) { return std::isprint(c); });
  }
}
