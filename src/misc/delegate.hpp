#pragma once

#include "misc/ct_literals.hpp"
#include "misc/ct_type.hpp"
#include "misc/fixed_address.hpp"

#include <cstring>
#include <tuple>
#include <type_traits>

namespace cx {

  // delegate_proto

  template<auto FnPtr, typename T>
  struct delegate_proto;

  template<auto FreeFnPtr, typename BoundArg>
    requires(std::is_pointer_v<decltype(FreeFnPtr)>)
  struct delegate_proto<FreeFnPtr, BoundArg> {
    ct_value<FreeFnPtr> free_fn_ptr = {};
    BoundArg bound_arg              = {};

    template<typename... Args>
    constexpr auto operator()(Args&&... args) const noexcept
    {
      if constexpr (std::is_same_v<BoundArg, void*>) {
        return (*free_fn_ptr)(std::forward<Args>(args)...);
      }
      else {
        return (*free_fn_ptr)(bound_arg, std::forward<Args>(args)...);
      }
    }
  };

  template<auto MemFnPtr, typename T>
    requires(std::is_member_function_pointer_v<decltype(MemFnPtr)>)
  struct delegate_proto<MemFnPtr, T> {
    ct_value<MemFnPtr> mem_fn_ptr = {};
    T* obj_ptr                    = nullptr;

    template<typename... Args>
    constexpr auto operator()(Args&&... args) const noexcept
    {
      return (obj_ptr->*MemFnPtr)(std::forward<Args>(args)...);
    }
  };

  template<auto FreeFnPtr, typename BoundArg = void*>
  constexpr delegate_proto<FreeFnPtr, BoundArg>
  make_delegate(BoundArg bound_arg = {}) noexcept
    requires(std::is_pointer_v<decltype(FreeFnPtr)>);

  template<auto MemFnPtr, typename T>
  constexpr delegate_proto<MemFnPtr, T> make_delegate(T* obj_ptr) noexcept
    requires(std::is_member_function_pointer_v<decltype(MemFnPtr)>);

  // delegate

  template<typename F>
  class delegate;

  template<typename R, typename... Args>
  class delegate<R(Args...)> {
   public:
    using raw_func_ptr_t = R (*)(void*, Args...);

   private:
    constexpr static R empty_func(void*, Args...);

    raw_func_ptr_t ptr_ = &empty_func;
    void* storage_      = nullptr;

   public:
    constexpr delegate() noexcept = default;
    constexpr delegate(raw_func_ptr_t, void*) noexcept;

    constexpr delegate(const delegate&) noexcept            = default;
    constexpr delegate& operator=(const delegate&) noexcept = default;

    // Constructor from delegate_ff

    template<auto FreeFnPtr>
    constexpr delegate(delegate_proto<FreeFnPtr, void*>) noexcept
      requires(std::is_same_v<decltype(FreeFnPtr), R (*)(Args...)>);

    template<auto FreeFnPtr, typename BoundArg>
    constexpr delegate(delegate_proto<FreeFnPtr, BoundArg>) noexcept
      requires(std::is_same_v<decltype(FreeFnPtr), R (*)(BoundArg, Args...)>);

    // Constructor from delegate_mf

    template<typename T, auto MemFnPtr>
    constexpr delegate(delegate_proto<MemFnPtr, T>) noexcept
      requires(std::is_same_v<decltype(MemFnPtr), R (T::*)(Args...)>);

    template<typename T, auto MemFnPtr>
    constexpr delegate(delegate_proto<MemFnPtr, T>) noexcept
      requires(std::is_same_v<decltype(MemFnPtr), R (T::*)(Args...) const>);

    // Other functions

    constexpr bool has_value() const noexcept;
    constexpr operator bool() const noexcept;

    constexpr void clear() noexcept;

    constexpr R operator()(Args...) const;
  };

  template<typename Delegate>
  consteval bool is_delegate(ct_type<Delegate> = {});

  // delegate_set

  template<typename F>
  class delegate_set;

  template<typename... Args>
  class delegate_set<delegate<void(Args...)>> {
   public:
    using priority_t = int64_t;
    using id_t       = uint64_t;

   private:
    struct data {
      delegate<void(Args...)> dg;
      priority_t prio = 0;
    };
    map<id_t, data> delegates_;

    // Derived from delegates_
    vector<delegate<void(Args...)>> compute_sorted_delegates();
    vector<delegate<void(Args...)>> sorted_delegates_;

   public:
    id_t add(delegate<void(Args...)>, priority_t = 0);

    void remove(id_t);
    void remove_by_priority(priority_t);

    void operator()(Args...) const;
  };
}

// #include "delegate.ipp"

namespace cx {
  // delegate_proto

  template<auto FreeFnPtr, typename BoundArg>
  constexpr delegate_proto<FreeFnPtr, BoundArg>
  make_delegate(BoundArg bound_arg) noexcept
    requires(std::is_pointer_v<decltype(FreeFnPtr)>)
  {
    return {.bound_arg = bound_arg};
  }

  template<auto MemFnPtr, typename T>
  constexpr delegate_proto<MemFnPtr, T> make_delegate(T* obj_ptr) noexcept
    requires(std::is_member_function_pointer_v<decltype(MemFnPtr)>)
  {
    return {.obj_ptr = obj_ptr};
  }

  // delegate

  template<typename R, typename... Args>
  constexpr R delegate<R(Args...)>::empty_func(void*, Args...)
  {
    if constexpr (std::is_same_v<R, void>) {
      return;
    }
    else {
      return {};
    }
  }

  template<typename R, typename... Args>
  constexpr delegate<R(Args...)>::delegate(
      const raw_func_ptr_t ptr, void* const storage) noexcept
    : ptr_(ptr), storage_(storage)
  {
  }

  template<typename R, typename... Args>
  constexpr bool delegate<R(Args...)>::has_value() const noexcept
  {
    return (ptr_ != &empty_func);
  }

  template<typename R, typename... Args>
  constexpr delegate<R(Args...)>::operator bool() const noexcept
  {
    return (ptr_ != &empty_func);
  }

  template<typename R, typename... Args>
  constexpr void delegate<R(Args...)>::clear() noexcept
  {
    ptr_ = &empty_func;
  }

  template<typename R, typename... Args>
  constexpr R delegate<R(Args...)>::operator()(Args... args) const
  {
    return (*ptr_)(storage_, std::move(args)...);
  }

  namespace detail {
    template<typename Delegate>
    struct is_delegate_impl : public std::false_type {};

    template<typename R, typename... Args>
    struct is_delegate_impl<delegate<R(Args...)>> : public std::true_type {};
  }

  template<typename Delegate>
  consteval bool is_delegate(ct_type<Delegate>)
  {
    return detail::is_delegate_impl<Delegate>::value;
  }

  namespace detail {
    template<class To, class From>
    constexpr To void_ptr_cast(const From& src) noexcept
    {
      static_assert(std::is_same_v<To, void*> || std::is_same_v<From, void*>);
      static_assert(std::is_trivial_v<From>);
      static_assert(std::is_trivial_v<To>);

      if constexpr (std::is_same_v<To, void*> && std::is_same_v<From, void*>) {
        return src;
      }
      else if constexpr (std::is_same_v<To, void*>) {
        static_assert(sizeof(From) <= sizeof(void*));
        void* retval = nullptr;
        std::memcpy(&retval, &src, sizeof(From));
        return retval;
      }
      else if constexpr (std::is_same_v<From, void*>) {
        static_assert(sizeof(To) <= sizeof(void*));
        To retval{};
        std::memcpy(&retval, &src, sizeof(To));
        return retval;
      }
    }

    // delegate_free_stub

    template<typename R, typename ArgsTuple, typename MaybeBoundArgsTuple>
    struct delegate_free_stub;

    template<typename R, typename... Args>
    struct delegate_free_stub<R, std::tuple<Args...>, std::tuple<>> {
      template<R (*FreeFnPtr)(Args...)>
      static R stub(void*, Args... args)
      {
        return FreeFnPtr(args...);
      }
    };

    template<typename R, typename... Args, typename BoundArg>
    struct delegate_free_stub<R, std::tuple<Args...>, std::tuple<BoundArg>> {
      template<R (*FreeFnPtr)(BoundArg, Args...)>
      static R stub(void* const storage, Args... args)
      {
        return FreeFnPtr(detail::void_ptr_cast<BoundArg>(storage), args...);
      }
    };
  }

  template<typename R, typename... Args>
  template<auto FreeFnPtr>
  constexpr delegate<R(Args...)>::delegate(
      delegate_proto<FreeFnPtr, void*>) noexcept
    requires(std::is_same_v<decltype(FreeFnPtr), R (*)(Args...)>)
    : ptr_(&(detail::delegate_free_stub<R, std::tuple<Args...>, std::tuple<>>::
                 template stub<FreeFnPtr>))
  {
  }

  template<typename R, typename... Args>
  template<auto FreeFnPtr, typename BoundArg>
  constexpr delegate<R(Args...)>::delegate(
      delegate_proto<FreeFnPtr, BoundArg> dff) noexcept
    requires(std::is_same_v<decltype(FreeFnPtr), R (*)(BoundArg, Args...)>)
    : ptr_(&(detail::delegate_free_stub<
             R,
             std::tuple<Args...>,
             std::tuple<BoundArg>>::template stub<FreeFnPtr>))
    , storage_(detail::void_ptr_cast<void*>(dff.bound_arg))
  {
  }

  namespace detail {
    template<typename R, typename T, typename ArgsTuple>
    struct delegate_method_stub;

    template<typename R, typename T, typename... Args>
    struct delegate_method_stub<R, T, std::tuple<Args...>> {
      template<R (T::*MemFnPtr)(Args...)>
      static R stub(void* const storage, Args... args)
      {
        T* class_ptr = void_ptr_cast<T*>(storage);
        return (class_ptr->*MemFnPtr)(std::move(args)...);
      }

      template<R (T::*MemFnPtr)(Args...) const>
      static R stub_const(void* const storage, Args... args)
      {
        const T* class_ptr = void_ptr_cast<const T*>(storage);
        return (class_ptr->*MemFnPtr)(std::move(args)...);
      }
    };
  }

  template<typename R, typename... Args>
  template<typename T, auto MemFnPtr>
  constexpr delegate<R(Args...)>::delegate(
      const delegate_proto<MemFnPtr, T> dmf) noexcept
    requires(std::is_same_v<decltype(MemFnPtr), R (T::*)(Args...)>)
    : ptr_(&(detail::delegate_method_stub<R, T, std::tuple<Args...>>::
                 template stub<MemFnPtr>))
    , storage_(detail::void_ptr_cast<void*>(dmf.obj_ptr))
  {
    static_assert(is_fixed_address<T>());
  }

  template<typename R, typename... Args>
  template<typename T, auto MemFnPtr>
  constexpr delegate<R(Args...)>::delegate(
      const delegate_proto<MemFnPtr, T> dmf) noexcept
    requires(std::is_same_v<decltype(MemFnPtr), R (T::*)(Args...) const>)
    : ptr_(&(detail::delegate_method_stub<R, T, std::tuple<Args...>>::
                 template stub_const<MemFnPtr>))
    , storage_(detail::void_ptr_cast<void*>(dmf.obj_ptr))
  {
    static_assert(is_fixed_address<T>());
  }
}
