#pragma once

#include "misc/ct_literals.hpp"

#include <utility>

namespace cx {
  template<typename TupleLike, typename F>
  constexpr F tuple_for_each(TupleLike& tuple_like, F);

  template<typename TupleLike, typename F>
  constexpr F tuple_for_each_indexed(TupleLike& tuple_like, F);

  template<typename... Ts, typename F>
  constexpr auto tuple_transform(std::tuple<Ts...>, F);
}

// #include "tuple.ipp"

#include <cstdint>

namespace cx {
  namespace detail {
    template<size_t... Is, typename TupleLike, typename F>
    void tuple_for_each_indexed_impl(
        std::index_sequence<Is...>, TupleLike& tuple_like, F& f)
    {
      (f(ct_value<Is>{}, std::get<Is>(tuple_like)), ...);
    }
  }

  template<typename TupleLike, typename F>
  constexpr F tuple_for_each(TupleLike& tt, F f)
  {
    tuple_for_each_indexed(
        tt, [&f](auto, auto&& elem) { f(std::forward<decltype(elem)>(elem)); });
    return f;
  }

  template<typename TupleLike, typename F>
  constexpr F tuple_for_each_indexed(TupleLike& tt, F f)
  {
    detail::tuple_for_each_indexed_impl(
        std::make_index_sequence<std::tuple_size_v<TupleLike>>(), tt, f);
    return f;
  }

  template<typename... Ts, typename F>
  constexpr auto tuple_transform(std::tuple<Ts...> t, F f)
  {
    return std::apply(
        [f](auto... xs) { return std::tuple{f(xs)...}; }, std::move(t));
  }
}
