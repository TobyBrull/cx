#pragma once

#include <mutex>
#include <thread>

namespace cx {
  template<typename T, typename Mutex = std::mutex>
  class mutexed {
    T value_ = {};
    Mutex mtx_;

   public:
    mutexed() = default;
    mutexed(T value) : value_(std::move(value)) {}

    template<typename F>
    auto with(F)
      requires std::regular_invocable<F, T&>;
  };
}

// #include "thread.ipp"

namespace cx {
  template<typename T, typename Mutex>
  template<typename F>
  auto mutexed<T, Mutex>::with(F f)
    requires std::regular_invocable<F, T&>
  {
    std::lock_guard<Mutex> lock(mtx_);

    return f(value_);
  }
}
