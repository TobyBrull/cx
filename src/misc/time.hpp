#pragma once

#include "misc/containers.hpp"

#include <fmt/format.h>

#include <chrono>
#include <cstdint>
#include <limits>
#include <ratio>
#include <sys/time.h>

namespace cx {

  struct clock_rdtsc {
    using rep    = uint64_t;
    using period = std::ratio<1, 2'400'000'000>; // Normalised to a 2.4 GHz CPU
    using duration                  = std::chrono::duration<rep, period>;
    using time_point                = std::chrono::time_point<clock_rdtsc>;
    constexpr static bool is_steady = true;

    static time_point now() noexcept
    {
      unsigned lo, hi;
      asm volatile("rdtsc" : "=a"(lo), "=d"(hi));
      return time_point(duration(static_cast<rep>(hi) << 32 | lo));
    }
  };

  template<clockid_t ClockID>
  struct clock_gettime {
    using rep    = uint64_t;
    using period = std::ratio<1, 1'000'000'000>; // Normalised to a 2.4 GHz CPU
    using duration   = std::chrono::duration<rep, period>;
    using time_point = std::chrono::time_point<clock_gettime<ClockID>>;
    constexpr static bool is_steady = true;

    static time_point now() noexcept
    {
      timespec ts;
      ::clock_gettime(ClockID, &ts);
      return time_point(duration(ts.tv_sec * 1'000'000'000 + ts.tv_nsec));
    }
  };

  using clock_gettime_realtime  = clock_gettime<CLOCK_REALTIME>;
  using clock_gettime_monotonic = clock_gettime<CLOCK_MONOTONIC>;

  using default_clock = std::chrono::high_resolution_clock;

  using time_t = int64_t;

  // UTC nanos since 1970-01-01
  // Includes default constructed state that represents "invalid time".
  class time {
    time_t nanos_ = std::numeric_limits<time_t>::min();

   public:
    constexpr time() noexcept = default;
    constexpr explicit time(time_t nanos) noexcept;

    constexpr operator bool() const noexcept;

    static time now() noexcept;
    constexpr static time epoch() noexcept;

    constexpr static time min() noexcept;
    constexpr static time max() noexcept;

    constexpr time_t nanos() const noexcept;

    constexpr auto operator<=>(const time&) const noexcept = default;
  };

  class duration {
    time_t nanos_ = 0;

   public:
    constexpr duration() noexcept = default;
    constexpr explicit duration(time_t nanos) noexcept;

    constexpr static duration nanoseconds(long long) noexcept;
    constexpr static duration microseconds(long long) noexcept;
    constexpr static duration milliseconds(long long) noexcept;
    constexpr static duration seconds(long long) noexcept;
    constexpr static duration minutes(long long) noexcept;
    constexpr static duration hours(long long) noexcept;
    constexpr static duration days(long long) noexcept;

    constexpr time_t nanos() const noexcept;

    constexpr auto operator<=>(const duration&) const noexcept = default;
  };

  namespace literals {
    constexpr duration operator""_ns(unsigned long long) noexcept;
    constexpr duration operator""_us(unsigned long long) noexcept;
    constexpr duration operator""_ms(unsigned long long) noexcept;
    constexpr duration operator""_s(unsigned long long) noexcept;
    constexpr duration operator""_min(unsigned long long) noexcept;
    constexpr duration operator""_h(unsigned long long) noexcept;
    constexpr duration operator""_day(unsigned long long) noexcept;
  }

  constexpr duration operator-(time, time) noexcept;
  constexpr time operator+(time, duration) noexcept;
  constexpr duration operator*(long long, duration) noexcept;
}

// #include "time.ipp"

namespace cx {

  // time

  constexpr time::time(const time_t nanos) noexcept : nanos_(nanos) {}

  constexpr time::operator bool() const noexcept
  {
    return (*this) != time();
  }

  inline time time::now() noexcept
  {
    return time(default_clock::now().time_since_epoch().count());
  }

  constexpr time time::epoch() noexcept
  {
    return time(0);
  }

  constexpr time time::min() noexcept
  {
    return time(std::numeric_limits<time_t>::min() + 1);
  }

  constexpr time time::max() noexcept
  {
    return time(std::numeric_limits<time_t>::max());
  }

  constexpr time_t time::nanos() const noexcept
  {
    return nanos_;
  }

  // duration

  constexpr duration::duration(const time_t nanos) noexcept : nanos_(nanos) {}

  constexpr duration duration::nanoseconds(const long long x) noexcept
  {
    return duration(x);
  }
  constexpr duration duration::microseconds(const long long x) noexcept
  {
    return duration(1'000ll * x);
  }
  constexpr duration duration::milliseconds(const long long x) noexcept
  {
    return duration(1'000'000ll * x);
  }
  constexpr duration duration::seconds(const long long x) noexcept
  {
    return duration(1'000'000'000ll * x);
  }

  constexpr duration duration::minutes(const long long x) noexcept
  {
    return duration((60 * 1'000'000'000ll) * x);
  }

  constexpr duration duration::hours(const long long x) noexcept
  {
    return duration((60 * 60 * 1'000'000'000ll) * x);
  }

  constexpr duration duration::days(const long long x) noexcept
  {
    return duration((24 * 60 * 60 * 1'000'000'000ll) * x);
  }

  constexpr time_t duration::nanos() const noexcept
  {
    return nanos_;
  }

  namespace literals {
    constexpr duration operator""_ns(const unsigned long long x) noexcept
    {
      return duration::nanoseconds(x);
    }

    constexpr duration operator""_us(const unsigned long long x) noexcept
    {
      return duration::microseconds(x);
    }

    constexpr duration operator""_ms(const unsigned long long x) noexcept
    {
      return duration::milliseconds(x);
    }

    constexpr duration operator""_s(const unsigned long long x) noexcept
    {
      return duration::seconds(x);
    }

    constexpr duration operator""_min(const unsigned long long x) noexcept
    {
      return duration::minutes(x);
    }

    constexpr duration operator""_h(const unsigned long long x) noexcept
    {
      return duration::hours(x);
    }

    constexpr duration operator""_day(const unsigned long long x) noexcept
    {
      return duration::days(x);
    }
  }

  // operators

  constexpr duration operator-(const time x, const time y) noexcept
  {
    return duration(x.nanos() - y.nanos());
  }

  constexpr time operator+(const time t, const duration d) noexcept
  {
    return time(t.nanos() + d.nanos());
  }

  constexpr duration operator*(const long long mult, const duration d) noexcept
  {
    return duration(mult * d.nanos());
  }
}

template<>
struct fmt::formatter<cx::time> : public fmt::formatter<cx::string> {
  template<typename FormatContext>
  auto format(const cx::time& t, FormatContext& ctx)
  {
    // TODO: The following was mostly generated by ChatGPT! Replace with
    // something that is confirmed to be correct and improves the performance of
    // the first while loop.

    if (t == cx::time()) {
      return fmt::format_to(ctx.out(), "INVALID");
    }
    else if (t == cx::time::min()) {
      return fmt::format_to(ctx.out(), "MIN_TIME");
    }
    else if (t == cx::time::max()) {
      return fmt::format_to(ctx.out(), "MAX_TIME");
    }

    const time_t sec  = t.nanos() / 1'000'000'000;
    const time_t nsec = t.nanos() % 1'000'000'000;

    int days = static_cast<int>(sec / 86'400);
    [[maybe_unused]] const int weekday =
        (days + 4) % 7; // day of the week (Sunday is 0)

    int year  = 1970;
    int month = 1;
    int mday  = 1;

    while (days >= 365) {
      const bool leap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
      days -= leap ? 366 : 365;
      year++;
    }
    while (days < 0) {
      const int prev_year = year - 1;
      const bool leap =
          (prev_year % 4 == 0 &&
           (prev_year % 100 != 0 || prev_year % 400 == 0));
      days += leap ? 366 : 365;
      year--;
    }

    while (days >= 31) {
      int days_in_month = 31;

      if (month == 2) {
        bool leap     = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        days_in_month = leap ? 29 : 28;
      }
      else if (month == 4 || month == 6 || month == 9 || month == 11) {
        days_in_month = 30;
      }

      days -= days_in_month;
      month++;
    }

    mday += days;

    // Format the timestamp string in ISO 8601 format using fmt::format_to
    return fmt::format_to(
        ctx.out(),
        "{:04}-{:02}-{:02}T{:02}:{:02}:{:02}.{:09}Z",
        year,
        month,
        mday,
        (sec / 3600) % 24,
        (sec / 60) % 60,
        sec % 60,
        nsec);
  }
};
