#include "06_newline_lib.hpp"

#include "async/conn_timer.hpp"
#include "async/conn_wrapper.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_epoll.hpp"
#include "async/poller_timer.hpp"

using namespace cx::literals;

struct NewlineClient final
  : public cx::tutorial::conn_my_client<NewlineClient> {
  NewlineClient(cx::conn_journal* journal, cx::chan_stream_ptr ptr)
    : cx::tutorial::conn_my_client<NewlineClient>(journal, std::move(ptr))
  {
    write_request(cx::tutorial::my_request_child_agents{
        .agent_path = {
            "agent-2",
            "sub-z",
        }});
    write_request(cx::tutorial::my_request_num_coros{
        .agent_path = {
            "newline-server",
        }});
    write_request(cx::tutorial::my_request_quit{});
    mark_written();
  }

  void on_conn_my_client(const cx::tutorial::my_response resp)
  {
    if (std::holds_alternative<cx::tutorial::my_response_child_agents>(resp)) {
      const auto& r = std::get<cx::tutorial::my_response_child_agents>(resp);
      fmt::print("child-agents: [");
      for (const auto& ca: r.child_agents) {
        fmt::print("{}, ", ca);
      }
      fmt::print("]\n");
    }
    else if (std::holds_alternative<cx::tutorial::my_response_num_coros>(
                 resp)) {
      const auto& r = std::get<cx::tutorial::my_response_num_coros>(resp);
      fmt::print("num-coros: {}\n", r.n);
    }
    else if (std::holds_alternative<cx::tutorial::my_response_quit>(resp)) {
      fmt::print("quitted\n");
      remove_this_conn();
      owner_agent()->remove_this_agent();
    }
    else if (std::holds_alternative<cx::error>(resp)) {
      const auto& e = std::get<cx::error>(resp);
      fmt::print("error: {}\n", e);
    }
  }
};

struct NewlineAgent final : public cx::agent {
  NewlineAgent(cx::agent* parent) : cx::agent(parent) {}

  virtual cx::result<> on_start() override final
  {
    fmt::print("NewlineAgent::on_start {}\n", static_cast<void*>(this));
    make_conn<NewlineClient>(
        CX_TRY(loop_.create_chan_stream(cx::endpoint_ipv4{.port = 12345}, {})));
    return {};
  }

  virtual void on_stop() override final
  {
    fmt::print("NewlineAgent::on_stop {}\n", static_cast<void*>(this));
  }
};

cx::result<>
cx_main()
{
  cx::loop_impl<
      cx::loop_config_default,
      cx::poller_timer_default,
      cx::poller_epoll>
      loop;
  CX_TRY(loop.root_agent().add_child_agent<NewlineAgent>("newline-client"));
  CX_TRY(loop.run());
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
