#include "async/coro.hpp"
#include "async/coro_in_parallel.hpp"
#include "async/coro_stream.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_epoll.hpp"
#include "async/poller_timer.hpp"

using namespace cx::literals;

struct CoroEchoServer final : public cx::agent {
  cx::chan_stream_accepter_ptr accepter;

  CoroEchoServer(cx::agent* parent) : cx::agent(parent) {}

  virtual cx::result<> on_start() override final
  {
    accepter = CX_TRY(loop_.create_chan_stream_accepter(
        cx::endpoint_ipv4{.port = 12345},
        {.callback = cx::make_delegate<&CoroEchoServer::on_accept>(this)}));
    return {};
  }

  cx::coro<> handle_connection(cx::chan_stream_ptr s)
  {
    cx::coro_stream cs(std::move(s));

    while (true) {
      const cx::span<const cx::byte> r = co_await cs.read_some();
      if (r.empty()) {
        fmt::print("closing connection\n");
        co_return;
      }
      fmt::print("Echoing {} bytes\n", r.size());
      co_await cs.write(r);
    }
  }

  void on_accept(cx::chan_stream_ptr s)
  {
    fmt::print("accepted new connection\n");
    add_coro(handle_connection(std::move(s)));
  }
};

cx::result<>
cx_main()
{
  cx::loop_impl<
      cx::loop_config_default,
      cx::poller_timer_default,
      cx::poller_epoll>
      loop;
  CX_TRY(loop.root_agent().add_child_agent<CoroEchoServer>("coro-echo-server"));
  CX_TRY(loop.run());
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
