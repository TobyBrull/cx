#include "06_newline_lib.hpp"

#include "async/conn_timer.hpp"
#include "async/conn_wrapper.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_epoll.hpp"
#include "async/poller_timer.hpp"
#include "misc/error_cx.hpp"

using namespace cx::literals;

struct NewlineConnection final
  : public cx::tutorial::conn_my_server<NewlineConnection> {
  NewlineConnection(cx::conn_journal* journal, cx::chan_stream_ptr ptr)
    : cx::tutorial::conn_my_server<NewlineConnection>(journal, std::move(ptr))
  {
  }

  void on_conn_my_server(const cx::tutorial::my_request req)
  {
    const auto find_agent =
        [&](const cx::vector<cx::string>& agent_path) -> const cx::agent* {
      const cx::agent* retval = &(ptr_->owner_loop().root_agent());
      cx::size_t pos          = 0;
      while (pos < agent_path.size()) {
        const auto it = retval->children().find(agent_path[pos]);
        if (it == retval->children().end()) {
          return nullptr;
        }
        retval = it->second.get();
        pos += 1;
      }
      return retval;
    };

    if (std::holds_alternative<cx::tutorial::my_request_child_agents>(req)) {
      const cx::agent* a = find_agent(
          std::get<cx::tutorial::my_request_child_agents>(req).agent_path);
      if (a == nullptr) {
        write_response(cx::tutorial::errcat_unknown_agent_path);
        return;
      }
      cx::vector<cx::string> child_agents;
      const auto& children = a->children();
      child_agents.reserve(children.size());
      for (const auto& [name, _]: children) {
        child_agents.push_back(name);
      }
      write_response(cx::tutorial::my_response_child_agents{
          .child_agents = std::move(child_agents),
      });
    }
    else if (std::holds_alternative<cx::tutorial::my_request_num_coros>(req)) {
      const cx::agent* a = find_agent(
          std::get<cx::tutorial::my_request_num_coros>(req).agent_path);
      if (a == nullptr) {
        write_response(cx::tutorial::errcat_unknown_agent_path);
        return;
      }
      write_response(cx::tutorial::my_response_num_coros{
          .n = a->manager().coros.num_coros(),
      });
    }
    else if (std::holds_alternative<cx::tutorial::my_request_stop_server>(
                 req)) {
      ptr_->owner_loop().root_agent().remove_this_agent();
    }
    else if (std::holds_alternative<cx::tutorial::my_request_quit>(req)) {
      fmt::print("connection quitted\n");
    }
    mark_written();
  }
};

struct NewlineServer final : public cx::agent {
  cx::chan_stream_accepter_ptr accepter;

  NewlineServer(cx::agent* parent) : cx::agent(parent) {}

  virtual cx::result<> on_start() override final
  {
    fmt::print("NewlineServer::on_start called\n");
    accepter = CX_TRY(loop_.create_chan_stream_accepter(
        cx::endpoint_ipv4{.port = 12345},
        {.callback = cx::make_delegate<&NewlineServer::on_accept>(this)}));
    return {};
  }

  void on_accept(cx::chan_stream_ptr s)
  {
    fmt::print("accepted new connection\n");
    make_conn<NewlineConnection>(std::move(s));
  }

  virtual void on_stop() override final
  {
    fmt::print("NewlineServer::on_stop called\n");
  }
};

struct TrivialAgent final : public cx::agent {
  TrivialAgent(cx::agent* parent) : cx::agent(parent) {}
};

cx::result<>
cx_main()
{
  cx::loop_impl<
      cx::loop_config_default,
      cx::poller_timer_default,
      cx::poller_epoll>
      loop;
  auto& ra = loop.root_agent();

  auto* a1 = CX_TRY(ra.add_child_agent<NewlineServer>("newline-server"));

  // Add some trivial agents so that the server can publish some interesting
  // info.

  auto* a2 = CX_TRY(ra.add_child_agent<TrivialAgent>("agent-1"));
  auto* a3 = CX_TRY(ra.add_child_agent<TrivialAgent>("agent-2"));

  CX_TRY(a1->add_child_agent<TrivialAgent>("trivial"));
  CX_TRY(a2->add_child_agent<TrivialAgent>("sub-b"));
  CX_TRY(a3->add_child_agent<TrivialAgent>("sub-x"));
  auto* sub_a3 = CX_TRY(a3->add_child_agent<TrivialAgent>("sub-z"));

  CX_TRY(sub_a3->add_child_agent<TrivialAgent>("sub-sub-i"));
  CX_TRY(sub_a3->add_child_agent<TrivialAgent>("sub-sub-ii"));
  CX_TRY(sub_a3->add_child_agent<TrivialAgent>("sub-sub-iii"));

  CX_TRY(loop.run());
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
