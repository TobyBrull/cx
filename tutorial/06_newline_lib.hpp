#pragma once

#include "async/chan_stream.hpp"
#include "async/conn_manager.hpp"
#include "async/conn_stream_delimited.hpp"
#include "misc/error_cx.hpp"
#include "misc/result.hpp"

#include <coroutine>

namespace cx::tutorial {

  CX_DEFINE_ERROR_CATEGORY(unknown_agent_path, 10000);

  // conn_stream_item

  template<typename Derived>
  class conn_stream_item
    : public conn_stream_delimited<conn_stream_item<Derived>> {
   public:
    using item = variant<string, vector<string>, error>;

   private:
    enum class item_type {
      UNKNOWN,
      STRING,
      VECTOR,
      ERROR,
    };
    struct item_desc {
      size_t count   = 0;
      item_type type = item_type::UNKNOWN;
    };
    vector<item_desc> item_queue_;
    size_t parse_idx_begin_ = 0;
    size_t parse_idx_end_   = 0;

    size_t offset_of_item(size_t item_idx) const;

    using conn_base = conn_stream_delimited<conn_stream_item<Derived>>;
    friend conn_base;
    void on_conn_stream_delim(chan_stream_event);

   protected:
    size_t num_items() const;
    item get_item(size_t idx) const;
    void mark_read_items(size_t);

    result<> write_item(const item&);

   public:
    conn_stream_item(conn_journal*, chan_stream_ptr);
  };

  // definition of my_request_.../my_response_... protocol

  struct my_request_child_agents {
    vector<string> agent_path;
  };
  struct my_request_num_coros {
    vector<string> agent_path;
  };
  struct my_request_stop_server {};
  struct my_request_quit {};
  using my_request = variant<
      my_request_child_agents,
      my_request_num_coros,
      my_request_stop_server,
      my_request_quit>;

  struct my_response_child_agents {
    vector<string> child_agents;
  };
  struct my_response_num_coros {
    size_t n = -1;
  };
  struct my_response_quit {};
  using my_response = variant<
      my_response_child_agents,
      my_response_num_coros,
      my_response_quit,
      error>;

  // conn_my_server

  template<typename Derived>
  class conn_my_server : public conn_stream_item<conn_my_server<Derived>> {
    using conn_base = conn_stream_item<conn_my_server<Derived>>;

    optional<string> cur_command;
    optional<vector<string>> cur_data;

    friend conn_base;
    void on_conn_stream_item(chan_stream_event);

   protected:
    void write_response(const my_response&);

   public:
    conn_my_server(conn_journal*, chan_stream_ptr);
  };

  // conn_my_client

  template<typename Derived>
  class conn_my_client : public conn_stream_item<conn_my_client<Derived>> {
    using conn_base = conn_stream_item<conn_my_client<Derived>>;

    friend conn_base;
    void on_conn_stream_item(chan_stream_event);

   protected:
    void write_request(my_request);

   public:
    conn_my_client(conn_journal*, chan_stream_ptr);
  };

  // coro_my_client

  class coro_my_client : private conn_my_client<coro_my_client> {
    using conn_base = conn_my_client<coro_my_client>;

    optional<my_response> stored_response;
    std::coroutine_handle<> to_resume_ = nullptr;

    friend conn_base;
    void on_conn_my_client(const cx::tutorial::my_response);

   public:
    coro_my_client(conn_journal*, chan_stream_ptr);

    struct read_next_awaiter;
    read_next_awaiter read_next();

    struct write_next_awaiter;
    write_next_awaiter write_next();
  };
}

// #include "06_newline_lib.ipp"

namespace cx::tutorial {

  // conn_stream_item

  template<typename Derived>
  conn_stream_item<Derived>::conn_stream_item(
      conn_journal* journal, chan_stream_ptr ptr)
    : conn_base(journal, std::move(ptr))
  {
  }

  template<typename Derived>
  void
  conn_stream_item<Derived>::on_conn_stream_delim(const chan_stream_event ev)
  {
    bool found_new_item = false;

    while (parse_idx_end_ < conn_base::num_lines()) {
      const auto line = as_string_view(conn_base::get_line(parse_idx_end_));
      const bool in_vector = (parse_idx_begin_ < parse_idx_end_);
      if (!in_vector && line.starts_with("(string)")) {
        item_queue_.push_back(item_desc{.count = 1, .type = item_type::STRING});
        found_new_item = true;
        parse_idx_begin_ += 1;
        parse_idx_end_ += 1;
      }
      else if (!in_vector && line.starts_with("(error)")) {
        item_queue_.push_back(item_desc{.count = 1, .type = item_type::ERROR});
        found_new_item = true;
        parse_idx_begin_ += 1;
        parse_idx_end_ += 1;
      }
      else if (!in_vector && line == "(vector)[") {
        parse_idx_end_ += 1;
      }
      else if (in_vector && line == "(vector)]") {
        parse_idx_end_ += 1;
        item_queue_.push_back(item_desc{
            .count = parse_idx_end_ - parse_idx_begin_,
            .type  = item_type::VECTOR,
        });
        found_new_item   = true;
        parse_idx_begin_ = parse_idx_end_;
      }
      else if (in_vector && line.starts_with("(vector)")) {
        parse_idx_end_ += 1;
      }
      else {
        conn_base::fail();
        return;
      }
    }

    if (found_new_item || is_finished(conn_base::ptr_->state())) {
      static_cast<Derived*>(this)->on_conn_stream_item(ev);
    }

    // If one item is longer than the complete receive buffer.
    if (conn_base::is_recv_buffer_full() && item_queue_.empty()) {
      conn_base::fail();
      return;
    }
  }

  template<typename Derived>
  size_t conn_stream_item<Derived>::offset_of_item(const size_t item_idx) const
  {
    size_t retval = 0;
    for (size_t i = 0; i < item_idx; ++i) {
      retval += item_queue_[i].count;
    }
    return retval;
  }

  template<typename Derived>
  size_t conn_stream_item<Derived>::num_items() const
  {
    return item_queue_.size();
  }

  template<typename Derived>
  typename conn_stream_item<Derived>::item
  conn_stream_item<Derived>::get_item(const size_t idx) const
  {
    CX_REQUIRE(
        idx < num_items(),
        "idx={} out-of-bounds with num-items={}",
        idx,
        item_queue_.size());

    size_t i_line = offset_of_item(idx);

    const auto& item = item_queue_[idx];

    if (item.type == item_type::STRING) {
      return string{as_string_view(conn_base::get_line(i_line).subspan(8))};
    }
    else if (item.type == item_type::ERROR) {
      return error{
          errcat_received_error,
          string{as_string_view(conn_base::get_line(i_line).subspan(7))},
      };
    }
    else {
      CX_ASSERT(item.type == item_type::VECTOR);
      CX_ASSERT(item.count >= 2);
      vector<string> retval;
      retval.reserve(item.count - 2);
      for (size_t i = i_line + 1; i < i_line + item.count - 1; ++i) {
        retval.push_back(
            string{as_string_view(conn_base::get_line(i).subspan(8))});
      }
      return retval;
    }
  }

  template<typename Derived>
  void conn_stream_item<Derived>::mark_read_items(const size_t num_read_items)
  {
    const size_t num_read_lines = offset_of_item(num_read_items);
    item_queue_.erase(
        item_queue_.begin(), item_queue_.begin() + num_read_items);
    conn_base::mark_read_lines(num_read_lines);
    parse_idx_begin_ -= num_read_items;
    parse_idx_end_ -= num_read_items;
  }

  template<typename Derived>
  result<> conn_stream_item<Derived>::write_item(const item& data)
  {
    if (std::holds_alternative<string>(data)) {
      CX_TRY(conn_base::write_line(
          as_byte_span(fmt::format("(string){}", std::get<string>(data)))));
    }
    else if (std::holds_alternative<vector<string>>(data)) {
      CX_TRY(conn_base::write_line(as_byte_span("(vector)[")));
      for (const string& s: std::get<vector<string>>(data)) {
        CX_TRY(
            conn_base::write_line(as_byte_span(fmt::format("(vector){}", s))));
      }
      CX_TRY(conn_base::write_line(as_byte_span("(vector)]")));
    }
    else if (std::holds_alternative<error>(data)) {
      CX_TRY(conn_base::write_line(
          as_byte_span(fmt::format("(error){}", std::get<error>(data)))));
    }
    return {};
  }

  // conn_my_server

  template<typename Derived>
  void conn_my_server<Derived>::on_conn_stream_item(chan_stream_event)
  {
    Derived* d = static_cast<Derived*>(this);

    while (conn_base::num_items() > 0) {
      const auto& item = conn_base::get_item(0);

      if (std::holds_alternative<vector<string>>(item)) {
        vector<string> vec = std::move(std::get<vector<string>>(item));
        if (vec == vector<string>({"stop-server"})) {
          d->on_conn_my_server(my_request_stop_server{});
        }
        else if (vec == vector<string>({"quit"})) {
          conn_base::write_item(string{"quit"});
          d->on_conn_my_server(my_request_quit{});
          conn::remove_this_conn();
        }
        else if (vec.size() >= 1) {
          const string command = std::move(vec.front());
          vec.erase(vec.begin());
          if (command == "child-agents") {
            d->on_conn_my_server(my_request_child_agents{
                .agent_path = std::move(vec),
            });
          }
          else if (command == "num-coros") {
            d->on_conn_my_server(my_request_num_coros{
                .agent_path = std::move(vec),
            });
          }
        }
      }
      else {
        conn_base::fail();
        return;
      }

      conn_base::mark_read_items(1);
    }
  }

  template<typename Derived>
  conn_my_server<Derived>::conn_my_server(
      conn_journal* journal, chan_stream_ptr ptr)
    : conn_base(journal, std::move(ptr))
  {
  }

  template<typename Derived>
  void conn_my_server<Derived>::write_response(const my_response& resp)
  {
    if (std::holds_alternative<my_response_child_agents>(resp)) {
      const auto& r = std::get<my_response_child_agents>(resp);
      conn_base::write_item(r.child_agents);
    }
    else if (std::holds_alternative<my_response_num_coros>(resp)) {
      const auto& r = std::get<my_response_num_coros>(resp);
      conn_base::write_item(std::to_string(r.n));
    }
    else if (std::holds_alternative<error>(resp)) {
      conn_base::write_item(std::get<error>(std::move(resp)));
    }
  }

  // conn_my_client

  template<typename Derived>
  void conn_my_client<Derived>::on_conn_stream_item(chan_stream_event)
  {
    Derived* d = static_cast<Derived*>(this);

    while (conn_base::num_items() > 0) {
      const auto& item = conn_base::get_item(0);

      if (std::holds_alternative<string>(item)) {
        auto& dat = std::get<string>(item);
        if (dat == "quit") {
          d->on_conn_my_client(my_response_quit{});
        }
        else {
          d->on_conn_my_client(my_response_num_coros{
              .n = static_cast<size_t>(std::stoi(dat)),
          });
        }
      }
      else if (std::holds_alternative<vector<string>>(item)) {
        auto& dat = std::get<vector<string>>(item);
        d->on_conn_my_client(my_response_child_agents{
            .child_agents = std::move(dat),
        });
      }
      else if (std::holds_alternative<error>(item)) {
        d->on_conn_my_client(std::get<error>(std::move(item)));
      }

      conn_base::mark_read_items(1);
    }
  }

  template<typename Derived>
  conn_my_client<Derived>::conn_my_client(
      conn_journal* journal, chan_stream_ptr ptr)
    : conn_base(journal, std::move(ptr))
  {
  }

  template<typename Derived>
  void conn_my_client<Derived>::write_request(my_request req)
  {
    if (std::holds_alternative<my_request_child_agents>(req)) {
      auto& r = std::get<my_request_child_agents>(req);
      r.agent_path.insert(r.agent_path.begin(), "child-agents");
      conn_base::write_item(std::move(r.agent_path));
    }
    else if (std::holds_alternative<my_request_num_coros>(req)) {
      auto& r = std::get<my_request_num_coros>(req);
      r.agent_path.insert(r.agent_path.begin(), "num-coros");
      conn_base::write_item(std::move(r.agent_path));
    }
    else if (std::holds_alternative<my_request_stop_server>(req)) {
      conn_base::write_item(vector<string>({"stop-server"}));
    }
    else if (std::holds_alternative<my_request_quit>(req)) {
      conn_base::write_item(vector<string>({"quit"}));
    }
  }

  // coro_my_client

  inline coro_my_client::coro_my_client(
      conn_journal* journal, chan_stream_ptr ptr)
    : conn_base(journal, std::move(ptr))
  {
  }

  inline void
  coro_my_client::on_conn_my_client(const cx::tutorial::my_response resp)
  {
    (void)resp;
  }

  struct coro_my_client::read_next_awaiter {
    coro_my_client* client_ = nullptr;
  };
  inline coro_my_client::read_next_awaiter coro_my_client::read_next()
  {
    return read_next_awaiter{.client_ = this};
  }

  struct coro_my_client::write_next_awaiter {
    coro_my_client* client_ = nullptr;
  };
  inline coro_my_client::write_next_awaiter coro_my_client::write_next()
  {
    return write_next_awaiter{.client_ = this};
  }
}
