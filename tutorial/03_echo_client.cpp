#include "async/conn_timer_default.hpp"
#include "async/conn_wrapper.hpp"
#include "async/endpoint.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_epoll.hpp"
#include "async/poller_timer.hpp"

using namespace cx::literals;

static const std::string_view send_data = "Hello, World!";

struct EchoClient final : public cx::agent {
  cx::chan_stream_ptr ch;

  EchoClient(cx::agent* parent) : cx::agent(parent) {}

  virtual cx::result<> on_start() override final
  {
    fmt::print("EchoClient::on_start {}\n", static_cast<void*>(this));

    ch = CX_TRY(loop_.create_chan_stream(
        cx::endpoint_ipv4{.port = 12345},
        {.callback = cx::make_delegate<&EchoClient::on_stream>(this)}));

    make_conn<cx::conn_timer_default>(
        CX_TRY(loop_.create_chan_timer()),
        cx::conn_timer_default_config{
            .expiry = 200_ms,
            .repeat = cx::conn_timer_repeat{.interval = 200_ms}},
        cx::make_delegate<&EchoClient::on_timer>(this));
    return {};
  }

  cx::size_t count = 0;
  void on_timer()
  {
    count += 1;
    if (count > send_data.size()) {
      ch->close();
    }
    else {
      const cx::span<const cx::byte> to_send{
          reinterpret_cast<const cx::byte*>(send_data.data()), count};
      fmt::print("Sending:  {}\n", to_send);
      CX_ASSERT(ch->write(to_send), "send-buffer is full");
      ch->mark_written();
    }
  }

  void on_stream(const cx::chan_stream_event ev, cx::chan_stream* p)
  {
    if (ev == cx::chan_stream_event::state) {
      fmt::print("EchoClient::on_stream(state={})\n", p->state());
    }

    if (is_finished(p->state())) {
      remove_this_agent();
    }
    else {
      if (!p->recv_buffer().read_span().empty()) {
        fmt::print("Received: {}\n", p->recv_buffer().read_span());
        p->recv_buffer().mark_read(p->recv_buffer().read_span().size());
      }
    }
  }

  virtual void on_stop() override final
  {
    fmt::print("EchoClient::on_stop {}\n", static_cast<void*>(this));
  }
};

cx::result<>
cx_main()
{
  cx::loop_impl<
      cx::loop_config_default,
      cx::poller_timer_default,
      cx::poller_epoll>
      loop;
  CX_TRY(loop.root_agent().add_child_agent<EchoClient>("echo-client"));
  CX_TRY(loop.run());
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
