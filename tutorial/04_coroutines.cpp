#include "async/coro.hpp"
#include "async/coro_in_parallel.hpp"
#include "async/coro_timer.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_timer.hpp"

using namespace cx::literals;

struct MyCoroAgent final : public cx::agent {
  MyCoroAgent(agent* parent) : cx::agent(parent) {}

  int count = 0;

  virtual cx::result<> on_start() override final
  {
    fmt::print("MyCoroAgent::on_start\n");
    add_coro(main_coro());

    return {};
  }

  cx::coro<> main_coro()
  {
    co_await cx::coro_sleep_for(loop_, 1000_ms);
    for (int i = 0; i < 5; ++i) {
      fmt::print("MyCoroAgent timer, {} times\n", i);
      co_await cx::coro_sleep_for(loop_, 1000_ms);
    }

    fmt::print(
        "this coro is owned by agent = {}\n",
        (co_await cx::coro_owner_agent{})->name());

    remove_this_agent();
  }

  virtual void on_stop() override final
  {
    fmt::print("MyCoroAgent::on_stop\n");
  }
};

cx::coro<int>
my_timer(cx::loop& loop, cx::string name, int count, const cx::duration d)
{
  fmt::print("my_timer started\n");

  for (int i = 0; i < count; ++i) {
    co_await cx::coro_sleep_for(loop, d);
    fmt::print("Timer {}, ticked {} times\n", name, i);
  }

  fmt::print("my_timer about to co_return\n");
  co_return count * 10;
}

cx::coro<>
my_coro(cx::loop& loop)
{
  fmt::print("my_coro started\n");

  for (int i = 0; i < 4; ++i) {
    co_await cx::coro_sleep_for(loop, 300_ms);
    fmt::print("Timer main, ticked {} times\n", i);
  }

  fmt::print("my_coro yielding\n");
  co_await cx::coro_yield{};
  fmt::print("my_coro yielding done\n");

  {
    co_await my_timer(loop, "timer-1", 10, 200_ms);
    const int result = co_await my_timer(loop, "timer-2", 20, 100_ms);
    fmt::print("timer-2 returned {}\n", result);
  }

  {
    const auto [r1, r2] = co_await cx::coro_in_parallel(
        my_timer(loop, "timer-a", 10, 200_ms),
        my_timer(loop, "timer-b", 35, 80_ms));
    fmt::print("coro-in-parallel returned tuple = ({}, {})\n", r1, r2);
  }

  fmt::print("my_coro ended\n");
}

cx::result<>
cx_main()
{
  cx::loop_impl<cx::loop_config_default, cx::poller_timer_default> loop;
  CX_TRY(loop.root_agent().add_child_agent<MyCoroAgent>("my-coro-agent"));
  loop.root_agent().add_coro(my_coro(loop));
  CX_TRY(loop.run());
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
