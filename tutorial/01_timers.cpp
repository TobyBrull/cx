#include "async/conn_timer_default.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_timer_simple.hpp"

using namespace cx::literals;

struct MyAgent final : public cx::agent {
  MyAgent(agent* parent) : cx::agent(parent) {}

  virtual cx::result<> on_start() override final
  {
    fmt::print("MyAgent::on_start\n");

    timer_1 = CX_TRY(loop_.create_chan_timer(
        loop_.tick_time() + 500_ms,
        cx::make_delegate<&MyAgent::on_timer_1>(this)));

    make_conn<cx::conn_timer_default>(
        CX_TRY(loop_.create_chan_timer()),
        cx::conn_timer_default_config{
            .expiry = 100_ms,
            .repeat = cx::conn_timer_repeat{.interval = 100_ms, .count = 6}},
        cx::make_delegate<&MyAgent::on_timer_2>(this));

    return {};
  }

  int count = 0;
  cx::time on_timer_1(const cx::time expiry)
  {
    count += 1;
    fmt::print("Timer 1 fired {} times at {}.\n", count, loop_.tick_time());

    if (count == 3) {
      remove_this_agent();
      return {};
    }
    else {
      return loop_.tick_time() + 500_ms;
      return expiry + 500_ms;
      return cx::time::now() + 500_ms;
    }
  }
  cx::chan_timer_ptr timer_1;

  void on_timer_2()
  {
    fmt::print("Timer 2 fired at {}.\n", loop_.tick_time());
  }

  virtual void on_stop() override final
  {
    fmt::print("MyAgent::on_stop\n");
  }
};

cx::result<>
cx_main()
{
  cx::loop_impl<cx::loop_config_default, cx::poller_timer_simple> loop;
  CX_TRY(loop.root_agent().add_child_agent<MyAgent>("my-agent"));
  fmt::print("Starting event-loop.\n");
  CX_TRY(loop.run());
  fmt::print("Event-loop finished.\n");
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
