#include "async/chan_accepter.hpp"
#include "async/chan_stream.hpp"
#include "async/endpoint.hpp"
#include "async/loop_impl.hpp"
#include "async/poller_epoll.hpp"
#include "async/poller_timer.hpp"

struct EchoAgent final : public cx::agent {
  cx::chan_stream_ptr chan_remote;

  EchoAgent(agent* parent, cx::chan_stream_ptr cc)
    : cx::agent(parent), chan_remote(std::move(cc))
  {
    chan_remote->set_callback(cx::make_delegate<&EchoAgent::on_stream>(this));
  }

  virtual cx::result<> on_start() override final
  {
    fmt::print("EchoAgent::on_start {}\n", static_cast<void*>(this));
    return {};
  }

  void on_stream(cx::chan_stream_event, cx::chan_stream* p)
  {
    CX_ASSERT(p == chan_remote.ptr(), "Bug in cx");
    if (is_finished(p->state())) {
      remove_this_agent();
    }
    else {
      const size_t num_copy_bytes = std::min(
          p->send_buffer().write_span().size(),
          p->recv_buffer().read_span().size());
      if (num_copy_bytes > 0) {
        fmt::print("Echoing {} bytes\n", num_copy_bytes);
        std::ranges::copy(
            p->recv_buffer().read_span().subspan(0, num_copy_bytes),
            p->send_buffer().write_span().begin());
        p->recv_buffer().mark_read(num_copy_bytes);
        p->send_buffer().mark_written(num_copy_bytes);
        p->mark_written();
      }
    }
  }

  virtual void on_stop() override final
  {
    fmt::print("EchoConnection::on_stop {}\n", static_cast<void*>(this));
  }
};

struct EchoServer final : public cx::agent {
  cx::chan_stream_accepter_ptr accepter;

  EchoServer(cx::agent* parent) : cx::agent(parent) {}

  virtual cx::result<> on_start() override final
  {
    accepter = CX_TRY(loop_.create_chan_stream_accepter(
        cx::endpoint_ipv4{.port = 12345},
        {.callback = cx::make_delegate<&EchoServer::on_accept>(this)}));
    return {};
  }

  void on_accept(cx::chan_stream_ptr s)
  {
    add_child_agent<EchoAgent>("connection", std::move(s));
  }
};

cx::result<>
cx_main()
{
  cx::loop_impl<
      cx::loop_config_default,
      cx::poller_timer_default,
      cx::poller_epoll>
      loop;
  CX_TRY(loop.root_agent().add_child_agent<EchoServer>("echo-server"));
  CX_TRY(loop.run());
  return {};
}

#include "misc/main.hpp"
CX_MAIN(cx_main);
