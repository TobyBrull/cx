TODO
====

* use expandable circular buffer in conn_stream_delimited, conn_stream_item, ...
* conn configurations

* make buffer_default configurable in CMake

* make coros for 06-newline-lib
* conn_stream_persistent

* Write tests:
  * generic poller_timer test function (use on cx::poller_timer_simple)
  * test cx::agent
    * think about returning cx::result<> from on_start?
    * What happens if cx::agent::on_start call remove_this_agent for a parent?
  * test tutorial code

* conn_json, conn_json_monitor
* conn_message
* conn_capnproto
* pollers should support config params?

* Write Python script that toggles between ipp files and having
  the implementation in the header (under `// #include "loop.ipp"`).

* Add move constructor to cx::error?
* Allow access to cx::result value without move.
