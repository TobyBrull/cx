find_program(
  CLANG_FORMAT_EXECUTABLE
  NAMES clang-format
  )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  ClangFormat
  "Failed to find clang-format executable"
  CLANG_FORMAT_EXECUTABLE
  )

macro(add_clang_format)
  cmake_parse_arguments(PARAM "" "TARGET_PREFIX" "FILES" ${ARGN})

  add_custom_target(
    "${PARAM_TARGET_PREFIX}-clang-format"
    "${CLANG_FORMAT_EXECUTABLE}" "-i" ${PARAM_FILES})
  add_test(
    NAME "${PARAM_TARGET_PREFIX}-clang-format-check"
    COMMAND "${CLANG_FORMAT_EXECUTABLE}" ${PARAM_FILES} "--dry-run" "--Werror"
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    )
endmacro()

