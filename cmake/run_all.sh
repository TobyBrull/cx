#!/bin/bash

set -Eeuxo pipefail

script_abs_path=$(realpath "$0")
script_abs_dir=$(dirname "$script_abs_path")

if [[ $# -ne 3 ]]; then
  echo "Usage: $script_abs_path <source-dir> <base-build-dir> <output-dir>"
  exit 2
fi

source_dir=$(realpath "$1")
base_build_dir=$(realpath "$2")
output_dir=$(realpath "$3")

mkdir "$base_build_dir"

for toolchain_file in "$script_abs_dir"/toolchains/*.cmake
do
  toolchain_name=$(basename "$toolchain_file" .cmake)
  build_dir="$base_build_dir/$toolchain_name"
  "$script_abs_dir/run_unit_test.sh" "$source_dir" "$build_dir" "$toolchain_file"
done

"$script_abs_dir/run_build.sh" "$source_dir" "$base_build_dir/release" "$output_dir"
