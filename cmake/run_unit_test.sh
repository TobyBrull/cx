#!/bin/bash

set -Eeuxo pipefail

script_abs_path=$(realpath "$0")
script_abs_dir=$(dirname "$script_abs_path")

if [[ $# -ne 3 ]]; then
  echo "Usage: $script_abs_path <source-dir> <build-dir> <toolchain-file>"
  exit 2
fi

source_dir=$(realpath "$1")
build_dir=$(realpath "$2")
toolchain_file=$(realpath "$3")

mkdir "$build_dir"

cmake \
  -S "$source_dir" \
  -B "$build_dir" \
  "-DCMAKE_BUILD_TYPE=Debug" \
  "-DCMAKE_TOOLCHAIN_FILE=$toolchain_file"

make \
  -C "$build_dir" \
  "-j$(nproc)" \
  all \
  VERBOSE=1

export LSAN_OPTIONS=suppressions=$script_abs_dir/toolchains/suppr.txt
ctest \
  --test-dir "$build_dir" \
  -C extended \
  --verbose \
  -j $(nproc)
