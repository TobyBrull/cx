set(CMAKE_CXX_COMPILER "g++")
set(CMAKE_C_COMPILER "gcc")
set(CX_ADD_CLANG_TIDY_TESTS Off CACHE BOOL "")

string(APPEND CMAKE_CXX_FLAGS_INIT " -fsanitize=address,undefined")
