#!/bin/bash

set -Eeuxo pipefail

script_abs_path=$(realpath "$0")
script_abs_dir=$(dirname "$script_abs_path")

if [[ $# -ne 1 ]]; then
  echo "Usage: $script_abs_path <output-dir>"
  exit 2
fi

output_dir=$(realpath "$1")

cd /
tar xf "$output_dir/"*.tar.gz --strip-components=1
find /opt/cx/
