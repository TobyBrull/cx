#!/bin/bash

set -Eeuxo pipefail

common_args=("--tag=test" "." "--progress=plain")

#docker build ${common_args[@]} -f cmake/docker/Dockerfile.archlinux
docker build ${common_args[@]} -f cmake/docker/Dockerfile.ubuntu
