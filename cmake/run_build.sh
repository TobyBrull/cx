#!/bin/bash

set -Eeuxo pipefail

script_abs_path=$(realpath "$0")
script_abs_dir=$(dirname "$script_abs_path")

if [[ $# -ne 3 ]]; then
  echo "Usage: $script_abs_path <source-dir> <build-dir> <output-dir>"
  exit 2
fi

source_dir=$(realpath "$1")
build_dir=$(realpath "$2")
output_dir=$(realpath "$3")

mkdir "$build_dir"

cmake \
  -S "$source_dir" \
  -B "$build_dir" \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DCMAKE_CXX_COMPILER=clang++ \
  -DCMAKE_INSTALL_PREFIX=/opt/cx

make \
  -C "$build_dir" \
  "-j$(nproc)" \
  all \
  VERBOSE=1

ctest \
  --test-dir "$build_dir" \
  --verbose \
  -j $(nproc)

cd "$build_dir"
cpack

mkdir "$output_dir"
cp "$build_dir"/*.tar.gz "$output_dir"
