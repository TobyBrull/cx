cmake_minimum_required(VERSION 3.20)
project(cx LANGUAGES CXX)

set(CX_WITH_CCACHE On CACHE BOOL "")
set(CX_WITH_EXCEPTIONS On CACHE BOOL "")
set(CX_WITH_ASSERTS On CACHE BOOL "")
set(CX_ADD_CLANG_TIDY_TESTS On CACHE BOOL "")

set(CMAKE_EXPORT_COMPILE_COMMANDS On CACHE BOOL "" FORCE)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/module_path")

if(CX_WITH_CCACHE)
  find_program(CCACHE_FOUND ccache)
  if(CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
  endif()
endif()

if(MSVC)
  add_compile_options(/W4 /WX)
else()
  add_compile_options(-Wall -Wextra -Werror)
endif()

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_EXTENSIONS False)

find_package(ClangFormat REQUIRED)
find_package(ClangTidy REQUIRED)

enable_testing()

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

#add_subdirectory(doc)
add_subdirectory(src)
add_subdirectory(tutorial)
add_subdirectory(test)

# CPack
set(CPACK_GENERATOR "TGZ")
set(CPACK_PACKAGING_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

include(CPack)
