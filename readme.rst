cx - Concurrent eXecution and ConteXt framework
===============================================

Features
--------

* Event loops

  * futures, coroutines

    * support cancelation: f.cancel() sends signal to interrupt computation
    * support nested futures (like future<future<int>>)? This could be used for
      RPC (see below); the outer future would be satisfied if once the remote
      host has received/ACKed the RPC call.

  * support multiple event-notification methods:

    * epoll
    * io\_uring
    * SolarFlare
    * shared-memory queues

  * timers:

    * heap-based O(log(n))
    * constant offset O(1)

  * rewrite nicer version of tcp-ip-sim as a sample

  * channels

    * TCP socket
    * unix domain socket
    * linux pipe
    * in-memory queue (shm)

    * message framing over byte-buffered channel (reliable datagram)

    * in-memory single producer, multi consumer stream-channels?
    * message-channels should always be multi consumer
    * in-memory message-channels should always be multi producer as well?

  * datagram (unreliable)

  * support linux signals (via chan_message? add conn_signal?)

  * support logging and solid unit-testing

  * Add https://go.dev/tour/concurrency/1 to tutorial

* Multi-threading

  * scoped thread pools, integrated with Context
  * allow multiple threads to work on the same event loop
  * graph-based multi-threading (like oneTBB)

* Context

  * memory allocation
    * own version of string, vector, hash-maps
  * environment variables
  * program execution monitoring

    * logging
    * semi-typed env vars
    * progress bar
    * support chrome tracing

  * see numa/ctxt

* Channels & Connections

  * event-notification method agnostic
  * TCP, UDP, and shared-memory queues
  * Other L4-like protocols?

    * MTP (message transfer protocol): TCP with message framing
    * ZMQ-like sockets, with more well-defined semantics: XSUB/XPUB

  * DNS lookup:
    * getaddrinfo_a
    * https://stackoverflow.com/questions/58069/how-to-use-getaddrinfo-a-to-do-async-resolve-with-glibc

  * capnproto, protobuf
  * websocket
  * JSON, BSON
  * Text based protocols
  * HTTP(S)
  * SSH, TLS
  * Kafka, ZMQ
  * RPC via futures and any of the connections above (JSON, capnproto; TCP
    and/or UDP)

    * support interaction with logging and progress bar

  * Coroutine based protocol/connection parsers? E.g., coro-based json parser.

* Agents

  * add cx::agent::on_heartbeat(), make heartbeat interval configurable via cx::loop_config or something?
  * each agent can have multiple connections/channel/timers and/or strands of execution/coroutines?
  * monitoring agent port to check/control the agents in a process

* Python bindings

  * pybind11
  * interact with asyncio

* Samples

  * Load balancer
  * Simplistic database
  * RPC client/server
  * App that connects to http server and shows all hyperlinks

* own version of STL containers that use the context allocator
